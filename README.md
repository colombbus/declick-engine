# Declick Engine

Declick Engine is a javascript interpreter and runtime environment that can be used with graphical and non-graphical objects. Object names and methods are fully internationalized, as well as runtime environemnt itself. Graphical objects are rendered using [Phaser library](https://phaser.io/).
Declick Engine has been built primarily as an educational tool to teach children how to code and allow them to easily create small graphical games.

## Demonstration

A demonstration page is available here: https://colombbus.gitlab.io/declick-engine/

## How to build

### install dependencies

    npm install

### build library

    npm run build

Generated library is located in dist directory. Currently two versions are generated:

- declick-engine.js, CommonJS library
- declick-engine-esm.js ES module (no size optimization)

### demo

When library is built, a demo web page is generated at `demo/dist/index.html`

## Documentation

### Objects (Work in progress)

#### Documentation in English

- [Web version](https://colombbus.gitlab.io/declick-engine/doc/en/index.html)
- [Markdown version](doc/md/en.md)

#### Documentation en français

- [Version web](https://colombbus.gitlab.io/declick-engine/doc/fr/index.html)
- [Vesion markdown](doc/md/fr.md)

### Sprites and maps

- How to build and use [sprites and maps](doc/md/sprites_and_maps.md)

## How to add objects

### Internationalization

Classes names and exposed methods are internationalized, using two tools:

- [Lingui](https://lingui.dev/)
- [Metadata proposal](https://rbuckton.github.io/reflect-metadata/) through decorators

These tools make it really easy to define a translated name for a class or a method:

```javascript
import 'reflect-metadata'

@Reflect.metadata('translated', 'MyClass')
class MyClass {
  @Reflect.metadata('translated', 'exposedMethod')
  @Reflect.metadata('help', 'exposedMethod_help')
  exposedMethod(value) {
    this._value = value
  }
}
```

For internationalization of `exposedMethod`, two parameters are defined:

- `i18._(exposedMethod)` is the translated name
- `i18n._(exposedMethod_help)` is the help text that will be used for this method

Translation texts are stored in json files within directory `translations`.

Note: in Visual Studio Code, the use of [i18n ally extension](https://github.com/lokalise/i18n-ally) makes it really easy to manage translation texts.

### Adding a basic class

Classes are stored in directory `src/objects/classes`. A basic class should derive from `BaseClass`:

```javascript
import 'reflect-metadata'

class MyClass extends BaseClass {
    ...
}

export default MyClass
```

### Adding a graphical class

Graphical classes are stored in directory `src/objects/classes`. They derive from `GraphicClass`:

```javascript
import GraphicClass from '../graphic-class'

class MyGraphicClass extends GraphicClass {

  constructor() {
    super()
    this._object = // _object holds reference to the actual graphics object, handled by Phaser
  }

  static setup() {
    // this static method is called once at setup: it can be used e.g. to load some assets used by every instance
  }

  tick(delta) {
    // called by graphics framework, may be used to animate the instance
  }

  getX() {
    // should be implemented to give the X coordinate of the instance
  }

  getY() {
    // should be implemented to give the Y coordinate of the instance
  }

  setLocation(x, y) {
    // should be implemented to position the object
  }

}
```

### Adding an instance

Instances are stored in directory `src/objects/instances`. An instance should derive from `BaseInstance`:

```javascript
import BaseInstance from '../base-instance'

class MyInstance extends BaseInstance {}

export default MyInstance
```

### Checking arguments of exposed methods

Since exposed metohds will be used from outside of Declick, there is a strong need for checking the type of provided arguments.To do so, Declick object may use `@checkArguments` as a decorator for the exposed method:

```javascript
import { checkArguments } from '../utils/checks'

  @checkArguments(['integer'])
  myExposedMethod(value) {
    // value should be an integer, otherwise an error is thrown
    ...
  }

  @checkArguments(['integer', 'string'])
  myExposedMethod2(value1, value2) {
    // value1 should be an integer, value2 should by a string
    ...
  }

  @checkArguments(['integer', 'string', 'array'], 2)
  myExposedMethod3(value1, value2, value3) {
    // value1 should be an integer, value2 should by a string, value3 should by an array
    // the two last parameters are optional
    ...
  }

  @checkArguments(['integer', 'integer|string|boolean'])
  myExposedMethod4(value1, value2) {
    // value1 should be a en integer
    // value2 should be an integer OR a string OR a boolean
    ...
  }

```

Available types are: `integer`, `string`, `array`, `number`, `boolean`, `object`, `function`, `canvas`, `any`

### Handling events

Classes and instances may manage events using the following methods:

- `this.addListener('eventName', function)` adds a listener for event `eventName`
- `this.dispatch('eventName')` dispatches event `eventName`

## Dependencies

Declick Engine uses the following libs:

- [acorn javascript parser](https://github.com/acornjs/acorn)
- [JS-Interpreter](https://github.com/NeilFraser/JS-Interpreter)
- [Phaser](https://phaser.io/) for graphical objects rendering
