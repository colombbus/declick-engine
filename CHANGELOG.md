# Changelog

All notable changes to **declick-engine** project will be documented in this file.

## [0.10.0] - 2025-01-20

- [feature] Add dutch translation

## [0.9.4] - 2025-01-15

- [fix] Enable overlaps on builder's platform

## [0.9.3] - 2025-01-15

- [feature] Add interrupt method to declick instance

## [0.9.2] - 2024-11-05

- [feature] Add wait method to keyboard instance

## [0.9.1] - 2024-11-02

- [fix] Remove unused method addScene from Sprite
- [fix] Remove unused method hasIn from List
- [doc] Add French documentation for Builder
- [doc] Add French documentation for List
- [doc] Complete French documentation for Item
- [doc] Complete French documentation for Sprite

## [0.9.0] - 2024-10-19

- [dev] Use vite instead of parcel for build
- [dev] Use vitest instead of mocha for tests
- [fix] Enhance translation management
- [fix] Fix various bugs (TS errors)

## [0.8.14] - 2024-10-17]

- [fix] Fix internationalization of demo site and production build

## [0.8.13] - 2024-10-16

- [fix] Fix custom item creation

## [0.8.12] - 2024-10-16

- [feature] Enhance Sprite and Item interactions

## [0.8.11] - 2024-10-16

- [fix] Fix Sprite method name

## [0.8.10] - 2024-10-16

- [feature] Refine List object
- [feature] Retrieve the list of objects caught by a Sprite

## [0.8.9] - 2024-10-15

- [fix] Preserve tile properties when creating new tiles in Platform

## [0.8.8] - 2024-10-10

- [fix] Fix Plaform collision and overlap management
- [dev] Use lingui instead of es2015-i18n-tag for i18n

## [0.8.7] - 2024-10-08

- [fix] Allow a Robot to carry an Item
- [fix] Enhance Platform collision and overlap management

## [0.8.6] - 2024-10-01

- [doc] Update documentation for sprites and maps
- [feature] Enhance Builder so that it uses a Platform object that can be reused
- [fix] Change default tile name from 'entry' to 'entrance'
- [feature] Add Item default representations

## [0.8.5] - 2024-05-14

- [fix] Platform creation : handle case where tile properties are not set

## [0.8.4] - 2024-05-02

- [fix] Rename mayMove method to mayBeMoved

## [0.8.3] - 2024-05-01

- [doc] Add French documentation for Robot
- [doc] Add French documentation for Turtle
- [doc] Add French documentation for Button
- [feature] Add ability to retrieve step length from a Robot
- [fix] Fix automatic documentation generation
- [fix] Fix string commands parsing

## [0.8.2] - 2024-05-01

- [feature] Add ability to get all mathods for a class or instance
- [feature] Handle messages from declick.write

## [0.8.1] - 2024-04-28

- [fix] Handle errors when loading a program from declick

## [0.8.0] - 2024-04-25

- [dev] Add automatic documentation generation for objects
- [dev] Simplify i18n management
- [doc] Add French documentation for Sprite
- [doc] Add French documentation for Walker
- [doc] Add French documentation for Item
- [doc] Add French documentation for Text
- [doc] Add French documentation for Platform
- [doc] Add French documentation for PlatformTile
- [doc] Add French documentation for declick instance
- [doc] Add French documentation for keyboard instance
- [fix] Fix dragging of Robot objects
- [feature] Add program name management
- [feature] Add ability to load a program from declick
- [fix] declick.clear keeps scheduler alive

## [0.7.6] - 2024-04-09

- [fix] Add default implementation for getX and getY

## [0.7.5] - 2024-04-09

- [dev] Refactor graphical objects deletion
- [dev] Update to Phaser 3.60
- [feature] Add ability to drag graphical objects
- [feature] Suspend and resume graphics and runtime

## [0.7.4] - 2024-04-05

- [fix] Fix collision detection for Sprite Groups
- [fix] Fix Sprite Groups deletion

## [0.7.3] - 2024-04-03

- [feature] Add ability to add colliders and overlaps to Sprite Groups
- [feature] Change default map for Platform object

## [0.7.2] - 2024-03-22

### Changed

- [fix] Remove canvas from run dependencies

## [0.7.0] - 2022-08-18

### Changed

- [feature] Robot now derives from Walker: it can fall and jump

### Added

- [feature] declick.wait suspends Declick for a given amount of time (in miliseconds)
- [feature] Add clear method to remove all created objects, keeping the scene active and resources loaded

## [0.6.4] - 2022-07-28

### Changed

- [fix] Clear resource list at reset

## [0.6.3] - 2022-05-30

### Changed

- [fix] Fix npm package

## [0.6.2] - 2022-05-30

### Changed

- [fix] Fix Sprite movement when body size is set

## [0.6.1] - 2022-05-14

### Changed

- [fix] Fix npm package

## [0.6.0] - 2022-05-13

### Added

- [feature] Support for optional parameters in Declick functions
- [feature] Add clock object
- [feature] Add sequence object
- [feature] Add button object
- [feature] Add stopwatch object
- [feature] Add text object
- [feature] Add builder object
- [feature] Add score object
- [feature] Add item object
- [fix] Fix sprite movement
- [dev] Add more tests for graphical objects
- [dev] Add demo files for builder, text, stopwatch, score and items

### Changed

- [fix] Display internationalized error messages when supplied arguments are wrong

## [0.5.0] - 2021-12-14

### Added

- [dev] Generate ES6 (.esm) module

### Changed

- [dev] Rewrite all build process using parcel

## [0.4.0] - 2021-12-06

### Added

- [feature] Add ability to load local transient resource (image, spritesheet or map)

### Changed

- [fix] Load map from file
- [fix] Unload map when graphic is reset

## [0.3.1] - 2021-12-04

### Changed

- [fix] Load local map

## [0.3.0] - 2021-12-04

### Added

- [feature] Add `getDeclickVariable(name)` to retrieve the value of a variable from Declick
- [feature] Load local resources (images, spritesheets, maps)
- [dev] Use [release-it](https://github.com/release-it/release-it) to handle releases
- [dev] Add changelog

### Changed

- [dev] Update node version to 16.13

## Format

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
