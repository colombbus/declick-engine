const description = 'Montre comment manipuler les objets texte et chronomètre'

const resources = {}

const code = `texte = new Texte("objet texte")
texte.définirTailleTexte(30)
texte.définirCouleur(255, 0, 0)
texte.définirTexte("objet texte 2")
texte.définirPolice("Times New Roman")

c = Chronomètre()
c.définirPosition(0, 40)
c.commencer()

répéter() {
	if (clavier.gauche) {
        c.pause()
	}
	if (clavier.droite) {
		c.continuer()
	}
	if (clavier.haut) {
		c.commencer()
	}
	if (clavier.bas) {
		c.arrêter()
	}
}
`

export { description, resources, code }
