const description = 'Montre comment créer et utiliser un maçon.'

const resources = {}

const code = `bob = new Maçon()

bob.avancer()
bob.poserSol()

bob.avancer()
bob.poserEntrée()

bob.descendre()
bob.poserSortie()

bob.reculer()
bob.poserMur()

bob.reculer()
bob.descendre()

bob.sePresser(true)
bob.poserLigne(1,1,1,1,1,1,1,1,1,1)
bob.descendre()
bob.poserLigne(1,0,0,0,0,0,0,0,0,1)
bob.descendre()
bob.poserLigne(1,2,2,2,2,2,0,0,2,1)
bob.monter()
bob.avancer(5)

bill = new Robot()
test = bob.poserItem("clé")
bob.avancer(5)
bill.peutAttraper(test)
bill.peutTomber(true)
bill.définirLongueurPas(40)
bill.ajouterBloc(bob.récupérerPlateforme())
bill.définirPosition(1,3)
bill.avancer(5)
`

export { description, resources, code }
