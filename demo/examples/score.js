const description = 'Montre comment manipuler un objet score'

const resources = {}

const code = `s = new Score()

score = s.récupérerScore()
a = Texte(score)
a.définirPosition(0, 20)

label = s.récupérerLabel()
b = Texte(label)
b.définirPosition(0, 40)

start = s.récupérerNombreScore()
c = Texte(start)
c.définirPosition(0, 60)
s.augmenterScore()
s.augmenterScore()

s.baisserScore()
score = s.récupérerScore()
d = Texte(start)
d.définirPosition(0, 80)
s.définirNombreScore(52)

s.définirNombreScore("lkdjml")
score = s.récupérerScore()
e = Texte(start)
e.définirPosition(0, 100)
s.définirNombreScore(52.9)
s.définirNombreScore(-52.9)
s.définirNombreScore(-52)
s.définirNombreScore(00)
`

export { description, resources, code }
