const description =
  "Montre comment utiliser un item avec un personnage Promeneur. (flèches pour avancer, espace pour sauter, 'a' pour lâcher l'item)"

const resources = {}

const code = `
p = new Plateforme()
p.poserBrique("entrance", 18, 8)
bob = new Promeneur()
bob.définirPosition(80,40)
bob.peutTomber()
bob.définirGravité(500)
bob.définirAmplitudeSaut(400)
bob.ajouterBloc(p)

function ouvrirPorte(moi, item) {
  p.poserBrique('exit',18,8)
}

it = new Item()
it.définirPosition(120, 40)
bob.siAttrape(it, ouvrirPorte)

répéter() {
	if (clavier.nouvelAppui("a")) {
    if (bob.estSurItem(it)) {
      bob.lâcher(it)
    }
	}
	if (clavier.nouvelAppui("espace")) {
		bob.sauter()
	}
	if (clavier.droite) {
		bob.avancerToujours()
	}
	else if (clavier.gauche) {
		bob.reculerToujours()
	} else {
		bob.arrêter()
	}
}
`

export { description, resources, code }
