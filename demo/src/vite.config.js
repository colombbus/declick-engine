import babel from 'vite-plugin-babel'
import { resolve } from 'path'
import { lingui } from '@lingui/vite-plugin'
import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    outDir: resolve(__dirname, '../dist/'),
    emptyOutDir: true,
  },
  publicDir: resolve(__dirname, '../examples/resources'),
  plugins: [babel(), lingui()],
})
