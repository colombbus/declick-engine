import CodeMirror from 'codemirror'
import 'codemirror/lib/codemirror.css'
import 'codemirror/theme/abcdef.css'
import 'codemirror/addon/dialog/dialog.css'
import 'codemirror/mode/javascript/javascript.js'
import 'codemirror/addon/dialog/dialog.js'
import './editor.css'
const examples = import.meta.glob('../examples/*.js', {
  eager: true,
})

const images = new Map()
const spriteSheets = new Map()
const maps = new Map()

const exampleNames = Object.keys(examples).map(fullName =>
  fullName.substring(12, fullName.length - 3),
)

export default {
  getExampleList() {
    return exampleNames.sort()
  },
  loadExample(name) {
    images.clear()
    spriteSheets.clear()
    maps.clear()
    // @ts-ignore
    const { resources, code, description } = examples[`../examples/${name}.js`]
    for (const [name, resource] of Object.entries(resources)) {
      switch (resource[0]) {
        case 'image':
          images.set(name, `${resource[1]}`)
          break
        case 'spritesheet':
          spriteSheets.set(name, [`${resource[1]}`, `${resource[2]}`])
          break
        case 'map':
          maps.set(name, `${resource[1]}`)
          break
      }
    }
    return { description, code, images, spriteSheets, maps }
  },
  initializeEditor(element, config) {
    return CodeMirror(element, config)
  },
}
