import DeclickRuntime from './runtime/main.js'
import DeclickObjects from './objects/loader.js'
import { i18n } from '@lingui/core'
const translations = import.meta.glob('../translations/translation.*.json', {
  query: '?lingui',
  eager: true,
  import: 'messages',
})

const _images = new Map()
const _spriteSheets = new Map()
const _maps = new Map()

function loadLocale(language) {
  const messages = translations[`../translations/translation.${language}.json`]
  i18n.load(language, messages)
  i18n.activate(language)
}

export default {
  addImageResource(image, name, local = false, transient = null) {
    _images.set(name, { image, local, transient })
  },
  addSpriteSheetResource(
    texture,
    atlas,
    name,
    local = false,
    transient = null,
  ) {
    _spriteSheets.set(name, { texture, atlas, local, transient })
  },
  addMapResource(map, name, local = false, transient = null) {
    _maps.set(name, { map, local, transient })
  },
  addImageResources(images, local = false, transient = null) {
    for (const name of images.keys()) {
      _images.set(name, { image: images.get(name), local, transient })
    }
  },
  addSpriteSheetResources(spriteSheets, local = false, transient = null) {
    for (const name of spriteSheets.keys()) {
      const spriteSheetData = spriteSheets.get(name)
      _spriteSheets.set(name, {
        texture: spriteSheetData[0],
        atlas: spriteSheetData[1],
        local,
        transient,
      })
    }
  },
  addMapResources(maps, local = false, transient = null) {
    for (const name of maps.keys()) {
      _maps.set(name, { map: maps.get(name), local, transient })
    }
  },
  initialize(container, forceCanvas = false, language = 'fr') {
    loadLocale(language)
    return DeclickRuntime.initDisplay(container, forceCanvas).then(() => {
      const objects = DeclickObjects.load()
      DeclickRuntime.initialize(objects)
    })
  },
  reset() {
    _images.clear()
    _spriteSheets.clear()
    _maps.clear()
    DeclickRuntime.reset()
  },
  startGraphics() {
    DeclickRuntime.addImageResources(_images)
    DeclickRuntime.addSpriteSheetResources(_spriteSheets)
    DeclickRuntime.addMapResources(_maps)
    return DeclickRuntime.startGraphics()
  },
  execute(code, programName = null) {
    return new Promise(resolve => {
      DeclickRuntime.executeCode(code, resolve, programName)
    })
  },
  parse(code) {
    return DeclickRuntime.parse(code)
  },
  getGraphics() {
    return DeclickRuntime.getGraphics()
  },
  getLastValue() {
    return DeclickRuntime.getLastValue()
  },
  getDeclickObject(name) {
    return DeclickRuntime.getDeclickObject(name)
  },
  getDeclickVariable(name) {
    return DeclickRuntime.getDeclickVariable(name)
  },
  setErrorHandler(handler) {
    DeclickRuntime.setErrorHandler(handler)
  },
  addExternalFunction(name, body) {
    DeclickRuntime.addExternalFunction(name, body)
  },
  destroy() {
    DeclickRuntime.destroy()
  },
  clear() {
    DeclickRuntime.clear()
  },
  setDragHandler(handler) {
    DeclickRuntime.setDragHandler(handler)
  },
  enableDrag() {
    DeclickRuntime.enableDrag()
  },
  disableDrag() {
    DeclickRuntime.disableDrag()
  },
  setProgramProvider(provider) {
    DeclickRuntime.setProgramProvider(provider)
  },
  suspend(graphics = true) {
    DeclickRuntime.suspend(false, graphics)
  },
  resume(graphics = true) {
    DeclickRuntime.resume(graphics)
  },
  getMethods(entity) {
    return DeclickObjects.getMethods(entity)
  },
  setMessageHandler(handler) {
    DeclickRuntime.setMessageHandler(handler)
  },
}
