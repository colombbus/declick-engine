export function detectError(object) {
  return (
    object instanceof Error ||
    (Object.getPrototypeOf(object).constructor &&
      Object.getPrototypeOf(Object.getPrototypeOf(object).constructor).name ===
        'Error')
  )
}
