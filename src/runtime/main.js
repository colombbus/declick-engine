import _data from './data.js'
import _scheduler from './scheduler.js'
import _parser from './parser.js'
import _graphics from './graphics.js'
import { i18n } from '@lingui/core'

let _interpreter = null
let _dragHandler = null
let _programProvider = null
let _messageHandler = null

export default {
  initialize(objects) {
    const instances = []
    _parser.setRepeatKeyword(i18n._('repeat'))

    objects.forEach(objectData => {
      if (objectData.instance) {
        const instance = new objectData.object()
        instance.setRuntime(this)
        _data.addInstance(objectData.name, instance, objectData.methods)
        instances.push(instance)
      } else {
        objectData.object.setRuntime(this)
        if (typeof objectData.object.setup === 'function') {
          objectData.object.setup()
        }
        _data.addClass(objectData.name, objectData.object, objectData.methods)
      }
    })

    _interpreter = _data.createInterpreter()
    _scheduler.initialize(_interpreter)

    instances.forEach(instance => {
      instance.dispatch('runtimeInitialized')
    })
  },

  getDeclickObjectName(reference) {
    if (reference.objectName == null) {
      reference.objectName = _data.findInterpreterObjectName(reference)
    }
    return reference.objectName
  },

  getDeclickObject(name) {
    return _data.findInterpreterObject(name)
  },

  getDeclickVariable(name) {
    return _data.findInterpreterVariable(name)
  },

  stop() {
    _data.stop()
    _scheduler.clear()
  },

  interrupt() {
    _interpreter.interrupt()
  },

  suspend(keepPriorityOn, suspendGraphics = false) {
    _scheduler.suspend(keepPriorityOn)
    if (suspendGraphics) {
      _graphics.suspend()
    }
  },

  resume(resumeGraphics = false) {
    _scheduler.resume()
    if (resumeGraphics) {
      _graphics.resume()
    }
  },

  clear(keepScheduler = false) {
    _data.clear()
    if (!keepScheduler) {
      _scheduler.clear()
    }
    _graphics.clear()
  },

  executeCode(code, callback, programName = null) {
    _scheduler.addStatements(_parser.parse(code, programName), null, callback)
  },

  executeStatements(statements, parameters, callback) {
    _scheduler.addStatements(statements, parameters, callback)
  },

  executePriorityCode(code) {
    _scheduler.addPriorityStatements(_parser.parse(code))
  },

  executePriorityStatements(statements, parameters, callback) {
    _scheduler.addPriorityStatements(statements, parameters, callback)
  },

  getStatements(code) {
    return _parser.parse(code)
  },

  getStatementsFromCommand(command) {
    if (typeof command === 'string' || command instanceof String) {
      // TODO: support getting commands from program name
      return _parser.parse(command).body
    } else if (
      typeof command === 'object' &&
      command.type !== undefined &&
      command.type === 'function'
    ) {
      return [_interpreter.createCallStatement(command)]
    }
  },

  getLastValue() {
    return _scheduler.getLastValue()
  },
  reset() {
    _scheduler.clear()
    _data.clear()
    _graphics.reset()
  },
  addObject(object) {
    _data.registerObject(object)
  },

  deleteObject(object) {
    _data.deleteObject(object)
  },

  addInstance(object) {
    _data.registerInstance(object)
  },

  initDisplay(container, displayType = false) {
    return _graphics.initialize(container, displayType)
  },

  resizeDisplay() {
    _graphics.resize()
  },

  getGraphics() {
    return _graphics
  },

  exposeProperties(object, properties) {
    _data.exposeProperties(object, properties)
  },

  parse(code) {
    return _parser.parse(code)
  },

  startGraphics() {
    return _graphics.start()
  },

  addImageResource(name, data) {
    if (data.local) {
      if (data.transient == true) {
        _graphics.addLocalTransientResource('image', name, data.image)
      } else {
        _graphics.addLocalResource('image', name, data.image)
      }
    } else {
      _graphics.addResource('image', name, data.image)
    }
  },

  addImageResources(data) {
    for (let [name, imageData] of data) {
      this.addImageResource(name, imageData)
    }
  },

  addSpriteSheetResource(name, data) {
    if (data.local) {
      if (data.transient === true) {
        _graphics.addLocalTransientResource(
          'atlas',
          name,
          data.texture,
          data.atlas,
        )
      } else {
        _graphics.addLocalResource('atlas', name, data.texture, data.atlas)
      }
    } else {
      _graphics.addResource('atlas', name, data.texture, data.atlas)
    }
  },

  addSpriteSheetResources(data) {
    for (let [name, spriteSheetData] of data) {
      this.addSpriteSheetResource(name, spriteSheetData)
    }
  },

  addMapResource(name, data) {
    if (data.local) {
      if (data.transient == true) {
        _graphics.addLocalTransientResource('map', name, data.map)
      } else {
        _graphics.addLocalResource('map', name, data.map)
      }
    } else {
      _graphics.addResource('map', name, data.map)
    }
  },

  addMapResources(data) {
    for (let [name, mapData] of data) {
      this.addMapResource(name, mapData)
    }
  },

  setErrorHandler(handler) {
    _scheduler.setErrorHandler(handler)
    _parser.setErrorHandler(handler)
  },

  enableDrag() {
    _graphics.enableDrag(event => {
      if (_dragHandler) {
        _dragHandler({
          object: _data.findInterpreterObjectName(event.object),
          x: event.x,
          y: event.y,
          method: i18n._('setLocation'),
        })
      }
    })
  },

  disableDrag() {
    _graphics.disableDrag()
  },

  setDragHandler(handler) {
    _dragHandler = handler
  },

  setProgramProvider(provider) {
    _programProvider = provider
  },

  async loadProgram(program) {
    const code = _programProvider(program)
    if (code == null) {
      throw new Error(i18n._('program {name} not found', { name: program }))
    }
    if (code instanceof Promise) {
      const value = await code
      if (value == null) {
        throw new Error(i18n._('program {name} not found', { name: program }))
      }
      this.executeCode(value, null, program)
    } else {
      this.executeCode(code, null, program)
    }
  },

  addExternalFunction(name, body) {
    _data.addFunction(name, body)
  },

  destroy() {
    _scheduler.clear()
    _data.clear()
    _data.reset()
    _graphics.destroy()
    _interpreter = null
  },

  setMessageHandler(handler) {
    _messageHandler = handler
  },

  message(text) {
    if (_messageHandler != null) {
      _messageHandler(text)
    } else {
      console.log(text)
    }
  },
}
