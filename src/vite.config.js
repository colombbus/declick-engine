import babel from 'vite-plugin-babel'
import { resolve } from 'path'
import { lingui } from '@lingui/vite-plugin'
import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'main.js'),
      formats: ['es'],
    },
    outDir: '../dist/',
    emptyOutDir: true,
  },
  plugins: [babel(), lingui()],
})
