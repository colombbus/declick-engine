import * as Phaser from 'phaser'

class ExtendedBody extends Phaser.Physics.Arcade.Body {
  update(delta) {
    super.update(delta)
    if (this._movement === 'target') {
      const referenceY = this.gravity.y === 0 ? this._targetY : this.position.y
      const vector = this._vectorBetween(
        this.position.x,
        this.position.y,
        this._targetX,
        referenceY,
      )
      const distance = this._squaredLength(vector)
      if (distance < 16 || this._dot(vector, this._oldTargetVector) < 0) {
        this._dx = this._targetX - this.prev.x
        this._dy = referenceY - this.prev.y
        this.newVelocity.set(this._dx, this._dy)
        this.reset(this._targetX - this.offset.x, referenceY - this.offset.y)
      } else {
        this._oldTargetVector = vector
        if (this.velocity.lengthSq() < 0.01) {
          this.velocity.set(0)
          this.reset(Math.round(this.position.x), Math.round(this.position.y))
          this._dx = this.position.x - this.prev.x
          this._dy = this.position.y - this.prev.y
          this.newVelocity.set(this._dx, this._dy)
        }
      }
    }
  }

  setMovement(movement) {
    this._movement = movement
  }

  initTargetMovement(targetX, targetY, targetVector) {
    this._targetX = targetX + this.offset.x
    this._targetY = targetY + this.offset.y
    this._oldTargetVector = this._vectorBetween(
      this.position.x,
      this.position.y,
      this._targetX,
      this._targetY,
    )
  }

  _vectorBetween(x1, y1, x2, y2) {
    return [x1 - x2, y1 - y2]
  }

  _squaredLength(vector) {
    const [x, y] = vector
    return x * x + y * y
  }

  _dot(vector1, vector2) {
    const [x1, y1] = vector1
    const [x2, y2] = vector2
    return x1 * x2 + y1 * y2
  }
}

class ExtendedSprite extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y, texture, frame) {
    super(scene, x, y, texture, frame)
    this.body = new ExtendedBody(scene.physics.world, this)
  }
}

export default ExtendedSprite
