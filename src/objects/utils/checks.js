import { i18n } from '@lingui/core'

let _types = null

const initializeChecks = function() {
  _types = {
    integer: {
      check: value => Number.isInteger(value),
      typeMessage: value => i18n._('integer value expected {value}', { value }),
      missingMessage: i18n._('missing integer argument'),
    },
    string: {
      check: value => typeof value === 'string' || value instanceof String,
      typeMessage: value => i18n._('string value expected {value}', { value }),
      missingMessage: i18n._('missing string argument'),
    },
    array: {
      check: value => Array.isArray(value),
      typeMessage: value => i18n._('array value expected {value}', { value }),
      missingMessage: i18n._('missing array argument'),
    },
    number: {
      check: value => typeof value === 'number' && isFinite(value),
      typeMessage: value => i18n._('number value expected {value}', { value }),
      missingMessage: i18n._('missing number argument'),
    },
    boolean: {
      check: value => typeof value === 'boolean',
      typeMessage: value => i18n._('boolean value expected {value}', { value }),
      missingMessage: i18n._('missing boolean argument'),
    },
    object: {
      check: value =>
        typeof value === 'object' && value._declickId_ !== undefined,
      typeMessage: value => i18n._('object value expected {value}', { value }),
      missingMessage: i18n._('missing object argument'),
    },
    function: {
      check: value =>
        typeof value === 'object' &&
        value.type !== undefined &&
        value.type === 'function',
      typeMessage: value =>
        i18n._('function value expected {value}', { value }),
      missingMessage: i18n._('missing function argument'),
    },
    canvas: {
      check: value => value instanceof HTMLCanvasElement,
      typeMessage: value => i18n._('canvas value expected {value}', { value }),
      missingMessage: i18n._('missing canvas argument'),
    },
    any: {
      check: () => true,
      missingMessage: i18n._('missing argument'),
    },
  }
}

// initialization without i118n for testing purpose
initializeChecks()

const _checkType = function(type, value) {
  return _types[type].check(value)
}

const _checkTypes = function(types, value) {
  return types.find(type => _checkType(type, value)) !== undefined
}

const _getTypeErrorMessage = function(type, value) {
  return _types[type].typeMessage(value)
}

const _getMissingErrorMessage = function(types, skip, length) {
  let messages = []
  for (let i = types.length - skip - length; i < types.length - skip; i++) {
    messages.push(_types[types[i]].missingMessage)
  }
  return messages.join('\n')
}

const checkArguments = function(types, optionalParameters = 0) {
  return function(target, name, descriptor) {
    const originalMethod = descriptor.value
    descriptor.value = function(...args) {
      types.forEach((type, index) => {
        const allowedTypes = type.split('|')
        const argument = args[index]
        if (argument !== undefined) {
          // required to handle the case where inner function calls are made
          if (argument.type && argument.type === 'undefined') {
            args[index] = undefined
          } else if (
            argument !== undefined &&
            !_checkTypes(allowedTypes, argument)
          ) {
            throw {
              declickObjectError: _getTypeErrorMessage(
                allowedTypes[0],
                argument,
              ),
            }
          }
        }
      })
      const missingArgsLength = types.length - args.length - optionalParameters
      if (missingArgsLength > 0) {
        throw {
          declickObjectError: _getMissingErrorMessage(
            types,
            optionalParameters,
            missingArgsLength,
          ),
        }
      }
      return originalMethod.apply(this, args)
    }
    return descriptor
  }
}
export { initializeChecks, checkArguments }
