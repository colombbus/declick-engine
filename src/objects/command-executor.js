import 'reflect-metadata'
import { checkArguments } from './utils/checks.js'

import BaseClass from './base-class.js'

/*

    CommandExecutor is a mother class for objects that
    execute commands. It has general methods for handling
    these commands.

*/

class CommandExecutor extends BaseClass {
  constructor() {
    super()

    this._commands = []
    this._running = false
    this._timeout = null
    this._frozen = false
    this._wasRunning = false
    this._loopFlag = true
    this._initialDelay = 0
    this._delay = 1000
  }

  @Reflect.metadata('translated', 'addCommand')
  @Reflect.metadata('help', 'addCommand_help')
  @checkArguments(['string|function'])
  addCommand(command) {
    const callStatement = this._runtime.getStatementsFromCommand(command)
    this._commands.push(callStatement)
  }

  @Reflect.metadata('translated', 'setDelay')
  @Reflect.metadata('help', 'setDelay_help')
  @checkArguments(['integer'])
  setDelay(delay) {
    this._delay = delay
    if (!this._initialDelay) {
      this.setInitialDelay(delay)
    }
  }

  @Reflect.metadata('translated', 'setInitialDelay')
  @Reflect.metadata('help', 'setInitialDelay_help')
  @checkArguments(['integer'])
  setInitialDelay(delay) {
    this._initialDelay = delay
  }

  @Reflect.metadata('translated', 'removeCommands')
  @Reflect.metadata('help', 'removeCommands_help')
  removeCommands() {
    this._commands = []
  }

  @Reflect.metadata('translated', 'start')
  @Reflect.metadata('help', 'start_help')
  start() {
    if (this._running) {
      // Command executor is already running: restart it
      this.stop()
    }
    this._running = true
    this._timeout = window.setTimeout(() => {
      this._execute()
    }, this._initialDelay)
  }

  @Reflect.metadata('translated', 'stop')
  @Reflect.metadata('help', 'stop_help')
  stop() {
    this._running = false
    if (this._timeout !== null) {
      window.clearTimeout(this._timeout)
      this._timeout = null
    }
  }

  _execute() {
    this.dispatch('execute')
    if (this._loopFlag && this._running) {
      this._timeout = window.setTimeout(() => {
        this._execute()
      }, this._delay)
    } else {
      this.stop()
    }
  }

  @Reflect.metadata('translated', 'loop')
  @Reflect.metadata('help', 'loop_help')
  @checkArguments(['boolean'])
  loop(value) {
    this._loopFlag = value
  }

  @checkArguments(['boolean'])
  _freeze(value) {
    // Already in the same state
    if (value === this._frozen) {
      return
    }

    if (value) {
      this._wasRunning = this._running
      this.stop()
      this._frozen = true
    } else {
      if (this._wasRunning) {
        this.start()
      }
      this._frozen = false
    }
  }

  delete() {
    this.stop()
    super.delete()
  }
}

export default CommandExecutor
