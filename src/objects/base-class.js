import 'reflect-metadata'
import EventsInterface from './events-interface.js'

@Reflect.metadata('instance', false)
class BaseClass extends EventsInterface {
  static setRuntime(runtime) {
    this._runtime = runtime
  }

  constructor() {
    super()
    // @ts-ignore
    this._runtime = this.constructor._runtime
    if (this._runtime) {
      this._runtime.addObject(this)
    }
    this.addListener('delete', () => {
      if (this._runtime) {
        this._runtime.deleteObject(this)
      }
    })
  }

  @Reflect.metadata('translated', 'delete')
  @Reflect.metadata('help', 'delete_help')
  delete() {
    this.dispatch('delete')
  }

  throwError(message) {
    throw {
      declickObjectError: message,
    }
  }
}

export default BaseClass
