import GraphicClass from '../graphic-class.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

@Reflect.metadata('translated', 'Text')
class Text extends GraphicClass {
  constructor(label) {
    super()
    this._label = label !== undefined ? label : ''
    this._object = this._graphics
      .getScene()
      .add.text(0, 0, this._label, { color: '#000000' })
    this._object.setOrigin(0)
    this.dispatch('created')
  }

  @Reflect.metadata('translated', 'setText')
  @Reflect.metadata('help', 'setText_help')
  @checkArguments(['string'])
  setText(label) {
    this._label = label
    this._object.setText(label)
  }

  @Reflect.metadata('translated', 'getText')
  @Reflect.metadata('help', 'getText_help')
  getText() {
    return this._label
  }

  @Reflect.metadata('translated', 'setTextSize')
  @Reflect.metadata('help', 'setTextSize_help')
  @checkArguments(['integer'])
  setTextSize(size) {
    this._object.setFontSize(size)
  }

  @Reflect.metadata('translated', 'setColor')
  @Reflect.metadata('help', 'setColor_help')
  @checkArguments(['integer', 'integer', 'integer'])
  setColor(red, green, blue) {
    let color = Phaser.Display.Color.RGBToString(red, green, blue)
    this._object.setColor(color)
  }

  @Reflect.metadata('translated', 'setFont')
  @Reflect.metadata('help', 'setFont_help')
  @checkArguments(['string'])
  setFont(font_name) {
    this._object.setFontFamily(font_name)
  }

  setLocation(x, y) {
    super.setLocation(x, y)
    this._object.setPosition(x, y)
  }
}

export default Text
