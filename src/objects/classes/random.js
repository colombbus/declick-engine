import BaseClass from '../base-class.js'
import { checkArguments } from '../utils/checks.js'

@Reflect.metadata('translated', 'Random')
class Random extends BaseClass {
  constructor() {
    super()
  }

  @Reflect.metadata('translated', 'throwDice')
  @Reflect.metadata('help', 'throwDice_help')
  @checkArguments(['integer'], 1)
  throwDice(max = 6) {
    return Math.floor(Math.random() * max) + 1
  }
}

export default Random
