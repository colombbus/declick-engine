import Text from './text.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

@Reflect.metadata('translated', 'Score')
class Score extends Text {
  constructor(label, score) {
    super(label)
    this._label = label !== undefined ? label : 'Score'
    this._score = score !== undefined ? score : 0
    this._setText(this._label, this._score)
  }

  _setText(label, score) {
    this._object.setText(`${label} : ${score}`)
  }

  @Reflect.metadata('translated', 'eraseScoreNumber')
  @Reflect.metadata('help', 'eraseScoreNumber_help')
  eraseScoreNumber() {
    this._score = 0
    this._setText(this._label, this._score)
  }

  @Reflect.metadata('translated', 'increaseScore')
  @Reflect.metadata('help', 'increaseScore_help')
  @checkArguments(['integer'], 1)
  increaseScore(step) {
    this._score += step !== undefined ? step : 1
    this._setText(this._label, this._score)
  }

  @Reflect.metadata('translated', 'decreaseScore')
  @Reflect.metadata('help', 'decreaseScore_help')
  @checkArguments(['integer'], 1)
  decreaseScore(step) {
    this._score -= step !== undefined ? step : 1
    this._setText(this._label, this._score)
  }

  @Reflect.metadata('translated', 'setLabel')
  @Reflect.metadata('help', 'setLabel_help')
  @checkArguments(['string'])
  setLabel(label) {
    if (typeof label === 'undefined') {
      this._label = 'Score'
    } else this._label = label
    this._setText(this._label, this._score)
  }

  @Reflect.metadata('translated', 'getLabel')
  @Reflect.metadata('help', 'getLabel_help')
  getLabel() {
    return this._label
  }

  @Reflect.metadata('translated', 'setScoreNumber')
  @Reflect.metadata('help', 'setScoreNumber_help')
  @checkArguments(['integer'])
  setScoreNumber(number) {
    this._score = number
    this._setText(this._label, this._score)
  }

  @Reflect.metadata('translated', 'getScoreNumber')
  @Reflect.metadata('help', 'getScoreNumber_help')
  getScoreNumber() {
    return this._score.toString()
  }

  @Reflect.metadata('translated', 'getScore')
  @Reflect.metadata('help', 'getScore_help')
  getScore() {
    return `${this._label} : ${this._score}`
  }
}

export default Score
