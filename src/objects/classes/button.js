import { i18n } from '@lingui/core'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

import GraphicClass from '../graphic-class.js'

const DEFAULT_FILL_COLOR = '#4d8cc2'
const DEFAULT_STROKE_COLOR = '#0d4c82'
const DEFAULT_TEXT_COLOR = '#ffffff'
const FILL_COLOR_ACTIVE = '#3276b1'
const STROKE_COLOR_ACTIVE = '#0d4c82'
const TEXT_COLOR_ACTIVE = '#ffffff'

@Reflect.metadata('translated', 'Button')
class Button extends GraphicClass {
  constructor(label = i18n._('Button'), x = 0, y = 0) {
    super()

    this.commands = new Array()

    this._textColor = DEFAULT_TEXT_COLOR
    this._strokeColor = DEFAULT_STROKE_COLOR
    this._fillColor = DEFAULT_FILL_COLOR

    this._textColorActive = TEXT_COLOR_ACTIVE
    this._strokeColorActive = STROKE_COLOR_ACTIVE
    this._fillColorActive = FILL_COLOR_ACTIVE

    const scene = this._graphics.getScene()

    this._object = scene.add
      .text(x, y, label)
      .setOrigin(0)
      .setPadding(10)
      .setStyle({
        color: this._textColor,
        stroke: this._strokeColor,
        backgroundColor: this._fillColor,
      })
      .setInteractive({ useHandCursor: true })
      .on('pointerdown', () => this._runCommands())
      .on('pointerover', () =>
        this._object.setStyle({
          color: this._textColorActive,
          stroke: this._strokeColorActive,
          backgroundColor: this._fillColorActive,
        }),
      )
      .on('pointerout', () =>
        this._object.setStyle({
          color: this._textColor,
          stroke: this._strokeColor,
          backgroundColor: this._fillColor,
        }),
      )
    this.dispatch('created')
  }

  @Reflect.metadata('translated', 'setText')
  @Reflect.metadata('help', 'setText_help')
  @checkArguments(['string'])
  setText(label) {
    this._object.setText(label)
  }

  @Reflect.metadata('translated', 'setTextSize')
  @Reflect.metadata('help', 'setTextSize_help')
  @checkArguments(['integer'])
  setTextSize(size) {
    this._object.setFontSize(size)
  }

  @Reflect.metadata('translated', 'setColor')
  @Reflect.metadata('help', 'setColor_help')
  @checkArguments(['integer', 'integer', 'integer'])
  setColor(red, green, blue) {
    let color = Phaser.Display.Color.RGBToString(red, green, blue)

    let redActive = Math.max(red - 40, 0)
    let greenActive = Math.max(green - 40, 0)
    let blueActive = Math.max(blue - 40, 0)
    let colorActive = Phaser.Display.Color.RGBToString(
      redActive,
      greenActive,
      blueActive,
    )

    this._fillColor = color
    this._fillColorActive = colorActive

    this._object.setBackgroundColor(color)
  }

  @Reflect.metadata('translated', 'setTextColor')
  @Reflect.metadata('help', 'setTextColor_help')
  @checkArguments(['integer', 'integer', 'integer'])
  setTextColor(red, green, blue) {
    let color = Phaser.Display.Color.RGBToString(red, green, blue)

    let redActive = Math.max(red - 40, 0)
    let greenActive = Math.max(green - 40, 0)
    let blueActive = Math.max(blue - 40, 0)
    let colorActive = Phaser.Display.Color.RGBToString(
      redActive,
      greenActive,
      blueActive,
    )

    this._textColor = color
    this._textColorActive = colorActive

    this._object.setColor(color)
  }

  @Reflect.metadata('translated', 'addCommand')
  @Reflect.metadata('help', 'addCommand_help')
  @checkArguments(['string|function'])
  addCommand(command) {
    const callStatement = this._runtime.getStatementsFromCommand(command)
    this.commands.push(callStatement)
  }

  _runCommands() {
    this.commands.forEach(callStatements => {
      this._runtime.executePriorityStatements(callStatements)
    })
  }

  @Reflect.metadata('translated', 'removeCommands')
  @Reflect.metadata('help', 'removeCommands_help')
  removeCommands() {
    this.commands.length = 0
  }

  setLocation(x, y) {
    super.setLocation(x, y)
    this._object.setPosition(x, y)
  }

  getX() {
    if (this._object) {
      return this._object.x
    } else {
      return -1
    }
  }

  getY() {
    if (this._object) {
      return this._object.y
    } else {
      return -1
    }
  }
}

export default Button
