import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'
import { i18n } from '@lingui/core'

import CommandExecutor from '../command-executor.js'

/*

    Sequence is an object that inherits from CommandExecutor.
    This object handles the saving and execution of commands in a sequence
    manner, with a possible delay between each too commands.
    It can execute commands endlessly or only one time.

*/

@Reflect.metadata('translated', 'Sequence')
class Sequence extends CommandExecutor {
  static MINIMUM_LOOP = 100
  static TYPE_COMMAND = 0x01
  static TYPE_DELAY = 0x02

  constructor() {
    super()
    this._index = 0
    this._initialDelay = 0
    this._delay = 0
    this._sequenceLoop = false
    this.addListener('execute', this._executeCommand)
  }

  addCommand(command) {
    const callStatement = this._runtime.getStatementsFromCommand(command)
    this._commands.push({
      type: Sequence.TYPE_COMMAND,
      value: callStatement,
    })
  }

  @Reflect.metadata('translated', 'addDelay')
  @Reflect.metadata('help', 'addDelay_help')
  @checkArguments(['integer'])
  addDelay(delay) {
    this._commands.push({
      type: Sequence.TYPE_DELAY,
      value: delay,
    })
  }

  _executeCommand() {
    if (this._commands.length === 0) {
      return
    }
    if (this._index >= this._commands.length) {
      this._index = 0
      if (!this._sequenceLoop) {
        this.stop()
        return
      }
    }

    const command = this._commands[this._index]

    if (command.type === Sequence.TYPE_DELAY) {
      // if command is a delay command, execute the next action after
      // delay milliseconds else execute immediately
      const delay = command.value
      this._delay = delay
      this._initialDelay = delay
    } else {
      const callStatements = command.value
      this._runtime.executePriorityStatements(callStatements)
      this._delay = 0
      this._initialDelay = 0
    }
    this._index++
  }

  @Reflect.metadata('translated', 'pause')
  @Reflect.metadata('help', 'pause_help')
  pause() {
    this._freeze(true)
  }

  @Reflect.metadata('translated', 'continue')
  @Reflect.metadata('help', 'continue_help')
  continue() {
    this._freeze(false)
  }

  @checkArguments(['boolean'])
  loop(value) {
    if (value) {
      // WARNING: in order to prevent Declick from freezing, check that there is at least a total delay of MINIMUM_LOOP in actions
      let totalDelay = this._commands
        .filter(command => command.type === Sequence.TYPE_DELAY)
        .reduce(
          (delay_acc, delay_command) => delay_acc + delay_command.value,
          0,
        )

      if (totalDelay < Sequence.MINIMUM_LOOP) {
        this.throwError(
          i18n._('freeze warning {minimum}', {
            minimum: Sequence.MINIMUM_LOOP,
          }),
        )
      }
    }
    this._sequenceLoop = value
  }

  start() {
    super.start()
    if (!this._frozen) {
      this._index = 0
    }
  }
}

export default Sequence
