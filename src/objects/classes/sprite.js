import GraphicClass from '../graphic-class.js'
import Item from './item.js'
import List from './list.js'
import 'reflect-metadata'
import robotData from '../resources/robot.json'
import robotTexture from '../resources/robot.png'
import { checkArguments } from '../utils/checks.js'
import ExtendedSprite from '../utils/extended-sprite.js'

const DEFAULT_SPEED = 200

@Reflect.metadata('translated', 'Sprite')
class Sprite extends GraphicClass {
  static _texture = 'sprite_default_animation'

  static setupDone = false

  static setup() {
    if (!this.setupDone) {
      super.setup()
      this._graphics.addLocalResource(
        'atlas',
        'sprite_default_animation',
        robotTexture,
        robotData,
      )
      this.setupDone = true
    }
  }

  _buildObject() {
    const scene = this._graphics.getScene()
    // instead of calling scene.physics.add.sprite(0, 0, this._texture)
    // we create our own Arcade Sprite, in order to use an extended body
    // the following code is taken from Phaser.Physics.Arcade.Factory#sprite
    this._object = new ExtendedSprite(scene, 0, 0, this._texture)
    scene.add.existing(this._object)
    scene.physics.world.enableBody(this._object)
    // end of Arcade Sprite creation
    this._object.setOrigin(0)
    this._object.body.reset(0, 0)
    const jsonData = scene.cache.json.get(`${this._texture}_anims`)
    if (jsonData !== undefined) {
      Object.keys(jsonData).forEach(anim => {
        if (
          jsonData[anim] !== undefined &&
          jsonData[anim].frames !== undefined
        ) {
          const moveData = jsonData[anim]
          const key = `${this._texture}_${anim}`
          if (!scene.anims.exists(key)) {
            scene.anims.create({
              key: key,
              frames: moveData.frames.map(name => ({
                key: this._texture,
                frame: name,
              })),
              repeat: -1,
              duration: moveData.duration ? moveData.duration : 1500,
            })
          }
        }
      })
      this.addListener('movementChange', movement => {
        this._setAnimation(movement)
      })
      if (scene.anims.exists(`${this._texture}_face`)) {
        this._object.play(`${this._texture}_face`)
      }
    }
    this.dispatch('created')
  }

  constructor(texture) {
    super()
    this._object = null
    //@ts-ignore
    this._texture = texture !== undefined ? texture : this.constructor._texture
    this._vX = DEFAULT_SPEED
    this._vY = DEFAULT_SPEED
    this._targetX = 0
    this._targetY = 0
    this._movement = 'stop'
    this._oldTargetDistance = 0
    this._state = null
    this._caughtObjects = new Set()
    this._buildObject()
    this.addListener('stop', () => {
      this.stop()
    })
  }

  _setAnimation(movement) {
    let animation
    if (movement === 'target') {
      if (this._targetX > this._object.x) {
        animation = 'right'
      } else if (this._targetX < this._object.x) {
        animation = 'left'
      } else {
        animation = 'face'
      }
    } else {
      const animations = {
        stop: 'face',
        forward: 'right',
        backward: 'left',
        upward: 'face',
        downward: 'face',
      }
      animation = animations[movement]
    }
    const anims = this._graphics.getScene().anims
    let animName
    if (
      this._state !== null &&
      anims.exists(`${this._texture}_${this._state}_${animation}`)
    ) {
      animName = `${this._texture}_${this._state}_${animation}`
    } else if (anims.exists(`${this._texture}_${animation}`)) {
      animName = `${this._texture}_${animation}`
    }
    if (animName) {
      this._object.anims.chain()
      this._object.anims.chain(animName)
      if (this._object.anims.getName() === animName) {
        this._object.anims.stopOnFrame(
          this._object.anims.currentAnim.getFrameAt(0),
        )
      } else {
        this._object.anims.stopAfterDelay(100)
      }
    }
  }

  tick(delta) {
    if (
      this._movement === 'target' &&
      this._object.body.velocity.x === 0 &&
      this._object.body.velocity.y === 0
    ) {
      this._graphics.getScene().game.events.once('postrender', () => {
        this._setMovement('stop')
      })
    }
  }

  _setMovement(newMovement) {
    const oldMovement = this._movement
    this._movement = newMovement
    if (newMovement !== oldMovement || newMovement === 'target') {
      this.dispatch('movementChange', newMovement)
    }
    this._object.body.setMovement(newMovement)
  }

  _initTargetMovement() {
    if (this._movement !== 'target') {
      this._targetX = this._object.x
      this._targetY = this._object.y
    }
  }

  _moveToTarget() {
    const physics = this._graphics.getScene().physics
    physics.moveTo(
      this._object,
      this._targetX,
      this._targetY,
      Math.max(this._vX, this._vY),
    )
    this._object.body.initTargetMovement(this._targetX, this._targetY)
    this._setMovement('target')
  }

  _setVelocity(vx, vy) {
    this._object.body.setVelocity(vx, vy)
  }

  @Reflect.metadata('translated', 'moveForward')
  @Reflect.metadata('help', 'moveForward_help')
  @checkArguments(['integer'])
  moveForward(distance) {
    this._initTargetMovement()
    this._targetX += distance
    this._moveToTarget()
  }

  @Reflect.metadata('translated', 'moveBackward')
  @Reflect.metadata('help', 'moveBackward_help')
  @checkArguments(['integer'])
  moveBackward(distance) {
    this._initTargetMovement()
    this._targetX -= distance
    this._moveToTarget()
  }

  @Reflect.metadata('translated', 'moveUpward')
  @Reflect.metadata('help', 'moveUpward_help')
  @checkArguments(['integer'])
  moveUpward(distance) {
    this._initTargetMovement()
    this._targetY -= distance
    this._moveToTarget()
  }

  @Reflect.metadata('translated', 'moveDownward')
  @Reflect.metadata('help', 'moveDownward_help')
  @checkArguments(['integer'])
  moveDownward(distance) {
    this._initTargetMovement()
    this._targetY += distance
    this._moveToTarget()
  }

  @Reflect.metadata('translated', 'stop')
  @Reflect.metadata('help', 'stop_help')
  stop() {
    this._setVelocity(0, 0)
    this._setMovement('stop')
  }
  @Reflect.metadata('translated', 'moveAlwaysForward')
  @Reflect.metadata('help', 'moveAlwaysForward_help')
  moveAlwaysForward() {
    this._setVelocity(this._vX, 0)
    this._setMovement('forward')
  }

  @Reflect.metadata('translated', 'moveAlwaysBackward')
  @Reflect.metadata('help', 'moveAlwaysBackward_help')
  moveAlwaysBackward() {
    this._setVelocity(-this._vX, 0)
    this._setMovement('backward')
  }

  @Reflect.metadata('translated', 'moveAlwaysUpward')
  @Reflect.metadata('help', 'moveAlwaysUpward_help')
  moveAlwaysUpward() {
    this._setVelocity(0, -this._vY)
    this._setMovement('upward')
  }

  @Reflect.metadata('translated', 'moveAlwaysDownward')
  @Reflect.metadata('help', 'moveAlwaysDownward_help')
  moveAlwaysDownward() {
    this._setVelocity(0, this._vY)
    this._setMovement('downward')
  }

  @Reflect.metadata('translated', 'ifCollisionWith')
  @Reflect.metadata('help', 'ifCollisionWith_help')
  @checkArguments(['object', 'function|string', 'any'], 1)
  ifCollisionWith(object, command, optionalParameter) {
    const callStatements = this._runtime.getStatementsFromCommand(command)
    object.addCollider(
      this._object,
      (me, who) => {
        this._runtime.executePriorityStatements(callStatements, [
          me.getData('declickObject'),
          who.getData('declickObject'),
        ])
      },
      optionalParameter,
    )
  }

  @Reflect.metadata('translated', 'addBlock')
  @Reflect.metadata('help', 'addBlock_help')
  @checkArguments(['object'])
  addBlock(block) {
    block.addCollider(this._object)
  }

  @Reflect.metadata('translated', 'ifOverlapWith')
  @Reflect.metadata('help', 'ifOverlapWith_help')
  @checkArguments(['object', 'string|function', 'any'], 1)
  ifOverlapWith(object, command, optionalParameter) {
    const callStatements = this._runtime.getStatementsFromCommand(command)
    object.addOverlap(
      this._object,
      (me, who) => {
        this._runtime.executePriorityStatements(callStatements, [
          me.getData('declickObject'),
          who.getData('declickObject'),
        ])
      },
      optionalParameter,
    )
  }

  @Reflect.metadata('translated', 'mayBeMoved')
  @Reflect.metadata('help', 'mayBeMoved_help')
  @checkArguments(['boolean'], 1)
  mayBeMoved(value = true) {
    this._object.setImmovable(!value)
  }

  setLocation(x, y) {
    super.setLocation(x, y)
    this._targetX = x
    this._targetY = y
    this._object.body.reset(x, y)
  }

  @Reflect.metadata('translated', 'isMovingForward')
  @Reflect.metadata('help', 'isMovingForward_help')
  isMovingForward() {
    return (
      this._movement === 'forward' ||
      (this._movement === 'target' && this._targetX > this._object.body.x)
    )
  }

  @Reflect.metadata('translated', 'isMovingBackward')
  @Reflect.metadata('help', 'isMovingBackward_help')
  isMovingBackward() {
    return (
      this._movement === 'backward' ||
      (this._movement === 'target' && this._targetX < this._object.body.x)
    )
  }

  @Reflect.metadata('translated', 'isMovingUpward')
  @Reflect.metadata('help', 'isMovingUpward_help')
  isMovingUpward() {
    return (
      this._movement === 'upward' ||
      (this._movement === 'target' && this._targetY < this._object.body.y)
    )
  }

  @Reflect.metadata('translated', 'isMovingDownward')
  @Reflect.metadata('help', 'isMovingDownward_help')
  isMovingDownward() {
    return (
      this._movement === 'downward' ||
      (this._movement === 'target' && this._targetY > this._object.body.y)
    )
  }

  addCollider(object, handler) {
    this._graphics
      .getScene()
      .physics.add.collider(object, this._object, handler)
  }

  addOverlap(object, handler) {
    this._graphics.getScene().physics.add.overlap(object, this._object, handler)
  }

  @Reflect.metadata('translated', 'setVelocity')
  @Reflect.metadata('help', 'setVelocity_help')
  @checkArguments(['integer'])
  setVelocity(value) {
    this._vX = value
    this._vY = value
  }

  @Reflect.metadata('translated', 'setDisplaySize')
  @Reflect.metadata('help', 'setDisplaySize_help')
  @checkArguments(['integer', 'integer'])
  setDisplaySize(width, height) {
    this._object.setDisplaySize(width, height)
  }

  @Reflect.metadata('translated', 'setBodySize')
  @Reflect.metadata('help', 'setBodySize_help')
  @checkArguments(['integer', 'integer'])
  setBodySize(width, height) {
    this._object.setBodySize(width, height)
  }

  @Reflect.metadata('translated', 'setState')
  @Reflect.metadata('help', 'setState_help')
  @checkArguments(['string'], 1)
  setState(state = null) {
    this._state = state
    this._setAnimation(this._movement)
  }

  @Reflect.metadata('translated', 'mayCatch')
  @Reflect.metadata('help', 'mayCatch_help')
  @checkArguments(['object'])
  mayCatch(object) {
    this.ifCatch(object)
  }

  @Reflect.metadata('translated', 'ifCatch')
  @Reflect.metadata('help', 'ifCatch_help')
  @checkArguments(['object', 'function|string'], 1)
  ifCatch(object, command) {
    if (object instanceof Item) {
      object.addPossibleCatcher(this, command)
    }
  }

  @Reflect.metadata('translated', 'isOnItem')
  @Reflect.metadata('help', 'isOnItem_help')
  @checkArguments(['object'], 1)
  isOnItem(object = null) {
    if (object === null) {
      return this._graphics
        .getScene()
        .physics.overlap(this._object, [...Item.itemGroup])
    } else {
      return this._graphics
        .getScene()
        .physics.overlap(object._object, this._object)
    }
  }

  @Reflect.metadata('translated', 'drop')
  @Reflect.metadata('help', 'drop_help')
  @checkArguments(['object'])
  drop(object) {
    if (object.getCatcher() === this) {
      object.drop()
    }
  }

  enableDrag() {
    if (this._object) {
      this.stop()
      this._object.anims.pause()
    }
    return super.enableDrag()
  }

  disableDrag() {
    if (this._object) {
      this._object.anims.resume()
    }
    return super.disableDrag()
  }

  caught(object) {
    this._caughtObjects.add(object)
  }

  dropped(object) {
    this._caughtObjects.delete(object)
  }

  @Reflect.metadata('translated', 'getCaughtItems')
  @Reflect.metadata('help', 'getCaughtItems_help')
  getCaughtItems() {
    return new List([...this._caughtObjects])
  }

  @Reflect.metadata('translated', 'hasItem')
  @Reflect.metadata('help', 'hasItem_help')
  @checkArguments(['string'], 1)
  hasItem(name = null) {
    if (name) {
      return (
        [...this._caughtObjects].find(item => item.getName() === name) !==
        undefined
      )
    } else {
      return this._caughtObjects.size > 0
    }
  }

  @Reflect.metadata('translated', 'catchItem')
  @Reflect.metadata('help', 'catchItem_help')
  @checkArguments(['object'], 1)
  catchItem(object = null) {
    if (object === null) {
      let item = {}
      const result = this._graphics
        .getScene()
        .physics.overlap(this._object, [...Item.itemGroup], (obj1, obj2) => {
          if (obj1 === this._object) {
            item = obj2.getData('declickObject')
          } else {
            item = obj1.getData('declickObject')
          }
        })
      if (result && item instanceof Item) {
        item.setCatcher(this)
      }
    }
    if (object && object instanceof Item) {
      object.setCatcher(this)
    }
  }
}

export default Sprite
