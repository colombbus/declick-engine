import Sprite from './sprite.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

const DEFAULT_GRAVITY = 500
const DEFAULT_JUMP = 400

@Reflect.metadata('translated', 'Walker')
class Walker extends Sprite {
  constructor(texture) {
    super(texture)
    this._gravityY = DEFAULT_GRAVITY
    this._jumpAmount = DEFAULT_JUMP
    this._mayFall = false
    this._object.setBounce(0.1)
    //this._object.setCollideWorldBounds(true)
  }

  _setVelocity(vx, vy) {
    this._object.setVelocityX(vx)
    if (!this._mayFall) {
      this._object.setVelocityY(vy)
    }
  }

  /*_distanceBetween(x1, y1, x2, y2) {
    if (this._mayFall) {
      let dx = x1 - x2
      return Math.sqrt(dx * dx)
    } else {
      return super._distanceBetween(x1, y1, x2, y2)
    }
  }*/

  @Reflect.metadata('translated', 'mayFall')
  @Reflect.metadata('help', 'mayFall_help')
  @checkArguments(['boolean'], 1)
  mayFall(value = true) {
    this._mayFall = value
    if (this._mayFall) {
      this._object.setGravityY(this._gravityY)
    } else {
      this._object.setGravityY(0)
    }
  }

  @Reflect.metadata('translated', 'setGravity')
  @Reflect.metadata('help', 'setGravity_help')
  @checkArguments(['integer'])
  setGravity(value) {
    this._gravityY = value
    if (this._mayFall) {
      this._object.setGravityY(value)
    }
  }

  @Reflect.metadata('translated', 'jump')
  @Reflect.metadata('help', 'jump_help')
  jump() {
    if (this._object.body.blocked.down) {
      this._object.setVelocityY(-this._jumpAmount)
    }
  }

  @Reflect.metadata('translated', 'setJumpAmount')
  @Reflect.metadata('help', 'setJumpAmount_help')
  @checkArguments(['integer'])
  setJumpAmount(value) {
    this._jumpAmount = value
  }

  enableDrag() {
    if (this._object) {
      this._wasFalling = this._mayFall
      this.mayFall(false)
    }
    return super.enableDrag()
  }

  disableDrag() {
    if (this._object) {
      this.mayFall(this._wasFalling)
    }
    return super.disableDrag()
  }
}

export default Walker
