import { i18n } from '@lingui/core'
import BaseClass from '../base-class.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

@Reflect.metadata('translated', 'PlatformTile')
class PlatformTile extends BaseClass {
  constructor(tile) {
    super()
    this._tile = tile
  }

  getData() {
    return this
  }

  getLayer() {
    if (this._tile) {
      return this._tile.tilemapLayer
    } else {
      return null
    }
  }

  @Reflect.metadata('translated', 'getX')
  @Reflect.metadata('help', 'getX_help')
  getX() {
    if (this._tile) {
      return this._tile.x
    } else {
      return -1
    }
  }

  @Reflect.metadata('translated', 'getY')
  @Reflect.metadata('help', 'getY_help')
  getY() {
    if (this._tile) {
      return this._tile.y
    } else {
      return -1
    }
  }

  @Reflect.metadata('translated', 'is')
  @Reflect.metadata('help', 'is_help')
  @checkArguments(['string'])
  is(value) {
    if (this._tile) {
      return (
        this._tile.properties.is === value ||
        this._tile.properties[i18n._('is')] === value
      )
    } else {
      return false
    }
  }
}

export default PlatformTile
