import 'reflect-metadata'

import CommandExecutor from '../command-executor.js'

/*

    Clock is an object that inherits from CommandExecutor.
    It is an object that can can handle executing multiple commands
    at the same time, in a possibly repeated manner.
    It can execute these commands endlessly or only one time.

*/

@Reflect.metadata('translated', 'Clock')
class Clock extends CommandExecutor {
  constructor() {
    super()
    this.addListener('execute', this._executeCommands)
  }

  _executeCommands() {
    this._timeout = null
    if (this._commands.length === 0 || !this._running) return
    this._commands.forEach(callStatements => {
      this._runtime.executePriorityStatements(callStatements)
    })
  }
}

export default Clock
