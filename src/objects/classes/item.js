import { i18n } from '@lingui/core'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'
import itemTexture from '../resources/coin.png'
import keyTexture from '../resources/key.png'
import lifeTexture from '../resources/life.png'
import poisonTexture from '../resources/poison.png'
import safeTexture from '../resources/safe.png'

import GraphicClass from '../graphic-class.js'

@Reflect.metadata('translated', 'Item')
class Item extends GraphicClass {
  static _texture = 'item_default_texture'

  static setupDone = false

  static itemGroup = null

  static setup() {
    if (!this.setupDone) {
      super.setup()
      this._graphics.addLocalResource(
        'image',
        'item_default_texture',
        itemTexture,
      )
      this._graphics.addLocalResource('image', 'item_key', keyTexture)
      this._graphics.addLocalResource('image', 'item_life', lifeTexture)
      this._graphics.addLocalResource('image', 'item_poison', poisonTexture)
      this._graphics.addLocalResource('image', 'item_safe', safeTexture)
      this.itemGroup = new Set()
      this.setupDone = true
    }
  }

  constructor(texture = null) {
    super()
    //@ts-ignore
    this._texture = this.constructor._texture
    switch (texture) {
      case i18n._('coin'):
        this._texture = 'item_default_texture'
        break
      case i18n._('key'):
        this._texture = 'item_key'
        break
      case i18n._('life'):
        this._texture = 'item_life'
        break
      case i18n._('poison'):
        this._texture = 'item_poison'
        break
      case i18n._('safe'):
        this._texture = 'item_safe'
        break
      default:
        if (texture !== null) {
          this._texture = texture
        }
    }
    const scene = this._graphics.getScene()
    this._object = scene.physics.add.image(0, 0, this._texture)
    this._object.setOrigin(0)
    //@ts-ignore
    this.constructor.itemGroup.add(this._object)
    this._catcher = null
    this._dropping = false
    this._name = texture !== null ? texture : ''
    this._overlaps = []
    this.addListener('delete', () => {
      //@ts-ignore
      this.constructor.itemGroup.delete(this._object)
      this._catcher = null
      this._dropping = false
      this._overlaps = []
    })
    this.dispatch('created')
  }

  followCatcher() {
    if (this._catcher === null) return
    if (!this._dropping) {
      // if caught item and not dropping, move item with sprite
      this.setLocation(
        Math.round(this._catcher.getRealX()),
        Math.round(this._catcher.getRealY()),
      )
      return
    }
    if (!this._catcher.isOnItem(this)) {
      // if dropping item, wait for not being on it
      // to set dropping to false, since otherwise
      // it will be instantly caught again
      this._dropping = false
      this._catcher.dropped(this)
      this._catcher = null
      this._graphics.getScene().game.events.once('postrender', () => {
        this._overlaps.forEach(overlap => {
          this._graphics.getScene().physics.world.colliders.add(overlap)
        })
      })
    }
  }

  tick(delta) {
    this.followCatcher()
  }

  @Reflect.metadata('translated', 'setName')
  @Reflect.metadata('help', 'setName_help')
  @checkArguments(['string'])
  setName(value) {
    this._name = value
  }

  @Reflect.metadata('translated', 'getName')
  @Reflect.metadata('help', 'getName_help')
  getName() {
    return this._name
  }

  setCatcher(object) {
    if (this._catcher == null) {
      this._graphics.getScene().game.events.once('postrender', () => {
        this._overlaps.forEach(overlap => {
          this._graphics.getScene().physics.world.removeCollider(overlap)
        })
      })
    } else {
      this._catcher.dropped(this)
    }
    this._catcher = object
    object.caught(this)
  }

  getCatcher() {
    return this._catcher
  }

  drop() {
    this._dropping = true
  }

  addPossibleCatcher(object, command) {
    let handler
    if (command !== undefined) {
      const callStatements = this._runtime.getStatementsFromCommand(command)
      handler = (me, who) => {
        this.setCatcher(object)
        this._runtime.executePriorityStatements(callStatements, [
          me.getData('declickObject'),
          who.getData('declickObject'),
        ])
      }
    } else {
      handler = () => {
        this.setCatcher(object)
      }
    }
    const overlap = this._graphics
      .getScene()
      .physics.add.overlap(this._object, object._object, handler)
    this._overlaps.push(overlap)
  }

  setLocation(x, y) {
    super.setLocation(x, y)
    this._object.body.reset(x, y)
  }
}

export default Item
