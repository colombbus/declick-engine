import GraphicClass from '../graphic-class.js'
import SpriteGroupItem from './sprite-group-item.js'
import 'reflect-metadata'
import groupTexture from '../resources/star.png'
import { checkArguments } from '../utils/checks.js'
import ExtendedSprite from '../utils/extended-sprite.js'
import * as Phaser from 'phaser'

@Reflect.metadata('translated', 'SpriteGroup')
class SpriteGroup extends GraphicClass {
  static setupDone = false
  static _texture = 'group_default_texture'

  static setup() {
    if (!this.setupDone) {
      super.setup()
      this._graphics.addLocalResource(
        'image',
        'group_default_texture',
        groupTexture,
      )
      this.setupDone = true
    }
  }

  constructor(texture, length) {
    super()
    if (arguments.length === 1 && Number.isInteger(arguments[0])) {
      length = arguments[0]
      texture = undefined
    }
    this._movable = true
    this._object = null
    this._livingTime = null
    //@ts-ignore
    this._texture = texture !== undefined ? texture : this.constructor._texture
    this._buildObject(length)
    this._size = null
    this._bodySize = null
    this.addListener('stop', () => {
      this._object.setVelocity(0, 0)
    })
    this.dispatch('created')
  }

  _buildObject(length) {
    const scene = this._graphics.getScene()
    this._object = scene.physics.add.group({ classType: ExtendedSprite })
    this._object.setOrigin(0)
    if (length !== undefined) {
      this._poolMode = true
      this._object.createMultiple({
        key: this._texture,
        quantity: length,
        visible: false,
        active: false,
      })
      // we have to enable bodies since Extended Sprite already has a body created
      this._object.children.iterate(child => {
        scene.physics.world.enableBody(
          child,
          Phaser.Physics.Arcade.DYNAMIC_BODY,
        )
        child.body.setEnable(false)
      })
    } else {
      this._poolMode = false
    }
    // add datamanager
    this._object.data = new Phaser.Data.DataManager(this._object)
    this._object.setData = (key, value) => {
      this._object.data.set(key, value)
    }
    this._object.getData = key => {
      return this._object.data.get(key)
    }
    this.addListener('delete', () => {
      this._object.data.destroy()
    })
  }

  @Reflect.metadata('translated', 'ifCollisionWith')
  @Reflect.metadata('help', 'ifCollisionWith_help')
  @checkArguments(['object', 'function|string', 'string'], 1)
  ifCollisionWith(object, command, optionalParameter) {
    const callStatements = this._runtime.getStatementsFromCommand(command)
    object.addCollider(
      this._object,
      (me, who) => {
        this._runtime.executePriorityStatements(callStatements, [
          me.getData('declickObject'),
          who.getData('declickObject'),
        ])
      },
      optionalParameter,
    )
  }

  @Reflect.metadata('translated', 'ifOverlapWith')
  @Reflect.metadata('help', 'ifOverlapWith_help')
  @checkArguments(['object', 'string|function', 'string'], 1)
  ifOverlapWith(object, command, optionalParameter) {
    const callStatements = this._runtime.getStatementsFromCommand(command)
    object.addOverlap(
      this._object,
      (me, who) => {
        this._runtime.executePriorityStatements(callStatements, [
          me.getData('declickObject'),
          who.getData('declickObject'),
        ])
      },
      optionalParameter,
    )
  }

  @Reflect.metadata('translated', 'addBlock')
  @Reflect.metadata('help', 'addBlock_help')
  @checkArguments(['object'])
  addBlock(block) {
    block.addCollider(this._object)
  }

  @Reflect.metadata('translated', 'createSprite')
  @Reflect.metadata('help', 'createSprite_help')
  @checkArguments(['integer', 'integer'], 2)
  createSprite(x = 0, y = 0) {
    let object
    if (this._poolMode) {
      object = this._object.getFirstDead(false, x, y)
      if (object) {
        object.setActive(true)
        object.setVisible(true)
        object.body.setEnable(true)
      }
    } else {
      object = this._object.create(x, y, this._texture)
      // we have to enable body since Extended Sprite already has a body created
      this._graphics
        .getScene()
        .physics.world.enableBody(object, Phaser.Physics.Arcade.DYNAMIC_BODY)
    }
    if (object) {
      object.setOrigin(0)
      object.setImmovable(!this._movable)
      if (this._size !== null) {
        object.setDisplaySize(this._size[0], this._size[1])
      }
      if (this._bodySize !== null) {
        object.setBodySize(this._bodySize[0], this._bodySize[1])
      }
      return new SpriteGroupItem(
        this._object,
        object,
        this._poolMode,
        this._livingTime,
      )
    }
    return null
  }

  @Reflect.metadata('translated', 'mayBeMoved')
  @Reflect.metadata('help', 'mayBeMoved_help')
  @checkArguments(['boolean'], 1)
  mayBeMoved(value = true) {
    this._movable = value
    this._object.getChildren().forEach(child => {
      child.setImmovable(!value)
    })
  }

  addCollider(object, handler) {
    this._graphics
      .getScene()
      .physics.add.collider(object, this._object, handler)
  }

  addOverlap(object, handler) {
    this._graphics.getScene().physics.add.overlap(object, this._object, handler)
  }

  @Reflect.metadata('translated', 'setLivingTime')
  @Reflect.metadata('help', 'setLivingTime_help')
  @checkArguments(['integer'])
  setLivingTime(time) {
    this._livingTime = time
    this._object.getChildren().forEach(child => {
      if (child.getData && child.getData('declickObject')) {
        child.getData('declickObject').expiresIn(time)
      }
    })
  }

  @Reflect.metadata('translated', 'setDisplaySize')
  @Reflect.metadata('help', 'setDisplaySize_help')
  @checkArguments(['integer', 'integer'])
  setDisplaySize(width, height) {
    this._size = [width, height]
    this._object.getChildren().forEach(child => {
      child.setDisplaySize(width, height)
    })
  }

  @Reflect.metadata('translated', 'setBodySize')
  @Reflect.metadata('help', 'setBodySize_help')
  @checkArguments(['integer', 'integer'])
  setBodySize(width, height) {
    this._bodySize = [width, height]
    this._object.getChildren().forEach(child => {
      child.setBodySize(width, height)
    })
  }

  delete() {
    if (this._object !== null) {
      // destroy children
      const children = [...this._object.getChildren()]
      children.forEach(child => {
        if (child.getData && child.getData('declickObject')) {
          child.getData('declickObject').delete()
        }
      })
      // destroy group and remaining children if any (pool mode)
      this._object.destroy(true, true)
      this._objet = null
    }
    super.delete()
  }
}

export default SpriteGroup
