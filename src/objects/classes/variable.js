import BaseClass from '../base-class.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

@Reflect.metadata('translated', 'Variable')
class Variable extends BaseClass {
  constructor() {
    super()
    this._value = ''
  }

  @Reflect.metadata('translated', 'setText')
  @Reflect.metadata('help', 'setText_help')
  @checkArguments(['string'])
  setText(value) {
    this._value = value
  }

  @Reflect.metadata('translated', 'getText')
  @Reflect.metadata('help', 'getText_help')
  getText() {
    return this._value
  }
}

export default Variable
