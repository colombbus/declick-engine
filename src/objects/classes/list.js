import BaseClass from '../base-class.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'
import { i18n } from '@lingui/core'

@Reflect.metadata('translated', 'List')
class List extends BaseClass {
  constructor(values = []) {
    super()
    this._list = [...values]
    this._index = 0
  }

  @Reflect.metadata('translated', 'add')
  @Reflect.metadata('help', 'add_help')
  @checkArguments(['any'])
  add(value) {
    this._list.push(value)
  }

  @Reflect.metadata('translated', 'remove')
  @Reflect.metadata('help', 'remove_help')
  @checkArguments(['any'])
  remove(object) {
    this._list = this._list.filter(a => object !== a)
  }
  @Reflect.metadata('translated', 'rewind')
  @Reflect.metadata('help', 'rewind_help')
  rewind() {
    this._index = 0
  }

  @Reflect.metadata('translated', 'getNextObject')
  @Reflect.metadata('help', 'getNextObject_help')
  getNextObject() {
    if (this._index >= this._list.length) {
      this.throwError(i18n._('index greater than the length of the list'))
    }
    return this._list[this._index++]
  }

  @Reflect.metadata('translated', 'hasObjects')
  @Reflect.metadata('help', 'hasObjects_help')
  hasObjects() {
    return this._list.length !== 0
  }

  @Reflect.metadata('translated', 'getObject')
  @Reflect.metadata('help', 'getObject_help')
  @checkArguments(['integer'])
  getObject(index) {
    if (index >= this._list.length) {
      this.throwError(i18n._('index greater than the length of the list'))
    }
    return this._list[index]
  }

  @Reflect.metadata('translated', 'modify')
  @Reflect.metadata('help', 'modify_help')
  @checkArguments(['integer', 'any'])
  modify(index, object) {
    if (index >= this._list.length) {
      this.throwError(i18n._('index greater than the length of the list'))
    }
    this._list[index] = object
  }

  @Reflect.metadata('translated', 'has')
  @Reflect.metadata('help', 'has_help')
  @checkArguments(['any'])
  has(object) {
    if (this._list.indexOf(object) >= 0) {
      return true
    } else {
      return false
    }
  }

  /**
   * Returns the number of objects in List.
   */
  @Reflect.metadata('translated', 'getSize')
  @Reflect.metadata('help', 'getSize_help')
  getSize() {
    return this._list.length
  }

  /**
   * Empty List.
   */
  @Reflect.metadata('translated', 'empty')
  @Reflect.metadata('help', 'empty_help')
  empty() {
    this._list = []
    this._index = 0
  }
}

export default List
