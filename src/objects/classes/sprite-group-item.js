import Sprite from './sprite.js'
import 'reflect-metadata'

@Reflect.metadata('translated', 'SpriteGroupItem')
class SpriteGroupItem extends Sprite {
  _buildObject() {}

  constructor(group, obj, poolMode = false, livingTime = null) {
    super()
    this._object = obj
    this._group = group
    this._poolMode = poolMode
    this._age = 0
    this._expires = livingTime
    this.dispatch('created')
  }

  delete() {
    if (this._poolMode && this._object !== null) {
      this._object.setActive(false)
      this._object.setVisible(false)
      this._object.body.setEnable(false)
      this._object.setData('declickObject', null)
      // do not destroy object
      this._object = null
    } else if (this._object !== null) {
      this._group.remove(this._object)
    }
    super.delete()
  }

  tick(delta) {
    if (this._expires !== null) {
      this._age += delta

      if (this._age > this._expires) {
        this.delete()
      }
    }
    super.tick(delta)
  }

  expiresIn(time) {
    this._expires = time
  }
}

export default SpriteGroupItem
