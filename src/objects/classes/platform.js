import { i18n } from '@lingui/core'
import GraphicClass from '../graphic-class.js'
import 'reflect-metadata'
import defaultMap from '../resources/map.json'
import defaultMapTiles from '../resources/tiles.png'
import PlatformTile from './platform-tile.js'
import { checkArguments } from '../utils/checks.js'

@Reflect.metadata('translated', 'Platform')
class Platform extends GraphicClass {
  static _map = 'platform_default_map'
  static _tiles = 'platform_default_tiles'

  static setupDone = false

  static setup() {
    if (!this.setupDone) {
      super.setup()
      this._graphics.addLocalResource('map', 'platform_default_map', defaultMap)
      this._graphics.addLocalResource(
        'image',
        'platform_default_tiles',
        defaultMapTiles,
      )
      this.setupDone = true
    }
  }

  constructor(map, tiles) {
    super()
    //@ts-ignore
    this._map = map !== undefined ? map : this.constructor._map
    //@ts-ignore
    this._tiles = tiles !== undefined ? tiles : this.constructor._tiles
    const scene = this._graphics.getScene()
    this._object = scene.add.tilemap(this._map)
    const mapData = scene.cache.tilemap.get(this._map)
    const tilesets = mapData.data.tilesets.map(tileset =>
      this._object.addTilesetImage(tileset.name, this._tiles),
    )
    this._layers = mapData.data.layers.map(layer => {
      return this._object.createLayer(layer.name, tilesets)
    })

    this._tilesProperties = this._object.tilesets.reduce(
      (properties, tileset) => {
        for (
          let i = tileset.firstgid;
          i < tileset.firstgid + tileset.total;
          i++
        ) {
          properties.set(i, tileset.getTileProperties(i) ?? {})
        }
        return properties
      },
      new Map(),
    )

    this._collisionTileIds = [...this._tilesProperties]
      .filter(
        ([index, properties]) =>
          properties.collides === true ||
          properties[i18n._('collides')] === true,
      )
      .map(([index, properties]) => index)

    this._overlapTileIds = [...this._tilesProperties]
      .filter(
        ([index, properties]) =>
          properties.overlaps === true ||
          properties[i18n._('overlaps')] === true,
      )
      .map(([index, properties]) => index)

    const interactiveLayers = this._layers.reduce(
      (layers, layer) => {
        let collision = false
        let overlap = false
        const properties = layer.layer.properties
        properties.forEach(property => {
          if (
            property.name === 'collides' ||
            property.name === i18n._('collides')
          ) {
            collision = property.value
          } else if (
            property.name === 'overlaps' ||
            property.name === i18n._('overlaps')
          ) {
            overlap = property.value
          }
        })
        if (!collision) {
          if (
            layer.filterTiles(tile =>
              this._collisionTileIds.includes(tile.index),
            ).length > 0
          ) {
            collision = true
          }
        }
        if (!overlap) {
          if (
            layer.filterTiles(tile => this._overlapTileIds.includes(tile.index))
              .length > 0
          ) {
            overlap = true
          }
        }
        if (collision) {
          layers[0].push(layer)
        }
        if (overlap) {
          layers[1].push(layer)
        }
        return layers
      },
      [[], []],
    )

    this._collisionLayers = interactiveLayers[0]
    this._overlapLayers = interactiveLayers[1]
    this._objectColliders = new Map()
    this._objectOverlaps = new Map()
    this.dispatch('created')
  }

  addCollider(object, handler, tileType) {
    let collisionHandlers = []
    if (this._objectColliders.has(object)) {
      collisionHandlers = this._objectColliders.get(object)
    } else {
      const scene = this._graphics.getScene()
      this._collisionLayers.forEach(layer => {
        scene.physics.add.collider(object, layer, (me, tile) => {
          if (this._objectColliders.has(me)) {
            const handlers = this._objectColliders.get(me)
            handlers.forEach(handler => {
              handler(me, tile)
            })
          }
        })
      })
    }

    if (tileType !== undefined) {
      const newTileIds = [...this._tilesProperties]
        .filter(
          ([index, properties]) =>
            properties.is === tileType || properties[i18n._('is')] === tileType,
        )
        .map(([index, properties]) => index)
      this._collisionTileIds = [
        ...new Set([...this._collisionTileIds, ...newTileIds]),
      ]
    }

    if (handler !== undefined) {
      let collisionHandler
      if (tileType !== undefined) {
        collisionHandler = (me, tile) => {
          if (tile.properties.is && tile.properties.is === tileType) {
            handler(me, new PlatformTile(tile))
          }
        }
      } else {
        collisionHandler = (me, tile) => {
          handler(me, new PlatformTile(tile))
        }
      }
      collisionHandlers.push(collisionHandler)
    }

    this._collisionLayers.forEach(layer => {
      layer.setCollision(this._collisionTileIds)
    })

    this._objectColliders.set(object, collisionHandlers)
  }

  addOverlap(object, handler, tileType) {
    let overlapHandlers = []
    if (this._objectOverlaps.has(object)) {
      overlapHandlers = this._objectOverlaps.get(object)
    } else {
      const scene = this._graphics.getScene()
      this._overlapLayers.forEach(layer => {
        scene.physics.add.overlap(object, layer)
      })
    }

    if (handler !== undefined) {
      let overlapHandler
      if (tileType !== undefined) {
        overlapHandler = (who, tile) => {
          if (tile.properties.is && tile.properties.is === tileType) {
            handler(who, new PlatformTile(tile))
          }
        }
      } else {
        overlapHandler = (who, tile) => {
          handler(who, new PlatformTile(tile))
        }
      }
      overlapHandlers.push(overlapHandler)
    }

    if (tileType !== undefined) {
      const newTileIds = [...this._tilesProperties]
        .filter(
          ([index, properties]) =>
            properties.is === tileType || properties[i18n._('is')] === tileType,
        )
        .map(([index, properties]) => index)
      this._overlapTileIds = [
        ...new Set([...this._overlapTileIds, ...newTileIds]),
      ]
    }

    this._overlapLayers.forEach(layer => {
      layer.setTileIndexCallback(
        this._overlapTileIds,
        (me, tile) => {
          if (this._objectOverlaps.has(me)) {
            const handlers = this._objectOverlaps.get(me)
            handlers.forEach(handler => {
              handler(me, tile)
            })
          }
        },
        this,
      )
    })

    this._objectOverlaps.set(object, overlapHandlers)
  }

  @Reflect.metadata('translated', 'removeTile')
  @Reflect.metadata('help', 'removeTile_help')
  @checkArguments(['object'])
  removeTile(tile) {
    const tileLayer = tile.getLayer()
    const layer = this._layers.find(layer => layer === tileLayer)
    if (layer && layer.removeTileAt !== undefined) {
      layer.removeTileAt(tile.getX(), tile.getY())
    } else if (layer) {
      this.throwError(i18n._('layer not dynamic'))
    } else {
      this.throwError(i18n._('could not find tile'))
    }
  }

  @Reflect.metadata('translated', 'selectLayer')
  @Reflect.metadata('help', 'selectLayer_help')
  @checkArguments(['string'])
  selectLayer(name) {
    const index = this._object.getLayerIndexByName(name)
    if (index === null) {
      this.throwError(i18n._('could not find layer {name}', { name }))
    }
    this._object.setLayer(index)
  }

  @Reflect.metadata('translated', 'putTile')
  @Reflect.metadata('help', 'putTile_help')
  @checkArguments(['string', 'integer', 'integer'])
  putTile(type, x, y) {
    const layer = this._object.getLayer().tilemapLayer
    if (layer.putTileAt === undefined) {
      this.throwError(i18n._('layer not dynamic'))
    }
    const tile = [...this._tilesProperties].find(
      ([index, properties]) =>
        properties.is === type || properties[i18n._('is')] === type,
    )
    if (tile === undefined) {
      this.throwError(i18n._('could not find tile {type}', { type }))
    }
    const newTile = layer.putTileAt(tile[0], x, y)
    newTile.properties = tile[1]
  }

  @Reflect.metadata('translated', 'getTileAt')
  @Reflect.metadata('help', 'getTileAt_help')
  @checkArguments(['integer', 'integer'])
  getTileAt(x, y) {
    const layer = this._object.getLayer().tilemapLayer
    return new PlatformTile(layer.getTileAt(x, y, true))
  }
}

export default Platform
