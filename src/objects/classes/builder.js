import Robot from './robot.js'
import Platform from './platform.js'
import Item from './item.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'
import mapTiles from '../resources/tiles.png'
import blankMap from '../resources/blank-map.json'
import builderData from '../resources/builder.json'
import builderTexture from '../resources/builder.png'

const DEFAULT_SPEED = 200
const FLASH_SPEED = 2000
const TILE_SIZE = 40

@Reflect.metadata('translated', 'Builder')
class Builder extends Robot {
  static WALL = 'wall'
  static FLOOR = 'floor'
  static ENTRANCE = 'entrance'
  static EXIT = 'exit'
  static TILES = [Builder.WALL, Builder.FLOOR, Builder.ENTRANCE, Builder.EXIT]

  static setup() {
    if (!this.setupDone) {
      super.setup()
      this._graphics.addLocalResource(
        'atlas',
        'builder_animation',
        builderTexture,
        builderData,
      )
      this._graphics.addLocalResource(
        'image',
        'builder_default_tiles',
        mapTiles,
      )
      this._graphics.addLocalResource('map', 'builder_blank_map', blankMap)
      this.setupDone = true
    }
  }

  constructor(texture = 'builder_animation') {
    const _platform = new Platform('builder_blank_map', 'builder_default_tiles')
    super(texture)
    this._platform = _platform
    this.setStepLength(TILE_SIZE)
    this.setFlash(false)
    this.addListener('delete', () => {
      this._platform.delete()
      this._platform = null
    })
  }

  build(type = Builder.WALL, x = this.getX(), y = this.getY()) {
    this._platform.putTile(type, x, y)
  }

  @Reflect.metadata('translated', 'buildEntrance')
  @Reflect.metadata('help', 'buildEntrance_help')
  @checkArguments(['integer', 'integer'], 2)
  buildEntrance(x, y) {
    this.build(Builder.ENTRANCE, x, y)
  }

  @Reflect.metadata('translated', 'buildExit')
  @Reflect.metadata('help', 'buildExit_help')
  @checkArguments(['integer', 'integer'], 2)
  buildExit(x, y) {
    this.build(Builder.EXIT, x, y)
  }

  @Reflect.metadata('translated', 'buildWall')
  @Reflect.metadata('help', 'buildWall_help')
  @checkArguments(['integer', 'integer'], 2)
  buildWall(x, y) {
    this.build(Builder.WALL, x, y)
  }

  @Reflect.metadata('translated', 'buildFloor')
  @Reflect.metadata('help', 'buildFloor_help')
  @checkArguments(['integer', 'integer'], 2)
  buildFloor(x, y) {
    this.build(Builder.FLOOR, x, y)
  }

  @Reflect.metadata('translated', 'buildLine')
  @Reflect.metadata('help', 'buildLine_help')
  buildLine(...row) {
    row.forEach((tile, i) => {
      if (tile > 0) {
        this.build(Builder.TILES[tile - 1], this.getX() + i, this.getY())
      }
    })
  }

  @Reflect.metadata('translated', 'setFlash')
  @Reflect.metadata('help', 'setFlash_help')
  @checkArguments(['boolean'])
  setFlash(value) {
    if (value === true) {
      this._vX = FLASH_SPEED
      this._vY = FLASH_SPEED
    } else {
      this._vX = DEFAULT_SPEED
      this._vY = DEFAULT_SPEED
    }
  }

  @Reflect.metadata('translated', 'dropItem')
  @Reflect.metadata('help', 'dropItem_help')
  @checkArguments(['string'], 1)
  dropItem(type) {
    var item = new Item(type)

    var offset_x = this._object.width / 2 - item._object.width / 2
    var offset_y = this._object.height / 2 - item._object.height / 2

    var item_x = Math.round(this.getX() * this.getStepLength() + offset_x)
    var item_y = Math.round(this.getY() * this.getStepLength() + offset_y)
    item.setLocation(item_x, item_y)

    // return created object
    return item
  }

  @Reflect.metadata('translated', 'getPlatform')
  @Reflect.metadata('help', 'getPlatform_help')
  getPlatform() {
    return this._platform
  }
}

export default Builder
