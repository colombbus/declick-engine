import Text from './text.js'
import 'reflect-metadata'

@Reflect.metadata('translated', 'Stopwatch')
class Stopwatch extends Text {
  constructor() {
    const defaultLabel = '0:0:0'
    super(defaultLabel)
    this._pause = true
    this._time = 0
  }

  _updateTime() {
    const label =
      Math.trunc(this._time / 60000) +
      ':' +
      Math.trunc((this._time % 60000) / 1000) +
      ':' +
      Math.trunc((this._time % 1000) / 10)
    super.setText(label)
  }

  tick(delta) {
    super.tick(delta)
    if (!this._pause) {
      this._time += delta
      this._updateTime()
    }
  }

  @Reflect.metadata('translated', 'continue')
  @Reflect.metadata('help', 'continue_help')
  continue() {
    this._pause = false
  }

  @Reflect.metadata('translated', 'start')
  @Reflect.metadata('help', 'start_help')
  start() {
    this._time = 0
    this.continue()
  }

  @Reflect.metadata('translated', 'stop')
  @Reflect.metadata('help', 'stop_help')
  stop() {
    this.pause()
    const label = '0:0:0'
    super.setText(label)
    this.time = 0
  }

  @Reflect.metadata('translated', 'pause')
  @Reflect.metadata('help', 'pause_help')
  pause() {
    this._pause = true
  }
}

export default Stopwatch
