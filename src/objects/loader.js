//replaced at build time
const classes = import.meta.glob('./classes/*.js', {
  eager: true,
})
const instances = import.meta.glob('./instances/*.js', {
  eager: true,
})
/*import * as classes from './classes/*.js'
import * as instances from './instances/*.js'*/
import { initializeChecks } from './utils/checks'
import 'reflect-metadata'
import { i18n } from '@lingui/core'

const allMethods = {}

function _getAllTranslatedMethodParams(aPrototype) {
  const translatedMethods = new Map()
  do {
    const keys = Reflect.ownKeys(aPrototype)
    keys.forEach(key => {
      const translatedValue = Reflect.getMetadata('translated', aPrototype, key)
      if (translatedValue) {
        const asyncValue = Reflect.getMetadata('async', aPrototype, key)
        const helpValue = Reflect.getMetadata('help', aPrototype, key)
        translatedMethods.set(i18n._(translatedValue), {
          name: key,
          async: asyncValue ?? false,
          help: helpValue ? i18n._(helpValue) : null,
        })
      }
    })
  } while ((aPrototype = Reflect.getPrototypeOf(aPrototype)))
  return translatedMethods
}

function _loadImportedClass(importedClass, source) {
  importedClass.prototype._declickId_ = source
  const name = i18n._(Reflect.getMetadata('translated', importedClass))
  const methods = _getAllTranslatedMethodParams(importedClass.prototype)
  registerMethods(name, methods)
  return {
    instance: Reflect.getMetadata('instance', importedClass),
    name,
    object: importedClass,
    methods,
  }
}

function registerMethods(name, methods) {
  allMethods[name] = [...methods.entries()].map(([methodName, value]) => ({
    method: methodName,
    text: value.help,
  }))
}

export default {
  load() {
    initializeChecks()
    const objects = []
    for (const name in classes) {
      // because babel and parcel do not handle glob import the same way...
      // @ts-ignore
      const classContent = classes[name].default
        ? // @ts-ignore
          classes[name].default
        : classes[name]
      objects.push(
        _loadImportedClass(classContent, name.substring(2, name.length - 3)),
      )
    }
    for (const name in instances) {
      // because babel and parcel do not handle glob import the same way...
      // @ts-ignore
      const instanceContent = instances[name].default
        ? // @ts-ignore
          instances[name].default
        : instances[name]
      objects.push(
        _loadImportedClass(instanceContent, name.substring(2, name.length - 3)),
      )
    }
    return objects
  },
  getMethods(name) {
    return allMethods[name] ?? null
  },
}
