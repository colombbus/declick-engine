import BaseClass from './base-class.js'
import 'reflect-metadata'
import { checkArguments } from './utils/checks.js'
/**
 * Basic class for graphic objects
 */
class GraphicClass extends BaseClass {
  constructor() {
    super()
    // @ts-ignore
    this._graphics = this.constructor._graphics
    this._object = null
    this._graphics.addObject(this)
    this.addListener('delete', () => {
      this._graphics.removeObject(this)
      if (this._object !== null) {
        this._object.destroy()
        this._object = null
      }
    })
    this.addListener('created', () => {
      if (this._object !== null && this._object.setData) {
        this._object.setData('declickObject', this)
      }
    })
  }

  static setRuntime(runtime) {
    super.setRuntime(runtime)
    this._graphics = this._runtime.getGraphics()
  }

  static setup() {
    // to be implemented by children classes
  }

  tick(delta) {
    // do nothing
  }

  getGraphicalObject() {
    return this._object
  }

  enableDrag() {
    if (this._object !== null && this._object.setInteractive) {
      this._object.setInteractive()
      return true
    }
    return false
  }

  disableDrag() {
    if (this._object !== null && this._object.disableInteractive) {
      this._object.disableInteractive()
      return true
    }
    return false
  }

  @Reflect.metadata('translated', 'setLocation')
  @Reflect.metadata('help', 'setLocation_help')
  @checkArguments(['integer', 'integer'])
  setLocation(x, y) {
    // do nothing
  }

  @Reflect.metadata('translated', 'getX')
  @Reflect.metadata('help', 'getX_help')
  getX() {
    if (this._object) {
      return Math.round(this._object.x)
    }
  }

  @Reflect.metadata('translated', 'getY')
  @Reflect.metadata('help', 'getY_help')
  getY() {
    if (this._object) {
      return Math.round(this._object.y)
    }
  }

  dragTo(x, y) {
    this.setLocation(x, y)
  }

  getRealX() {
    if (this._object) {
      return this._object.x
    }
  }

  getRealY() {
    if (this._object) {
      return this._object.y
    }
  }
}

export default GraphicClass
