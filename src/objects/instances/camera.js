import BaseInstance from '../base-instance.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

@Reflect.metadata('translated', 'camera')
class CameraClass extends BaseInstance {
  _getCamera() {
    const graphics = this._runtime.getGraphics()
    return graphics.getScene().cameras.main
  }

  @Reflect.metadata('translated', 'follow')
  @Reflect.metadata('help', 'follow_help')
  @checkArguments(['object'])
  follow(object) {
    const camera = this._getCamera()
    camera.startFollow(object.getGraphicalObject())
  }

  @Reflect.metadata('translated', 'stopFollowing')
  @Reflect.metadata('help', 'stopFollowing_help')
  stopFollowing() {
    const camera = this._getCamera()
    camera.stopFollow()
  }
}
export default CameraClass
