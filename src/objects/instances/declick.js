import BaseInstance from '../base-instance.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

@Reflect.metadata('translated', 'declick')
class DeclickClass extends BaseInstance {
  @Reflect.metadata('translated', 'write')
  @Reflect.metadata('help', 'write_help')
  @checkArguments(['string'])
  write(value) {
    this._runtime.message(value)
  }

  @Reflect.metadata('translated', 'clear')
  @Reflect.metadata('help', 'clear_help')
  clear() {
    this._runtime.clear(true)
  }

  @Reflect.metadata('translated', 'suspend')
  @Reflect.metadata('help', 'suspend_help')
  suspend() {
    this._runtime.suspend()
  }

  @Reflect.metadata('translated', 'resume')
  @Reflect.metadata('help', 'resume_help')
  resume() {
    this._runtime.resume()
  }

  @Reflect.metadata('translated', 'stop')
  @Reflect.metadata('help', 'stop_help')
  stop() {
    this._runtime.stop()
  }

  @Reflect.metadata('translated', 'wait')
  @Reflect.metadata('help', 'wait_help')
  wait(duration) {
    this._runtime.suspend()
    setTimeout(() => {
      this._runtime.resume()
    }, duration)
  }

  @Reflect.metadata('translated', 'load')
  @Reflect.metadata('help', 'load_help')
  @Reflect.metadata('async', true)
  load(program, callback) {
    this._runtime
      .loadProgram(program)
      .then(() => {
        callback()
      })
      .catch(e => {
        callback(e)
      })
  }

  @Reflect.metadata('translated', 'interrupt')
  @Reflect.metadata('help', 'interrupt_help')
  interrupt() {
    this._runtime.interrupt()
  }
}

export default DeclickClass
