import { i18n } from '@lingui/core'
import BaseInstance from '../base-instance.js'
import 'reflect-metadata'
import { checkArguments } from '../utils/checks.js'

const keyCodes = new Map([
  ['backspace', [0]],
  ['tab', [9]],
  ['return', [13]],
  ['shift', [16]],
  ['ctrl', [17]],
  ['alt', [18]],
  ['pausebreak', [19]],
  ['capslock', [20]],
  ['escape', [27]],
  ['space', [32]],
  ['pageup', [33]],
  ['pagedown', [34]],
  ['end', [35]],
  ['home', [36]],
  ['left', [37]],
  ['up', [38]],
  ['right', [39]],
  ['down', [40]],
  ['+', [43, 107]],
  ['printscreen', [44]],
  ['insert', [45]],
  ['delete', [46]],
  ['0', [48, 96]],
  ['1', [49, 97]],
  ['2', [50, 98]],
  ['3', [51, 99]],
  ['4', [52, 100]],
  ['5', [53, 101]],
  ['6', [54, 102]],
  ['7', [55, 103]],
  ['8', [56, 104]],
  ['9', [57, 105]],
  [';', [59, 186]],
  ['=', [61, 187]],
  ['a', [65]],
  ['b', [66]],
  ['c', [67]],
  ['d', [68]],
  ['e', [69]],
  ['f', [70]],
  ['g', [71]],
  ['h', [72]],
  ['i', [73]],
  ['j', [74]],
  ['k', [75]],
  ['l', [76]],
  ['m', [77]],
  ['n', [78]],
  ['o', [79]],
  ['p', [80]],
  ['q', [81]],
  ['r', [82]],
  ['s', [83]],
  ['t', [84]],
  ['u', [85]],
  ['v', [86]],
  ['w', [87]],
  ['x', [88]],
  ['y', [89]],
  ['z', [90]],
  ['*', [106]],
  ['-', [109, 189]],
  ['.', [110, 190]],
  ['/', [111, 191]],
  ['f1', [112]],
  ['f2', [113]],
  ['f3', [114]],
  ['f4', [115]],
  ['f5', [116]],
  ['f6', [117]],
  ['f7', [118]],
  ['f8', [119]],
  ['f9', [120]],
  ['f10', [121]],
  ['f11', [122]],
  ['f12', [123]],
  ['numlock', [144]],
  ['scrolllock', [145]],
  [',', [188]],
  ['`', [192]],
  ['[', [219]],
  ['\\', [220]],
  [']', [221]],
  ["'", [222]],
])

const keyTranslations = new Map([
  ['return', i18n._('return')],
  ['capslock', i18n._('capslock')],
  ['escape', i18n._('escape')],
  ['space', i18n._('space')],
  ['left', i18n._('left')],
  ['up', i18n._('up')],
  ['right', i18n._('right')],
  ['down', i18n._('down')],
  ['insert', i18n._('insert')],
  ['delete', i18n._('delete')],
])

const keyNames = new Map()

for (let [name, codes] of keyCodes.entries()) {
  codes.forEach(code => {
    keyNames.set(code, name)
  })
}

@Reflect.metadata('translated', 'keyboard')
class KeyboardClass extends BaseInstance {
  constructor() {
    super()
    this._keyCodes = new Map([...keyNames.keys()].map(code => [code, false]))
    this._justTyped = new Map([...keyCodes.keys()].map(name => [name, false]))
    this._translatedNames = new Map(
      [...keyTranslations].map(([name, translated]) => [
        i18n._(translated),
        name,
      ]),
    )
    const exposedProperties = []

    for (let name of keyCodes.keys()) {
      const keyName = `_key_${name}`
      this[keyName] = false
      exposedProperties.push({
        name: keyName,
        exposedName: keyTranslations.has(name)
          ? i18n._(keyTranslations.get(name))
          : name,
      })
    }
    this.addListener('runtimeInitialized', () => {
      this._runtime.exposeProperties(this, exposedProperties)
      window.addEventListener('keydown', event => {
        this._processKeyDown(event)
      })
      window.addEventListener('keyup', event => {
        this._processKeyUp(event)
      })
    })

    this.addListener('clear', () => {
      for (let name of keyCodes.keys()) {
        const keyName = `_key_${name}`
        this[keyName] = false
      }
      for (let code of this._keyCodes.keys()) {
        this._keyCodes.set(code, false)
      }
      for (let name of this._justTyped.keys()) {
        this._keyCodes.set(name, false)
      }
    })
  }

  _processKeyDown(event) {
    const code = event.keyCode
    if (keyNames.has(code)) {
      const name = keyNames.get(code)
      this[`_key_${name}`] = true
      this._keyCodes.set(code, true)
      this._justTyped.set(name, true)
    }
  }

  _processKeyUp(event) {
    const code = event.keyCode
    if (keyNames.has(code)) {
      const name = keyNames.get(code)
      this[`_key_${name}`] = false
      this._keyCodes.set(code, false)
      this._justTyped.set(name, false)
    }
  }

  @Reflect.metadata('translated', 'justTyped')
  @Reflect.metadata('help', 'justTyped_help')
  @checkArguments(['string'])
  justTyped(name) {
    if (this._translatedNames.has(name)) {
      name = this._translatedNames.get(name)
    }
    if (!this._justTyped.has(name)) {
      this.throwError(i18n._('unknown key'))
    }
    let value = this._justTyped.get(name)
    this._justTyped.set(name, false)
    return value
  }

  @Reflect.metadata('translated', 'wait')
  @Reflect.metadata('help', 'wait()')
  wait() {
    const temporaryListener = event => {
      window.setTimeout(() => {
        window.removeEventListener('keydown', temporaryListener)
        this._runtime.resume()
      }, 0)
    }
    window.addEventListener('keydown', temporaryListener)
    this._runtime.suspend(true)
  }
}

export default KeyboardClass
