import { readFileSync } from 'fs'

const mediaTypes = [
  'png',
  'jpg',
  'jpeg',
  'gif',
  'svg',
  'webp',
  'ico',
  'bmp',
  'avif',
  'webp',
]

function loadBase64(path) {
  for (const mediaType of mediaTypes) {
    if (path.endsWith(`.${mediaType}`)) {
      const src = readFileSync(path).toString('base64')
      return `data:image/${mediaType};base64,${src}`
    }
  }
}

export { loadBase64 }
