import { describe, it, beforeAll } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import { i18n } from '@lingui/core'
import {
  checkArguments,
  initializeChecks,
} from '../../../src/objects/utils/checks.js'
import englishTranslations from '../../../translations/translation.en.json'

describe('When checkArguments is added', () => {
  let TestClass
  let testInstance

  const DeclickClass = class {
    _declickId_ = 'test'
  }

  const declickFunction = {
    type: 'function',
  }

  beforeAll(function() {
    i18n.load('en', englishTranslations)
    i18n.activate('en')
    initializeChecks()
    TestClass = class {
      //@ts-ignore
      @checkArguments(['string'])
      expectString() {
        return true
      }

      //@ts-ignore
      @checkArguments(['integer'])
      expectInteger(value) {
        return true
      }

      //@ts-ignore
      @checkArguments(['number'])
      expectNumber(value) {
        return true
      }

      //@ts-ignore
      @checkArguments(['array'])
      expectArray(value) {
        return true
      }

      //@ts-ignore
      @checkArguments(['boolean'])
      expectBoolean(value) {
        return true
      }

      //@ts-ignore
      @checkArguments(['object'])
      expectObject(value) {
        return true
      }

      //@ts-ignore
      @checkArguments(['function'])
      expectFunction(value) {
        return true
      }

      //@ts-ignore
      @checkArguments(['any'])
      expectAny(value) {
        return true
      }

      //@ts-ignore
      @checkArguments(['string|boolean'])
      expectOr(value) {
        return true
      }

      //@ts-ignore
      @checkArguments([
        'string',
        'integer',
        'number',
        'array',
        'boolean',
        'object',
        'function',
      ])
      expectMultiple(value1, value2, value3, value4, value5, value6, value7) {
        return true
      }

      //@ts-ignore
      @checkArguments(
        [
          'string',
          'integer',
          'number',
          'array',
          'boolean',
          'object',
          'function',
        ],
        2,
      )
      expectMultipleOptional(
        value1,
        value2,
        value3,
        value4,
        value5,
        value6,
        value7,
      ) {
        return true
      }
    }

    testInstance = new TestClass()
  })

  it('should be ok when the right type is provided', () => {
    let value1, value2, value3, value4, value5, value6, value7, value8
    value1 = testInstance.expectString('a string')
    value2 = testInstance.expectInteger(42)
    value3 = testInstance.expectNumber(1.2)
    value4 = testInstance.expectArray([1, 2])
    value5 = testInstance.expectBoolean(false)
    value6 = testInstance.expectObject(new DeclickClass())
    value7 = testInstance.expectFunction(declickFunction)
    value8 = testInstance.expectAny('abc')
    assert.ok(value1)
    assert.ok(value2)
    assert.ok(value3)
    assert.ok(value4)
    assert.ok(value5)
    assert.ok(value6)
    assert.ok(value7)
    assert.ok(value8)
  })

  it('should be ok when the right types are provided in the right order', () => {
    let value = testInstance.expectMultiple(
      'string',
      5,
      4.2,
      ['a', 'b'],
      true,
      new DeclickClass(),
      declickFunction,
    )
    assert.ok(value)
  })

  it('should detect when a wrong type is provided', () => {
    let error1, error2, error3, error4, error5, error6, error7
    try {
      testInstance.expectString(42)
    } catch (e) {
      error1 = e
    }
    try {
      testInstance.expectInteger(42.3)
    } catch (e) {
      error2 = e
    }
    try {
      testInstance.expectNumber('test')
    } catch (e) {
      error3 = e
    }
    try {
      testInstance.expectArray(3)
    } catch (e) {
      error4 = e
    }
    try {
      testInstance.expectBoolean('text')
    } catch (e) {
      error5 = e
    }
    try {
      testInstance.expectObject(5)
    } catch (e) {
      error6 = e
    }
    try {
      testInstance.expectFunction({ a: 5 })
    } catch (e) {
      error7 = e
    }
    assert.deepEqual(error1, { declickObjectError: '42 is not a string' })
    assert.deepEqual(error2, {
      declickObjectError: i18n.number(42.3) + ' is not an integer',
    })
    assert.deepEqual(error3, {
      declickObjectError: 'test is not a number',
    })
    assert.deepEqual(error4, {
      declickObjectError: '3 is not an array',
    })
    assert.deepEqual(error5, {
      declickObjectError: 'text is not a boolean',
    })
    assert.deepEqual(error6, {
      declickObjectError: '5 is not an object',
    })
    assert.deepEqual(error7, {
      declickObjectError: '[object Object] is not a function',
    })
  })

  it('should detect when one of the provided types is not right', () => {
    let error
    try {
      testInstance.expectMultiple(
        'string',
        5.1,
        4.2,
        ['a', 'b'],
        true,
        new DeclickClass(),
        declickFunction,
      )
    } catch (e) {
      error = e
    }
    assert.deepEqual(error, {
      declickObjectError: i18n.number(5.1) + ' is not an integer',
    })
  })

  it('should detect when one argument is missing', () => {
    let error1, error2
    try {
      testInstance.expectMultiple(
        'string',
        5,
        4.2,
        ['a', 'b'],
        true,
        new DeclickClass(),
      )
    } catch (e) {
      error1 = e
    }
    try {
      testInstance.expectAny()
    } catch (e) {
      error2 = e
    }
    assert.deepEqual(error1, {
      declickObjectError: 'missing function argument',
    })
    assert.deepEqual(error2, {
      declickObjectError: 'missing argument',
    })
  })

  it('should be ok when one optional argument is missing', () => {
    let value = testInstance.expectMultipleOptional(
      'string',
      5,
      4.2,
      ['a', 'b'],
      true,
    )
    assert.ok(value)
  })

  it('should detect when one mandatory argument is missing', () => {
    let error
    try {
      testInstance.expectMultipleOptional('string', 5, 4.2, ['a', 'b'])
    } catch (e) {
      error = e
    }
    assert.deepEqual(error, {
      declickObjectError: 'missing boolean argument',
    })
  })

  it('should detect when two mandatory arguments are missing', () => {
    let error
    try {
      testInstance.expectMultipleOptional('string', 5, 4.2)
    } catch (e) {
      error = e
    }
    assert.deepEqual(error, {
      declickObjectError: 'missing array argument\nmissing boolean argument',
    })
  })

  it('should be ok when one of several accepted types is provided', () => {
    let value1 = testInstance.expectOr('string')
    let value2 = testInstance.expectOr(true)
    assert.ok(value1)
    assert.ok(value2)
  })

  it('should detect when a type that is not in accepted types is provided', () => {
    let error
    try {
      testInstance.expectOr(5)
    } catch (e) {
      error = e
    }
    assert.deepEqual(error, {
      declickObjectError: '5 is not a string',
    })
  })
})
