import { describe, it } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import BaseClass from '../../src/objects/base-class.js'

describe('When BaseClass is instantiated', () => {
  it('should trigger a delete listener when instance is deleted', () => {
    const anObject = new BaseClass()
    let called = null
    anObject.addListener('delete', function() {
      called = this
    })
    anObject.delete()
    assert.deepEqual(called, anObject)
  })

  it('should record reference to runtime and every instance should have it', () => {
    const fakeRuntime = {
      addObject() {},
    }
    BaseClass.setRuntime(fakeRuntime)
    const anObject = new BaseClass()
    assert.equal(anObject._runtime, fakeRuntime)
  })

  it('should call runtime addObject method when instance is created', () => {
    let called = null
    const fakeRuntime = {
      addObject(value) {
        called = value
      },
    }
    BaseClass.setRuntime(fakeRuntime)
    const anObject = new BaseClass()
    assert.deepEqual(called, anObject)
  })

  it('should call runtime deleteObject method when instance is deleted', () => {
    let called = null
    const fakeRuntime = {
      addObject() {},

      deleteObject(value) {
        called = value
      },
    }
    BaseClass.setRuntime(fakeRuntime)
    const anObject = new BaseClass()
    anObject.delete()
    assert.deepEqual(called, anObject)
  })

  it('should have metadata instance set to false', () => {
    assert.equal(Reflect.getMetadata('instance', BaseClass), false)
  })

  it('should be able to throw an error through its method throwError', () => {
    let error
    let anObject = new BaseClass()
    try {
      anObject.throwError('test error')
    } catch (e) {
      error = e
    }
    assert.deepEqual(error, { declickObjectError: 'test error' })
  })
})
