import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import pixelmatch from 'pixelmatch'
import Button from '../../../src/objects/classes/button.js'

let game = null
let scene = null
let fakeRuntime = null
let fakeGraphics = null

describe('When Button is instantiated', () => {
  beforeAll(
    () =>
      new Promise(done => {
        const canvasElement = document.createElement('canvas')
        canvasElement.width = 100
        canvasElement.height = 100
        document.body.append(canvasElement)
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          scene: {
            init: function() {
              scene = this
              done()
            },
          },
          callbacks: {
            postBoot: function() {
              game.loop.stop()
            },
          },
        })

        fakeGraphics = {
          addObject() {},
          getScene() {
            return scene
          },
        }

        fakeRuntime = {
          addObject() {},
          getGraphics() {
            return fakeGraphics
          },
        }
      }),
  )

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
  })

  it('should display the button at location 0,0', () =>
    new Promise(done => {
      Button.setRuntime(fakeRuntime)

      const testObject = new Button('test')

      scene.load.start()
      done()
    }))

  it('should allow to change its color', () =>
    new Promise(done => {
      Button.setRuntime(fakeRuntime)

      const testObject = new Button('test')
      testObject.setColor(255, 0, 0)
      assert.equal('#ff0000', testObject._fillColor)
      assert.equal('#ff0000', testObject._object.style.backgroundColor)

      scene.load.start()
      done()
    }))

  it('should allow to change its text color', () =>
    new Promise(done => {
      Button.setRuntime(fakeRuntime)

      const testObject = new Button('test')
      testObject.setTextColor(0, 255, 0)
      assert.equal('#00ff00', testObject._textColor)
      assert.equal('#00ff00', testObject._object.style.color)

      scene.load.start()
      done()
    }))

  it('should allow to change its text', () =>
    new Promise(done => {
      Button.setRuntime(fakeRuntime)

      const testObject = new Button()
      testObject.setText('test')
      assert.equal('test', testObject._object.text)

      scene.load.start()
      done()
    }))

  it('should allow to execute commands', () =>
    new Promise(done => {
      let test = false

      fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        getStatementsFromCommand(command) {
          return command
        },
        executePriorityStatements(commands) {
          test = true
        },
      }
      Button.setRuntime(fakeRuntime)

      const testObject = new Button('test')
      let test_command = 'a = Text(5)'
      testObject.addCommand(test_command)

      // var event = document.createEvent("HTMLEvents")
      // event.initEvent("click", false, true)
      // testObject._object.renderer.dispatchEvent(event)
      testObject._object.emit('pointerdown')

      scene.load.start()

      assert.equal(true, test)
      done()
    }))
})
