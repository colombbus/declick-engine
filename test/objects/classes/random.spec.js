import { describe, it } from 'vitest'
import { assert } from 'chai'
import Random from '../../../src/objects/classes/random.js'
import 'reflect-metadata'

describe('When Random is instantiated', () => {
  it('should return int between max and 1', () => {
    let anObject = new Random()
    assert.equal(anObject.throwDice(1), 1)
  })
})
