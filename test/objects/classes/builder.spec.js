import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import pixelmatch from 'pixelmatch'
import Item from '../../../src/objects/classes/item.js'
import Builder from '../../../src/objects/classes/builder.js'
import Platform from '../../../src/objects/classes/platform.js'

let game = null
let scene = null
let fakeGraphics = null
let fakeRuntime = null
let objects = []

describe('When Builder is instantiated', () => {
  beforeAll(() => {
    return new Promise((done, reject) => {
      fakeGraphics = {
        addObject(object) {
          objects.push(object)
        },
        addLocalResource(asset_type, asset_name, data1, data2) {
          switch (asset_type) {
            case 'atlas':
              const imageData = data1
              const jsonData = data2
              const atlasImage = new Image()
              atlasImage.onload = () => {
                if (jsonData.anims) {
                  scene.cache.json.add(`${asset_name}_anims`, jsonData.anims)
                }
                scene.textures.addAtlas(asset_name, atlasImage, jsonData)
              }
              atlasImage.src = imageData
              break
            case 'image':
              scene.textures.addBase64(asset_name, data1)
              break
            case 'map':
              scene.cache.tilemap.add(asset_name, {
                format: Phaser.Tilemaps.Formats.TILED_JSON,
                data: data1,
              })
              break
          }
        },
        getScene() {
          return scene
        },
        removeObject() {},
      }

      fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        deleteObject() {},
      }

      const canvasElement = document.createElement('canvas')
      canvasElement.width = 100
      canvasElement.height = 100
      document.body.append(canvasElement)

      try {
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          //@ts-ignore
          loader: { imageLoadType: 'HTMLImageElement' },
          physics: {
            default: 'arcade',
            arcade: {
              gravity: { y: 0 },
              debug: false,
            },
          },
          scene: {
            active: true,
            init: function() {
              scene = this
              Builder.setRuntime(fakeRuntime)
              Platform.setRuntime(fakeRuntime)
              scene.textures.once('onload', () => {
                done()
              })
              Builder.setup()
              Platform.setup()
            },
          },
          callbacks: {},
        })
      } catch (e) {
        reject(e)
      }
    }).catch(err => {
      console.error(err)
    })
  })

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
        update: function(time, delta) {
          objects.forEach(object => {
            object.tick(delta)
          })
        },
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
    objects = []
  })

  it('should initialize the object', () =>
    new Promise(done => {
      const testObject = new Builder()
      done()
    }))

  it('should drop an item at its location', () =>
    new Promise(done => {
      Item.setRuntime(fakeRuntime)
      Item.setup()
      const testObject = new Builder()
      testObject.setLocation(1, 1)
      scene.load.image('test', `file://${__dirname}/dk.png`)
      scene.load.once('complete', () => {
        // Item dropped at builder's location
        // which is where the item is dropped
        const testItem = testObject.dropItem('test')
        testObject.setLocation(3, 3)
        game.renderer.snapshotArea(
          Math.round(40 - 5 / 2),
          Math.round(40 - 5 / 2),
          45,
          45,
          image => {
            const canvas1 = document.createElement('canvas')
            const ctx1 = canvas1.getContext('2d')
            ctx1.drawImage(image, 0, 0)
            const canvas2 = document.createElement('canvas')
            canvas2.height = 45
            canvas2.width = 45
            const ctx2 = canvas2.getContext('2d')
            const testImage = document.createElement('img')
            testImage.onload = () => {
              ctx2.drawImage(testImage, 0, 0)
              const test = pixelmatch(
                ctx1.getImageData(0, 0, 45, 45).data,
                ctx2.getImageData(0, 0, 45, 45).data,
                null,
                45,
                45,
                { threshold: 0.1 },
              )
              try {
                assert.isBelow(test, 2)
                done()
              } catch (e) {
                done(e)
              }
            }
            testImage.src = `file://${__dirname}/dk.png`
          },
          'image/png',
        )
      })
      scene.load.start()
    }))

  // Trying to test map building, doesn't work for the time being
  it('should build an entrance at its location', () =>
    new Promise(done => {
      const testObject = new Builder()
      testObject.setLocation(1, 1)
      // Tile is put in builder's location
      testObject.buildEntrance()
      testObject.setLocation(3, 3)
      game.renderer.snapshotArea(
        40,
        40,
        40,
        40,
        image => {
          const canvas1 = document.createElement('canvas')
          canvas1.width = 40
          canvas1.height = 40
          const ctx1 = canvas1.getContext('2d')
          ctx1.drawImage(image, 0, 0)
          const canvas2 = document.createElement('canvas')
          canvas2.height = 42
          canvas2.width = 210
          const ctx2 = canvas2.getContext('2d')
          const testImage = document.createElement('img')
          testImage.onload = () => {
            ctx2.fillStyle = '#000000'
            ctx2.fillRect(0, 0, 210, 42)
            ctx2.drawImage(testImage, 0, 0)
            const test = pixelmatch(
              ctx1.getImageData(0, 0, 40, 40).data,
              ctx2.getImageData(43, 1, 40, 40).data,
              null,
              40,
              40,
              { threshold: 0.1 },
            )
            try {
              assert.isBelow(test, 2)
              done()
            } catch (e) {
              done(e)
            }
          }
          testImage.src = `file://${__dirname}/tiles/tiles.png`
        },
        'image/png',
      )
    }))
})
