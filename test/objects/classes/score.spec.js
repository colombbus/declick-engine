import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import { JSDOM } from 'jsdom'
import Score from '../../../src/objects/classes/score.js'

let game = null
let scene = null

describe('When Score is instantiated', () => {
  beforeAll(
    () =>
      new Promise(done => {
        const canvasElement = document.createElement('canvas')
        canvasElement.width = 100
        canvasElement.height = 100
        document.body.append(canvasElement)
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          scene: {
            init: function() {
              scene = this
              done()
            },
          },
          callbacks: {
            postBoot: function() {
              game.loop.stop()
            },
          },
        })
      }),
  )

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
  })

  it('should instantiate and display the Score at location 0,0', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Score.setRuntime(fakeRuntime)

      const testObject = new Score('test', 0)
      assert.equal('test : 0', testObject._object.text)
      assert.equal('test : 0', testObject.getScore())

      // default constructors
      const testObject2 = new Score('test')
      assert.equal('test : 0', testObject2._object.text)
      assert.equal('test : 0', testObject2.getScore())

      const testObject3 = new Score(undefined, 100)
      assert.equal('Score : 100', testObject3._object.text)
      assert.equal('Score : 100', testObject3.getScore())

      scene.load.start()
      done()
    }))

  it('should allow to increase its score', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Score.setRuntime(fakeRuntime)

      const testObject = new Score('test', 0)

      // default increase
      testObject.increaseScore()
      assert.equal('test : 1', testObject._object.text)

      // increase with custom value
      testObject.increaseScore(5)
      assert.equal('test : 6', testObject._object.text)

      scene.load.start()
      done()
    }))

  it('should allow to reset its score', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Score.setRuntime(fakeRuntime)

      const testObject = new Score('test', 100)
      testObject.eraseScoreNumber()
      assert.equal('test : 0', testObject._object.text)

      scene.load.start()
      done()
    }))

  it('should allow to decrease its score', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Score.setRuntime(fakeRuntime)

      const testObject = new Score('test', 5)

      // default decrease
      testObject.decreaseScore()
      assert.equal('test : 4', testObject._object.text)

      // decrease with custom value
      testObject.decreaseScore(2)
      assert.equal('test : 2', testObject._object.text)

      scene.load.start()
      done()
    }))

  it('should allow to change and get its label', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Score.setRuntime(fakeRuntime)

      const testObject = new Score('test')
      testObject.setLabel('new test')
      assert.equal('new test : 0', testObject._object.text)
      assert.equal('new test', testObject.getLabel())

      scene.load.start()
      done()
    }))

  it('should allow to change and get its score', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Score.setRuntime(fakeRuntime)

      const testObject = new Score('test')
      testObject.setScoreNumber(100)
      assert.equal('test : 100', testObject._object.text)
      assert.equal('test : 100', testObject.getScore())
      assert.equal('100', testObject.getScoreNumber())

      scene.load.start()
      done()
    }))
})
