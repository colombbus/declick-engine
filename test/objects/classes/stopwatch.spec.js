import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import Stopwatch from '../../../src/objects/classes/stopwatch.js'

let game = null
let scene = null

describe('When Stopwatch is instantiated', () => {
  beforeAll(
    () =>
      new Promise(done => {
        const canvasElement = document.createElement('canvas')
        canvasElement.width = 100
        canvasElement.height = 100
        document.body.append(canvasElement)
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          scene: {
            init: function() {
              scene = this
              done()
            },
          },
          callbacks: {
            postBoot: function() {
              game.loop.stop()
            },
          },
        })
      }),
  )

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
  })

  it('should instantiate and display the Stopwatch at location 0,0', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Stopwatch.setRuntime(fakeRuntime)

      const testObject = new Stopwatch()
      assert.equal('0:0:0', testObject._object.text)

      scene.load.start()
      done()
    }))

  it('should ', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Stopwatch.setRuntime(fakeRuntime)

      const testObject = new Stopwatch()

      assert.equal('0:0:0', testObject.getText())

      scene.load.start()
      done()
    }))

  it('should show time elapsed since start method called', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Stopwatch.setRuntime(fakeRuntime)
      const testObject = new Stopwatch()

      // hundredth of second
      testObject.start()
      testObject.tick(10)
      assert.equal('0:0:1', testObject._object.text)

      // tenth of second
      testObject.start()
      testObject.tick(100)
      assert.equal('0:0:10', testObject._object.text)

      // whole second
      testObject.start()
      testObject.tick(1000)
      assert.equal('0:1:0', testObject._object.text)

      // whole minute
      testObject.start()
      testObject.tick(60000)
      assert.equal('1:0:0', testObject._object.text)

      // random time
      testObject.start()
      testObject.tick(126285)
      assert.equal('2:6:28', testObject._object.text)

      scene.load.start()
      done()
    }))

  it('should revert and stay at 0 when stop is called', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Stopwatch.setRuntime(fakeRuntime)

      const testObject = new Stopwatch()
      testObject.start()
      testObject.tick(1000)
      testObject.stop()
      assert.equal('0:0:0', testObject._object.text)

      scene.load.start()
      done()
    }))

  it('should stop and keep current elapsed time when pause is called', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Stopwatch.setRuntime(fakeRuntime)

      const testObject = new Stopwatch()
      testObject.start()
      testObject.tick(1000)
      testObject.pause()
      testObject.tick(1000)
      assert.equal('0:1:0', testObject._object.text)

      scene.load.start()
      done()
    }))

  it('should start again from previous time when continue is called', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Stopwatch.setRuntime(fakeRuntime)

      const testObject = new Stopwatch()
      testObject.start()
      testObject.tick(1000)
      testObject.pause()
      testObject.continue()
      testObject.tick(1000)
      assert.equal('0:2:0', testObject._object.text)

      scene.load.start()
      done()
    }))
})
