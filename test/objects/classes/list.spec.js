import { describe, it } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import List from '../../../src/objects/classes/list.js'

describe('When List is instantiated', () => {
  it('should count length of list', () => {
    let anObject = new List()
    assert.equal(anObject.getSize(), 0)
  })

  it('should add in element list', () => {
    let anObject = new List()
    anObject.add('1')
    anObject.add('2')
    assert.equal(anObject.getSize(), 2)
  })

  it('should check if List have element', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.add('54321')
    assert.equal(anObject.has('12345'), true)
  })

  it('should remove element', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.add('54321')
    anObject.remove('12345')
    assert.equal(anObject.has('12345'), false)
  })

  it('should true if List has objects', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.remove('12345')
    assert.equal(anObject.hasObjects(), false)
  })

  it('should return first item', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.add('54321')
    anObject.add('09876')
    assert.equal(anObject.getNextObject(), '12345')
  })

  it('should true if List has objects', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.add('54321')
    anObject.add('09876')

    anObject.modify(0, '09876')

    assert.equal(anObject.getNextObject(), '09876')
  })

  it('should empty List', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.add('54321')
    anObject.add('09876')

    anObject.empty()

    assert.equal(anObject.getSize(), 0)
  })

  it('should return true if list equal to list have at least one object in common.', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.add('54321')
    anObject.add('09876')

    assert.equal(anObject.has('54321'), true)
  })

  it('should return next item in array', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.add('54321')
    anObject.add('09876')

    assert.equal(anObject.getNextObject(), '12345')
  })

  it('should return nth item in array', () => {
    let anObject = new List()
    anObject.add('12345')
    anObject.add('54321')
    anObject.add('09876')

    assert.equal(anObject.getObject(2), '09876')
  })
})
