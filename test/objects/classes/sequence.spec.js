import { describe, it } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import Sequence from '../../../src/objects/classes/sequence.js'

describe('When Sequence is instantiated', () => {
  it('should be able to save commands', () => {
    // let test = false;
    const runtime = {
      test: false,
      addObject(value) {},
      getStatementsFromCommand(command) {
        return command
      },
      executePriorityStatement(callStatement) {
        this.test = true
      },
    }
    Sequence.setRuntime(runtime)

    let sequence_object = new Sequence()
    let test_command = 'a = Text(5)'
    sequence_object.addCommand(test_command)

    assert.equal(sequence_object._commands[0].type, Sequence.TYPE_COMMAND)
    assert.equal(sequence_object._commands[0].value, test_command)
  })

  it('should ba able to add a delay between commands', () => {
    let sequence_object = new Sequence()

    // testing initial delay
    sequence_object.addDelay(1000)
    assert.equal(sequence_object._commands[0].type, Sequence.TYPE_DELAY)
    assert.equal(sequence_object._commands[0].value, 1000)

    // testing adding delays before a command
    sequence_object.addCommand('a = Text(2);')
    sequence_object.addDelay(2000)

    assert.equal(sequence_object._commands[2].type, Sequence.TYPE_DELAY)
    assert.equal(sequence_object._commands[2].value, 2000)
  })

  it('should be able to save commands', () => {
    let test = false
    const runtime = {
      addObject(value) {},
      getStatementsFromCommand(command) {
        return command
      },
      executePriorityStatements(callStatement) {
        test = true
      },
    }
    Sequence.setRuntime(runtime)

    let sequence_object = new Sequence()
    let test_command = 'a = Text(5)'
    sequence_object.addCommand(test_command)
    sequence_object.start()

    window.setTimeout(() => {
      sequence_object.stop()
      assert.isTrue(test)
    }, 0)
  })

  it("should throw an error when it's set to loop and the total delay is less than minimum loop", () => {
    let sequence_object = new Sequence()

    // testing initial delay
    sequence_object.addDelay(10)
    sequence_object.addDelay(50)
    try {
      sequence_object.loop(true)
    } catch (error) {
      assert.equal(error.declickObjectError, 'freeze warning 100')
    }
  })
})
