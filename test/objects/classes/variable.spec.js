import { describe, it } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import Variable from '../../../src/objects/classes/variable.js'

describe('When Variable is instantiated', () => {
  it('should trigger a custom listener when set with reference to instance', () => {
    const anObject = new Variable()
    let called = null
    anObject.addListener('customEvent', function() {
      called = this
    })
    anObject.dispatch('customEvent')
    assert.deepEqual(called, anObject)
  })

  it('should pass parameters to a listener', () => {
    const anObject = new Variable()
    let receivedParameter1 = null
    let receivedParameter2 = null
    anObject.addListener('customEvent', (param1, param2) => {
      receivedParameter1 = param1
      receivedParameter2 = param2
    })
    anObject.dispatch('customEvent', 1234, 5678)
    assert.equal(receivedParameter1, 1234)
    assert.equal(receivedParameter2, 5678)
  })

  it('should call runtime addOject method when instance is created', () => {
    let called = null
    const fakeRuntime = {
      addObject(value) {
        called = value
      },
    }
    Variable.setRuntime(fakeRuntime)
    const anObject = new Variable()
    assert.deepEqual(called, anObject)
  })

  it('should trigger a delete listener when instance is deleted', () => {
    let called = null
    // required because of preceding tests (runtime is maintained through baseclass)
    Variable.setRuntime(null)
    const anObject = new Variable()
    anObject.addListener('delete', function() {
      called = this
    })
    anObject.delete()
    assert.deepEqual(called, anObject)
  })

  it('should not trigger a listener when removed', () => {
    const anObject = new Variable()
    let called = false
    anObject.addListener('customEvent', () => {
      called = true
    })
    anObject.removeListener('customEvent')
    anObject.dispatch('customEvent', anObject)
    assert.equal(called, false)
  })
})
