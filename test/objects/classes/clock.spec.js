import { describe, it } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import Clock from '../../../src/objects/classes/clock.js'

describe('When Clock is instantiated', () => {
  it('should be able to save and execute commands', () => {
    let test = false
    const runtime = {
      addObject(value) {},
      getStatementsFromCommand(command) {
        return command
      },
      executePriorityStatements(callStatement) {
        test = true
      },
    }
    Clock.setRuntime(runtime)
    let clock_object = new Clock()

    clock_object.addCommand('a = 5;')
    clock_object.start()

    window.setTimeout(() => {
      clock_object.stop()
      assert.isTrue(test)
    }, 0)
  })

  it('should ba able to add a delay between commands', () => {
    let clock_object = new Clock()
    // testing initial delay
    clock_object.setDelay(1000)
    assert.equal(clock_object._delay, 1000)
  })
})
