import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import pixelmatch from 'pixelmatch'
import Image from '../../../src/objects/classes/image.js'

let game = null
let scene = null

describe('When Image is instantiated', () => {
  beforeAll(
    () =>
      new Promise(done => {
        const canvasElement = document.createElement('canvas')
        canvasElement.width = 100
        canvasElement.height = 100
        document.body.append(canvasElement)
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          //@ts-ignore
          loader: { imageLoadType: 'HTMLImageElement' },
          scene: {
            init: function() {
              scene = this
              done()
            },
          },
          callbacks: {
            postBoot: function() {
              game.loop.stop()
            },
          },
        })
      }),
  )

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
  })

  it('should display the given image at location 0,0', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Image.setRuntime(fakeRuntime)
      scene.load.image('test', `file://${__dirname}/dk.png`)
      scene.load.once('complete', () => {
        const testObject = new Image('test')
        game.renderer.snapshotArea(
          0,
          0,
          45,
          45,
          image => {
            const canvas1 = document.createElement('canvas')
            const ctx1 = canvas1.getContext('2d')
            ctx1.drawImage(image, 0, 0)
            const canvas2 = document.createElement('canvas')
            canvas2.height = 45
            canvas2.width = 45
            const ctx2 = canvas2.getContext('2d')
            const testImage = document.createElement('img')
            testImage.onload = () => {
              ctx2.drawImage(testImage, 0, 0)
              const test = pixelmatch(
                ctx1.getImageData(0, 0, 45, 45).data,
                ctx2.getImageData(0, 0, 45, 45).data,
                null,
                45,
                45,
                { threshold: 0.1 },
              )
              try {
                assert.isBelow(test, 2)
                done()
              } catch (e) {
                done(e)
              }
            }
            testImage.src = `file://${__dirname}/dk.png`
          },
          'image/png',
        )
      })
      scene.load.start()
    }))
})
