import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import Text from '../../../src/objects/classes/text.js'

let game = null
let scene = null

describe('When Text is instantiated', () => {
  beforeAll(
    () =>
      new Promise(done => {
        const canvasElement = document.createElement('canvas')
        canvasElement.width = 100
        canvasElement.height = 100
        document.body.append(canvasElement)
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          scene: {
            init: function() {
              scene = this
              done()
            },
          },
          callbacks: {
            postBoot: function() {
              game.loop.stop()
            },
          },
        })
      }),
  )

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
  })

  it('should display the Text at location 0,0', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Text.setRuntime(fakeRuntime)

      const testObject = new Text('test')
      assert.equal('test', testObject._object.text)
      assert.equal('test', testObject.getText())

      scene.load.start()
      done()
    }))

  it('should allow to change its text', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Text.setRuntime(fakeRuntime)

      const testObject = new Text()
      testObject.setText('test')
      assert.equal('test', testObject._object.text)
      assert.equal('test', testObject.getText())

      scene.load.start()
      done()
    }))

  it('should allow to change its color', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Text.setRuntime(fakeRuntime)

      const testObject = new Text('test')
      testObject.setColor(0, 255, 0)
      assert.equal('#00ff00', testObject._object.style.color)

      scene.load.start()
      done()
    }))

  it('should allow to change its text size', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Text.setRuntime(fakeRuntime)

      const testObject = new Text()
      testObject.setTextSize(40)
      assert.equal('40px', testObject._object.style.fontSize)

      scene.load.start()
      done()
    }))

  it('should allow to change its text', () =>
    new Promise(done => {
      const fakeGraphics = {
        addObject() {},
        getScene() {
          return scene
        },
      }

      const fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
      }
      Text.setRuntime(fakeRuntime)

      const testObject = new Text()
      testObject.setFont('Times New Roman')
      assert.equal('Times New Roman', testObject._object.style.fontFamily)

      scene.load.start()
      done()
    }))
})
