import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import Robot from '../../../src/objects/classes/robot.js'

let game = null
let scene = null
let fakeGraphics = null
let fakeRuntime = null
let objects = []

describe('When Robot is instantiated', () => {
  beforeAll(
    () =>
      new Promise(done => {
        const canvasElement = document.createElement('canvas')
        canvasElement.width = 100
        canvasElement.height = 100
        document.body.append(canvasElement)
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          physics: {
            default: 'arcade',
            arcade: {
              gravity: { y: 0 },
              debug: false,
            },
          },
          scene: {
            init: function() {
              scene = this
              done()
            },
          },
          callbacks: {},
        })

        fakeGraphics = {
          addObject(object) {
            objects.push(object)
          },
          getScene() {
            return scene
          },
          removeObject() {},
        }

        fakeRuntime = {
          addObject() {},
          getGraphics() {
            return fakeGraphics
          },
          deleteObject() {},
          suspend() {},
          resume() {},
        }
      }),
  )

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
        update: function(time, delta) {
          objects.forEach(object => {
            object.tick(delta)
          })
        },
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
    objects = []
  })

  it('should initialize the object', () =>
    new Promise(done => {
      Robot.setRuntime(fakeRuntime)
      const testObject = new Robot()
      done()
    }))

  // Basic Movements
  it('should allow to move the object forward', () =>
    new Promise(done => {
      Robot.setRuntime(fakeRuntime)
      const testObject = new Robot()

      scene.load.start()
      testObject.moveForward(1)
      const step_length = testObject.getStepLength()
      window.setTimeout(() => {
        try {
          assert.equal(testObject._object.x, step_length)
          assert.equal(testObject._object.y, 0)
          done()
        } catch (error) {
          done(error)
        }
      }, 1000)
    }))

  it('should allow to move the object backwards', () =>
    new Promise(done => {
      Robot.setRuntime(fakeRuntime)
      const testObject = new Robot()

      scene.load.start()
      testObject.moveBackward(1)
      const step_length = testObject.getStepLength()
      window.setTimeout(() => {
        try {
          assert.equal(testObject._object.x, -step_length)
          assert.equal(testObject._object.y, 0)
          done()
        } catch (error) {
          done(error)
        }
      }, 1000)
    }))

  it('should allow to move the object downwards', () =>
    new Promise(done => {
      Robot.setRuntime(fakeRuntime)
      const testObject = new Robot()

      scene.load.start()
      testObject.moveDownward(2)
      const step_length = testObject.getStepLength()
      window.setTimeout(() => {
        try {
          assert.equal(testObject._object.x, 0)
          assert.equal(testObject._object.y, 2 * step_length)
          done()
        } catch (error) {
          done(error)
        }
      }, 1000)
    }))

  it('should allow to move the object upwards', () =>
    new Promise(done => {
      Robot.setRuntime(fakeRuntime)
      const testObject = new Robot()

      scene.load.start()
      testObject.moveUpward(2)
      const step_length = testObject.getStepLength()
      window.setTimeout(() => {
        try {
          assert.equal(testObject._object.x, 0)
          assert.equal(testObject._object.y, -2 * step_length)
          done()
        } catch (error) {
          done(error)
        }
      }, 1000)
    }))

  it('should allow to change its step length', () =>
    new Promise(done => {
      Robot.setRuntime(fakeRuntime)
      const testObject = new Robot()

      scene.load.start()

      testObject.setStepLength(20)
      testObject.moveForward(1)

      const step_length = testObject.getStepLength()
      window.setTimeout(() => {
        try {
          assert.equal(step_length, 20, 'step length not equal')
          assert.equal(testObject._object.x, step_length, 'x not equal')
          assert.equal(testObject._object.y, 0, 'y not equal')
          done()
        } catch (error) {
          done(error)
        }
      }, 1000)
    }))
})
