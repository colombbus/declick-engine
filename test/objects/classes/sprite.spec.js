import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import pixelmatch from 'pixelmatch'
import Sprite from '../../../src/objects/classes/sprite.js'
import Item from '../../../src/objects/classes/item.js'
import Block from '../../../src/objects/classes/block.js'

let game = null
let scene = null
let fakeGraphics = null
let fakeRuntime = null
let objects = []

describe('When Sprite is instantiated', () => {
  beforeAll(
    () =>
      new Promise(done => {
        const canvasElement = document.createElement('canvas')
        canvasElement.width = 100
        canvasElement.height = 100
        document.body.append(canvasElement)
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          //@ts-ignore
          loader: { imageLoadType: 'HTMLImageElement' },
          physics: {
            default: 'arcade',
            arcade: {
              gravity: { y: 0 },
              debug: false,
            },
          },
          scene: {
            init: function() {
              scene = this
              done()
            },
          },
          callbacks: {},
        })

        fakeGraphics = {
          addObject(object) {
            objects.push(object)
          },
          getScene() {
            return scene
          },
          removeObject() {},
          addLocalResource() {},
        }

        fakeRuntime = {
          addObject() {},
          getGraphics() {
            return fakeGraphics
          },
          deleteObject() {},
        }
      }),
  )

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
        update: function(time, delta) {
          objects.forEach(object => {
            object.tick(delta)
          })
        },
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
    objects = []
  })

  // Test sprite display
  it('should display the given Sprite at location 0,0', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      scene.load.image('test', `file://${__dirname}/dk.png`)
      scene.load.once('complete', () => {
        const testObject = new Sprite('test')
        game.renderer.snapshotArea(
          0,
          0,
          45,
          45,
          image => {
            const canvas1 = document.createElement('canvas')
            const ctx1 = canvas1.getContext('2d')
            ctx1.drawImage(image, 0, 0)
            const canvas2 = document.createElement('canvas')
            canvas2.height = 45
            canvas2.width = 45
            const ctx2 = canvas2.getContext('2d')
            const testImage = document.createElement('img')
            testImage.onload = () => {
              ctx2.drawImage(testImage, 0, 0)
              const test = pixelmatch(
                ctx1.getImageData(0, 0, 45, 45).data,
                ctx2.getImageData(0, 0, 45, 45).data,
                null,
                45,
                45,
                { threshold: 0.1 },
              )
              try {
                assert.isBelow(test, 2)
                done()
              } catch (e) {
                done(e)
              }
            }
            testImage.src = `file://${__dirname}/dk.png`
          },
          'image/png',
        )
      })
      scene.load.start()
    }))

  // Basic Movements
  it('should allow to move the object forward', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()
      testObject.moveForward(50)
      window.setTimeout(() => {
        try {
          assert.equal(testObject.getX(), 50)
          assert.equal(testObject.getY(), 0)
          done()
        } catch (error) {
          done(error)
        }
      }, 300)
    }))

  it('should allow to move the object backwards', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()
      testObject.moveBackward(50)
      window.setTimeout(() => {
        try {
          assert.equal(testObject.getX(), -50)
          assert.equal(testObject.getY(), 0)
          done()
        } catch (error) {
          done(error)
        }
      }, 300)
    }))

  it('should allow to move the object upwards', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()
      testObject.moveUpward(50)
      window.setTimeout(() => {
        try {
          assert.equal(testObject.getX(), 0)
          assert.equal(testObject.getY(), -50)
          done()
        } catch (error) {
          done(error)
        }
      }, 300)
    }))

  it('should allow to move the object downwards', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()
      testObject.moveDownward(50)
      window.setTimeout(() => {
        try {
          assert.equal(testObject.getX(), 0)
          assert.equal(testObject.getY(), 50)
          done()
        } catch (error) {
          done(error)
        }
      }, 300)
    }))

  it('should allow to stop the object movement', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()

      let temp_x = 0
      testObject.moveForward(100)
      window.setTimeout(() => {
        testObject.stop()
        temp_x = testObject.getX()
      }, 100)
      window.setTimeout(() => {
        try {
          assert.equal(testObject.getX(), temp_x)
          done()
        } catch (error) {
          done(error)
        }
      }, 200)
    }))

  it('should allow to move the object always forward', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()
      testObject.moveAlwaysForward()

      let temp_x
      window.setTimeout(() => {
        assert.isAbove(testObject.getX(), 0)
        temp_x = testObject.getX()
      }, 100)

      window.setTimeout(() => {
        try {
          assert.isAbove(testObject.getX(), temp_x)
          done()
        } catch (error) {
          done(error)
        }
      }, 200)
    }))

  it('should allow to move the object always backwards', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()
      testObject.moveAlwaysBackward()

      let temp_x
      window.setTimeout(() => {
        assert.isBelow(testObject.getX(), 0)
        temp_x = testObject.getX()
      }, 100)

      window.setTimeout(() => {
        try {
          assert.isBelow(testObject.getX(), temp_x)
          done()
        } catch (error) {
          done(error)
        }
      }, 200)
    }))

  it('should allow to move the object always upwards', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()
      testObject.moveAlwaysUpward()

      let temp_y
      window.setTimeout(() => {
        assert.isBelow(testObject.getY(), 0)
        temp_y = testObject.getY()
      }, 100)

      window.setTimeout(() => {
        try {
          assert.isBelow(testObject.getY(), temp_y)
          done()
        } catch (error) {
          done(error)
        }
      }, 200)
    }))

  it('should allow to move the object always downwards', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      const testObject = new Sprite()

      scene.load.start()
      testObject.moveAlwaysDownward()

      let temp_y
      window.setTimeout(() => {
        assert.isAbove(testObject.getY(), 0)
        temp_y = testObject.getY()
      }, 100)

      window.setTimeout(() => {
        try {
          assert.isAbove(testObject.getY(), temp_y)
          done()
        } catch (error) {
          done(error)
        }
      }, 200)
    }))

  it('should allow to run command on collision with object', () =>
    new Promise(done => {
      let testCalled = false
      let called = null

      fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        deleteObject() {},
        getStatementsFromCommand(command) {
          return command
        },
        executePriorityStatements(commands) {
          testCalled = true
          called = commands
        },
      }

      Sprite.setRuntime(fakeRuntime)
      Block.setRuntime(fakeRuntime)

      const testObject = new Sprite()
      const testCollider = new Block()
      // Set object declick id to bypass check arguments
      //@ts-ignore
      testCollider._declickId_ = '0001'

      testCollider.setLocation(100, 0)
      let test_command = 'a = Text(5)'
      testObject.ifCollisionWith(testCollider, test_command)

      scene.load.start()
      testObject.moveForward(100)

      window.setTimeout(() => {
        try {
          assert.equal(testCalled, true)
          assert.equal(called, test_command)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))

  it('should allow to add a blocking object', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      Block.setRuntime(fakeRuntime)

      const testObject = new Sprite()
      const testBlock = new Block()
      // Set object declick id to bypass check arguments
      //@ts-ignore
      testBlock._declickId_ = '0001'
      testBlock.setLocation(50, 0)
      testObject.addBlock(testBlock)

      scene.load.start()
      testObject.moveForward(100)

      window.setTimeout(() => {
        try {
          assert.isAtMost(testObject.getX(), 50)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))

  it('should allow to run command on overlap with object', () =>
    new Promise(done => {
      let testCalled = false
      let called = null

      fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        deleteObject() {},
        getStatementsFromCommand(command) {
          return command
        },
        executePriorityStatements(commands) {
          testCalled = true
          called = commands
        },
      }

      Sprite.setRuntime(fakeRuntime)

      const testObject = new Sprite()
      const testCollider = new Sprite()
      // Set object declick id to bypass check arguments
      //@ts-ignore
      testCollider._declickId_ = '0001'

      testCollider.setLocation(100, 0)
      let test_command = 'a = Text(5)'
      testObject.ifOverlapWith(testCollider, test_command)

      scene.load.start()
      testObject.moveForward(100)

      window.setTimeout(() => {
        try {
          assert.equal(testCalled, true)
          assert.equal(called, test_command)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))

  it('should allow to grab item and carry it', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      Item.setRuntime(fakeRuntime)
      Item.setup()

      const testObject = new Sprite()
      const testItem = new Item()
      // Set object declick id to bypass check arguments
      //@ts-ignore
      testItem._declickId_ = '0001'

      scene.load.start()
      testObject.mayCatch(testItem)

      testObject.moveForward(100)
      testObject.moveDownward(100)
      window.setTimeout(() => {
        try {
          assert.equal(testItem._catcher, testObject)
          assert.equal(testItem._object.x, 100)
          assert.equal(testItem._object.y, 100)
          done()
        } catch (error) {
          done(error)
        }
      }, 1000)
    }))

  it('should allow to execute command when catching item', () =>
    new Promise(done => {
      let testCalled = false
      let called = null

      fakeRuntime = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        deleteObject() {},
        getStatementsFromCommand(command) {
          return command
        },
        executePriorityStatements(commands) {
          testCalled = true
          called = commands
        },
      }

      Sprite.setRuntime(fakeRuntime)
      Item.setRuntime(fakeRuntime)
      Item.setup()

      const testObject = new Sprite()
      const testItem = new Item()
      testItem.setLocation(100, 0)
      let test_command = 'a = Text(5)'
      // Set object declick id to bypass check arguments
      //@ts-ignore
      testItem._declickId_ = '0001'

      scene.load.start()
      testObject.ifCatch(testItem, test_command)

      testObject.moveForward(100)

      window.setTimeout(() => {
        try {
          assert.equal(testCalled, true)
          assert.equal(called, test_command)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))

  it('should know whether it is on a item', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      Item.setRuntime(fakeRuntime)
      Item.setup()

      const testObject = new Sprite()
      const testItem = new Item()

      testObject.setLocation(100, 100)
      testItem.setLocation(100, 100)
      // Set object declick id to bypass check arguments
      //@ts-ignore
      testItem._declickId_ = '0001'

      scene.load.start()

      assert.equal(testObject.isOnItem(testItem), true)
      done()
    }))

  it('should allow to drop an item it is carrying', () =>
    new Promise(done => {
      Sprite.setRuntime(fakeRuntime)
      Item.setRuntime(fakeRuntime)
      Item.setup()

      const testObject = new Sprite()
      const testItem = new Item()
      // Set object declick id to bypass check arguments
      //@ts-ignore
      testItem._declickId_ = '0001'

      scene.load.start()
      testObject.mayCatch(testItem)
      testObject.moveForward(100)
      testObject.moveDownward(100)
      window.setTimeout(() => {
        try {
          assert.equal(testItem._catcher, testObject)
          testObject.drop(testItem)
        } catch (error) {
          done(error)
        }
      }, 100)

      window.setTimeout(() => {
        try {
          // assert.equal(testItem.catcher, null)
          assert.isBelow(testItem._object.x, 50)
          assert.isBelow(testItem._object.y, 50)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))
})
