import { describe, it, beforeAll, beforeEach, afterEach } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import * as Phaser from 'phaser'
import SpriteGroup from '../../../src/objects/classes/sprite-group.js'
import SpriteGroupItem from '../../../src/objects/classes/sprite-group-item.js'

let game = null
let scene = null
let fakeGraphics = null
let fakeRuntime = null
let objects = []

describe('When Sprite Group is instantiated', () => {
  beforeAll(
    () =>
      new Promise(done => {
        const canvasElement = document.createElement('canvas')
        canvasElement.width = 100
        canvasElement.height = 100
        document.body.append(canvasElement)
        game = new Phaser.Game({
          type: Phaser.CANVAS,
          canvas: canvasElement,
          physics: {
            default: 'arcade',
            arcade: {
              gravity: { y: 0 },
              debug: false,
            },
          },
          scene: {
            init: function() {
              scene = this
              done()
            },
          },
          callbacks: {},
        })

        fakeGraphics = {
          addObject(object) {
            objects.push(object)
          },
          getScene() {
            return scene
          },
          removeObject() {},
        }

        fakeRuntime = {
          addObject() {},
          getGraphics() {
            return fakeGraphics
          },
          deleteObject() {},
          suspend() {},
          resume() {},
        }
      }),
  )

  beforeEach(() => {
    scene = game.scene.add(
      'main',
      {
        active: true,
        update: function(time, delta) {
          objects.forEach(object => {
            object.tick(delta)
          })
        },
      },
      true,
    )
  })

  afterEach(() => {
    scene.scene.remove()
    objects = []
  })

  it('should initialize the Sprite group', () =>
    new Promise(done => {
      SpriteGroup.setRuntime(fakeRuntime)
      const testObject = new SpriteGroup()
      done()
    }))

  it('should create a Sprite group item', () =>
    new Promise(done => {
      SpriteGroup.setRuntime(fakeRuntime)
      SpriteGroupItem.setRuntime(fakeRuntime)
      const testObject = new SpriteGroup()
      const item = testObject.createSprite(0, 0)
      assert.instanceOf(item, SpriteGroupItem)
      done()
    }))

  it('should forward collision handlers to created items (no pool mode)', () =>
    new Promise(done => {
      scene.load.start()
      let testCalled = false
      let called = null

      const fakeRuntime2 = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        deleteObject() {},
        getStatementsFromCommand(command) {
          return command
        },
        executePriorityStatements(commands) {
          testCalled = true
          called = commands
        },
      }

      SpriteGroup.setRuntime(fakeRuntime2)
      SpriteGroupItem.setRuntime(fakeRuntime2)

      const testObject1 = new SpriteGroup()
      const testObject2 = new SpriteGroup()

      const testCommand = 'a = new Text(5)'

      // Set object declick id to bypass check arguments
      //@ts-ignore
      testObject2._declickId_ = '0002'
      testObject1.ifCollisionWith(testObject2, testCommand)

      const item1 = testObject1.createSprite(0, 0)
      const item2 = testObject2.createSprite(50, 0)
      item1.moveForward(50)

      window.setTimeout(() => {
        try {
          assert.isTrue(testCalled)
          assert.equal(called, testCommand)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))

  it('should forward collision handlers to created items (pool mode)', () =>
    new Promise(done => {
      scene.load.start()
      let testCalled = false
      let called = null

      const fakeRuntime2 = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        deleteObject() {},
        getStatementsFromCommand(command) {
          return command
        },
        executePriorityStatements(commands) {
          testCalled = true
          called = commands
        },
      }

      SpriteGroup.setRuntime(fakeRuntime2)
      SpriteGroupItem.setRuntime(fakeRuntime2)

      const testObject1 = new SpriteGroup(2)
      const testObject2 = new SpriteGroup(2)

      const testCommand = 'a = new Text(5)'

      // Set object declick id to bypass check arguments
      //@ts-ignore
      testObject2._declickId_ = '0002'
      testObject1.ifCollisionWith(testObject2, testCommand)

      const item1 = testObject1.createSprite(0, 0)
      const item2 = testObject2.createSprite(50, 0)
      item1.moveForward(50)

      window.setTimeout(() => {
        try {
          assert.isTrue(testCalled)
          assert.equal(called, testCommand)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))

  it('a deleted item should not trigger collision (no pool mode)', () =>
    new Promise(done => {
      scene.load.start()
      let testCalled = false
      let called = null

      const fakeRuntime2 = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        deleteObject() {},
        getStatementsFromCommand(command) {
          return command
        },
        executePriorityStatements(commands) {
          testCalled = true
          called = commands
        },
      }

      SpriteGroup.setRuntime(fakeRuntime2)
      SpriteGroupItem.setRuntime(fakeRuntime2)

      const testObject1 = new SpriteGroup()
      const testObject2 = new SpriteGroup()

      const testCommand = 'a = new Text(5)'

      // Set object declick id to bypass check arguments
      //@ts-ignore
      testObject2._declickId_ = '0002'
      testObject1.ifCollisionWith(testObject2, testCommand)

      const item1 = testObject1.createSprite(0, 0)
      const item2 = testObject2.createSprite(50, 0)
      item2.delete()
      item1.moveForward(50)

      window.setTimeout(() => {
        try {
          assert.isFalse(testCalled)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))

  it('a deleted item should not trigger collision (pool mode)', () =>
    new Promise(done => {
      scene.load.start()
      let testCalled = false
      let called = null

      const fakeRuntime2 = {
        addObject() {},
        getGraphics() {
          return fakeGraphics
        },
        deleteObject() {},
        getStatementsFromCommand(command) {
          return command
        },
        executePriorityStatements(commands) {
          testCalled = true
          called = commands
        },
      }

      SpriteGroup.setRuntime(fakeRuntime2)
      SpriteGroupItem.setRuntime(fakeRuntime2)

      const testObject1 = new SpriteGroup(2)
      const testObject2 = new SpriteGroup(2)

      const testCommand = 'a = new Text(5)'

      // Set object declick id to bypass check arguments
      //@ts-ignore
      testObject2._declickId_ = '0002'
      testObject1.ifCollisionWith(testObject2, testCommand)

      const item1 = testObject1.createSprite(0, 0)
      const item2 = testObject2.createSprite(50, 0)
      item2.delete()
      item1.moveForward(50)

      window.setTimeout(() => {
        try {
          assert.isFalse(testCalled)
          done()
        } catch (error) {
          done(error)
        }
      }, 500)
    }))

  it('should delete items when the group is deleted (no pool mode)', () =>
    new Promise(done => {
      scene.load.start()
      SpriteGroup.setRuntime(fakeRuntime)
      SpriteGroupItem.setRuntime(fakeRuntime)
      const testObject1 = new SpriteGroup()
      const item1 = testObject1.createSprite(0, 0)
      const item2 = testObject1.createSprite(50, 50)
      testObject1.delete()
      assert.isNull(item1._object)
      assert.isNull(item2._object)
      done()
    }))

  it('should delete items when the group is deleted (pool mode)', () =>
    new Promise(done => {
      scene.load.start()
      SpriteGroup.setRuntime(fakeRuntime)
      SpriteGroupItem.setRuntime(fakeRuntime)
      const testObject1 = new SpriteGroup(2)
      const item1 = testObject1.createSprite(0, 0)
      const item2 = testObject1.createSprite(0, 0)
      const object1 = item1._object
      const object2 = item2._object
      testObject1.delete()
      assert.isNull(item1._object)
      assert.isNull(item2._object)
      assert.isUndefined(object1.body)
      assert.isFalse(object1.active)
      assert.isFalse(object1.visible)
      assert.isUndefined(object1.scene)
      assert.isUndefined(object1.parentContainer)
      assert.isUndefined(object2.body)
      assert.isFalse(object2.active)
      assert.isFalse(object2.visible)
      assert.isUndefined(object2.scene)
      assert.isUndefined(object2.parentContainer)
      done()
    }))
})
