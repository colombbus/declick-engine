import { describe, it } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import KeyboardClass from '../../../src/objects/instances/keyboard.js'

describe('When keyboard is instantiated', () => {
  it('should send to runtime properties to expose', () => {
    let properties = null
    const runtime = {
      exposeProperties(value) {
        properties = value
      },
      addInstance() {},
    }
    const keyboard = new KeyboardClass()
    keyboard.setRuntime(runtime)
    keyboard.dispatch('runtimeInitialized')
    assert.isNotNull(properties)
  })
})
