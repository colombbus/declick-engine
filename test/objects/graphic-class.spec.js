import { describe, it } from 'vitest'
import { assert } from 'chai'
import 'reflect-metadata'
import GraphicClass from '../../src/objects/graphic-class.js'

describe('When GraphicClass is instantiated', () => {
  it('should record reference to graphics and every instance should have it', () => {
    const fakeGraphics = {
      addObject() {},
    }

    const fakeRuntime = {
      addObject() {},
      getGraphics() {
        return fakeGraphics
      },
    }
    GraphicClass.setRuntime(fakeRuntime)
    const anObject = new GraphicClass()
    assert.deepEqual(anObject._graphics, fakeGraphics)
  })

  it('should call graphics addObject method when instance is created', () => {
    let called = null
    const fakeGraphics = {
      addObject(value) {
        called = value
      },
    }

    const fakeRuntime = {
      addObject() {},
      getGraphics() {
        return fakeGraphics
      },
    }

    GraphicClass.setRuntime(fakeRuntime)
    const anObject = new GraphicClass()
    assert.deepEqual(called, anObject)
  })

  it('should call graphics removeObject method when instance is deleted', () => {
    let called = null
    const fakeGraphics = {
      addObject() {},
      removeObject(value) {
        called = value
      },
    }

    const fakeRuntime = {
      addObject() {},
      deleteObject() {},
      getGraphics() {
        return fakeGraphics
      },
    }
    GraphicClass.setRuntime(fakeRuntime)
    const anObject = new GraphicClass()
    anObject.delete()
    assert.deepEqual(called, anObject)
  })
})
