import { describe, it, beforeAll, afterAll } from 'vitest'
import { assert } from 'chai'
import { i18n } from '@lingui/core'
import ennglishTranslations from '../../translations/translation.fr.json'
import frenchTranslations from '../../translations/translation.fr.json'
import loader from '../../src/objects/loader.js'

describe('When Loader is imported', () => {
  beforeAll(function() {
    i18n.load('fr', frenchTranslations)
    i18n.activate('fr')
  })

  afterAll(function() {
    i18n.load('en', ennglishTranslations)
    i18n.activate('en')
  })

  it('should load French translated classes', () => {
    const objects = loader.load()
    return import('../../src/objects/classes/variable').then(
      ({ default: Variable }) => {
        let variableClassData = objects.find(item => item.name === 'Variable')
        assert.equal(variableClassData.instance, false)
        assert.deepEqual(variableClassData.object, Variable)
        assert.deepEqual(variableClassData.methods.get('définirTexte'), {
          name: 'setText',
          help: 'définirTexte("texte")',
          async: false,
        })
        assert.deepEqual(variableClassData.methods.get('supprimer'), {
          name: 'delete',
          help: 'supprimer()',
          async: false,
        })
        assert.equal(
          variableClassData.object.prototype._declickId_,
          'classes/variable',
        )
      },
    )
  })

  it('should load French translated instances', () => {
    const objects = loader.load()
    return import('../../src/objects/instances/declick').then(
      ({ default: DeclickClass }) => {
        let declickData = objects.find(item => item.name === 'declick')
        assert.isTrue(declickData.instance)
        assert.deepEqual(declickData.object, DeclickClass)
        assert.deepEqual(declickData.methods.get('écrire'), {
          name: 'write',
          help: 'écrire("texte")',
          async: false,
        })
        assert.deepEqual(declickData.methods.get('initialiser'), {
          name: 'clear',
          help: 'initialiser()',
          async: false,
        })
        assert.equal(
          declickData.object.prototype._declickId_,
          'instances/declick',
        )
      },
    )
  })
})
