import { describe, it, beforeAll, afterAll, beforeEach } from 'vitest'
import { assert } from 'chai'
import runtime from '../../src/runtime/main.js'
import { i18n } from '@lingui/core'
import frenchTranslations from '../../translations/translation.fr.json'
import englishTranslations from '../../translations/translation.en.json'
import 'regenerator-runtime/runtime'

describe('When runtime is initialized', () => {
  let classResult = null
  let instanceResult = false

  beforeAll(() => {
    let MyClass = class {
      setResult() {
        instanceResult = true
      }
      addListener() {}
      setRuntime(value) {
        this.runtime = value
        this.runtime.addInstance(this)
      }
      dispatch() {}
      loadProgram(value, callback) {
        this.runtime
          .loadProgram(value)
          .then(() => {
            callback()
          })
          .catch(e => {
            callback(e)
          })
      }
    }
    const methods = new Map([
      ['exposedSetResult', { name: 'setResult', async: false }],
      ['exposedLoadProgram', { name: 'loadProgram', async: true }],
    ])

    let MyClass2 = class {
      constructor(value) {
        this.value = value
      }

      setResult() {
        classResult = this.value
      }
      static setRuntime() {}
    }

    let MyClass3 = class {
      getResult() {
        //@ts-ignore
        return this.constructor.setupCalled ? true : false
      }
      static setRuntime() {}

      static setup() {
        this.setupCalled = true
      }

      asyncFunction(callback) {
        new Promise((resolve, reject) => {
          window.setTimeout(() => {
            classResult = true
            resolve()
          }, 200)
        }).then(() => {
          callback()
        })
      }

      asyncErroneousFunction(callback) {
        new Promise((resolve, reject) => {
          window.setTimeout(() => {
            resolve()
          }, 200)
        })
          .then(() => {
            throw new Error('test')
          })
          .catch(e => {
            callback(e)
          })
      }

      asyncErroneousNotAsyncFunction(callback) {
        callback(new Error('test'))
      }

      asyncFunctionWithValue(callback) {
        new Promise((resolve, reject) => {
          window.setTimeout(() => {
            classResult = true
            resolve()
          }, 200)
        }).then(() => {
          callback(456)
        })
      }
    }

    const methods2 = new Map([
      ['exposedSetResult', { name: 'setResult', async: false }],
    ])
    const methods3 = new Map([
      ['exposedGetResult', { name: 'getResult', async: false }],
      ['exposedAsyncFunction', { name: 'asyncFunction', async: true }],
      [
        'exposedAsyncFunctionWithValue',
        { name: 'asyncFunctionWithValue', async: true },
      ],
      [
        'exposedErroneousAsyncFunction',
        { name: 'asyncErroneousFunction', async: true },
      ],
      [
        'exposedErroneousAsyncNotAsyncFunction',
        { name: 'asyncErroneousNotAsyncFunction', async: true },
      ],
    ])

    MyClass2.prototype.className = 'MyClass2'

    i18n.load('fr', frenchTranslations)
    i18n.activate('fr')

    return runtime.initialize([
      { instance: true, name: 'anInstance', object: MyClass, methods: methods },
      { instance: false, name: 'aClass', object: MyClass2, methods: methods2 },
      {
        instance: false,
        name: 'anotherClass',
        object: MyClass3,
        methods: methods3,
      },
    ])
  })

  afterAll(() => {
    i18n.load('en', englishTranslations)
    i18n.activate('en')
    runtime.reset()
  })

  beforeEach(() => {
    runtime.clear()
    classResult = false
    instanceResult = false
  })

  it('should be able to execute code', () => {
    runtime.executeCode('a = 5')
    assert.equal(runtime.getLastValue(), 5)
  })

  it('should be able to use declared instance', () => {
    runtime.executeCode('anInstance.exposedSetResult()')
    assert.ok(instanceResult)
  })

  it('should be able to create an instance of a declared class', () => {
    runtime.executeCode(`yo = new aClass(57)
    yo.exposedSetResult()`)
    assert.equal(classResult, 57)
  })

  it('should be able to retrieve the name of a created instance', () => {
    runtime.executeCode(`yep = new aClass(57)
    yo = new aClass(yep)
    yo.exposedSetResult()`)
    assert.equal(runtime.getDeclickObjectName(classResult), 'yep')
  })

  it('should be able to retrieve the value of a variable', () => {
    runtime.executeCode(`a = 42
    b = true
    c = {a:5}`)
    assert.equal(runtime.getDeclickVariable('a'), 42)
    assert.equal(runtime.getDeclickVariable('b'), true)
    assert.deepEqual(runtime.getDeclickVariable('c'), { a: 5 })
  })

  //TODO: this test does not succeed when executed only
  it('should be able to execute code with translated name of repeat statement', () => {
    runtime.executeCode(`a=0
    répéter(3) {
      a++
    }
    yo = new aClass(a)
    yo.exposedSetResult()`)
    assert.equal(classResult, 3)
  })

  it('should call static setup method if present', () => {
    runtime.executeCode(`a = new anotherClass()
    a.exposedGetResult()`)
    assert.equal(runtime.getLastValue(), true)
  })

  it('should be able to add an external function', () => {
    let testFunction = function(a, b) {
      return a === 3 && b === 'aText'
    }
    runtime.addExternalFunction('exposedFunction', testFunction)
    runtime.executeCode("exposedFunction(3, 'aText')")
    assert.equal(runtime.getLastValue(), true)
  })

  it('should create statements from a command which contains code', () => {
    let value = runtime.getStatementsFromCommand('a = 5')
    let referenceValue = runtime.getStatements('a = 5')
    assert.deepEqual(value, referenceValue.body)
  })

  it('should create a call statement from a command which contains a function', () => {
    let code = `function a() {
      return true
    }
    a`
    runtime.executeCode(code)
    let functionObject = runtime.getLastValue()
    let value = runtime.getStatementsFromCommand(functionObject)
    assert.equal(value[0].type, 'InnerCallExpression')
  })

  it('should wait for an async function to be executed inside declick', () =>
    new Promise(done => {
      let code = `test = new anotherClass()
    result = test.exposedAsyncFunction()
    `
      runtime.executeCode(code)
      window.setTimeout(() => {
        assert.equal(classResult, true)
        done()
      }, 500)
    }))

  it('should wait for async function to be executed outside declick', () =>
    new Promise(done => {
      let code = `test = new anotherClass()
    result = test.exposedAsyncFunction()
    `
      runtime.executeCode(code, () => {
        assert.equal(classResult, true)
        done()
      })
    }))

  it('should allow to retrieve value returned from an async function', () =>
    new Promise(done => {
      let code = `test = new anotherClass()
    result = test.exposedAsyncFunctionWithValue()
    `
      runtime.executeCode(code)
      window.setTimeout(() => {
        assert.equal(runtime.getLastValue(), 456)
        done()
      }, 500)
    }))

  it('should throw declick error when an async function throws error', () =>
    new Promise(done => {
      let code = `test = new anotherClass()
    test.exposedErroneousAsyncFunction()
    `
      runtime.setErrorHandler(error => {
        assert.equal(error.getMessage(), 'Error: test')
        assert.equal(error.getStart().line, 2)
        done()
      })
      runtime.executeCode(code)
    }))

  it('should throw declick error when an async function throws error synchronously', () =>
    new Promise(done => {
      let code = `test = new anotherClass()
    test.exposedErroneousAsyncNotAsyncFunction()
    `
      runtime.setErrorHandler(error => {
        assert.equal(error.getMessage(), 'Error: test')
        assert.equal(error.getStart().line, 2)
        done()
      })
      runtime.executeCode(code)
    }))

  it('should load another program provided aynchronously', () =>
    new Promise(done => {
      runtime.setProgramProvider(aProgram => {
        return new Promise(resolve => {
          window.setTimeout(() => {
            resolve(`a = 7600`)
          }, 200)
        })
      })
      let code = `anInstance.exposedLoadProgram("test")`
      runtime.executeCode(code)
      window.setTimeout(() => {
        assert.equal(runtime.getDeclickVariable('a'), 7600)
        done()
      }, 500)
    }))

  it('should load another program provided synchronously', () =>
    new Promise(done => {
      runtime.setProgramProvider(aProgram => {
        return `a = 8200`
      })
      let code = `anInstance.exposedLoadProgram("test")`
      runtime.executeCode(code)
      window.setTimeout(() => {
        assert.equal(runtime.getDeclickVariable('a'), 8200)
        done()
      }, 500)
    }))

  it('should handle synchronous error when loading another program', () =>
    new Promise(done => {
      runtime.setProgramProvider(aProgram => {
        throw new Error('test')
      })
      let code = `anInstance.exposedLoadProgram("test program")`
      runtime.setErrorHandler(error => {
        assert.equal(error.getMessage(), 'Error: test')
        done()
      })
      runtime.executeCode(code)
    }))

  it('should handle asynchronous error when loading another program', () =>
    new Promise(done => {
      runtime.setProgramProvider(aProgram => {
        return new Promise((resolve, reject) => {
          window.setTimeout(() => {
            reject(new Error('test'))
          }, 200)
        })
      })
      let code = `anInstance.exposedLoadProgram("test program")`
      runtime.setErrorHandler(error => {
        assert.equal(error.getMessage(), 'Error: test')
        done()
      })
      runtime.executeCode(code)
    }))

  it('should support interrupt', () =>
    new Promise(done => {
      let code = `while(true) {
      }
      a = 5
      `
      window.setTimeout(() => runtime.interrupt(), 100)
      runtime.executeCode(code, () => {
        assert.equal(runtime.getLastValue(), 5)
        done()
      })
    }))
})
