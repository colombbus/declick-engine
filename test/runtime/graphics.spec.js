import { describe, it, beforeAll, afterEach } from 'vitest'
import { assert } from 'chai'
import graphics from '../../src/runtime/graphics.js'
import localMap from './map.json'
import localAtlas from './atlas.json'

const localImage =
  'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgAQMAAABJtOi3AAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABVJREFUeF7NwIEAAAAAgKD9qdeocAMAoAABm3DkcAAAAABJRU5ErkJggg=='

describe('When graphics is initialized', () => {
  beforeAll(() => {
    document.body.innerHTML = `<div id='container'></div>`
  })
  afterEach(() => {
    // TODO: remove following line when Phaser has fixed removal of texture with headless config
    graphics.getController().renderer = { type: 'void' }
    graphics.reset()
  })
  it('should have a scene named main', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        const scenes = graphics.getController().scene.getScenes(false)
        assert.equal(scenes.length, 1)
        const scene = graphics.getController().scene.getScene('main')
        assert.isNotNull(scene)
      })
  })
  it('should not set scene active before start is called', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        assert.equal(graphics.getController().scene.isActive('main'), false)
      })
  })
  it('should set scene active when start is called', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        graphics.start()
        assert.equal(graphics.getController().scene.isActive('main'), true)
      })
  })
  it('should set scene inactive when stop is called', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        graphics.start()
        graphics.stop()
        assert.equal(graphics.getController().scene.isActive('main'), false)
      })
  })

  it('should not load a resource before start', () =>
    new Promise(done => {
      graphics
        .initialize(document.getElementById('container'), 'headless')
        .then(() => {
          const loader = graphics.getController().scene.getScene('main').load
          loader.imageLoadType = 'HTMLImageElement'
          let loadStart = false
          loader.once('start', () => {
            loadStart = true
          })
          graphics.addResource('image', 'anImage', `file://${__dirname}/dk.png`)
          window.setTimeout(() => {
            assert.equal(loadStart, false)
            done()
          }, 500)
        })
    }))

  it('should load a resource when started', () =>
    new Promise(done => {
      graphics
        .initialize(document.getElementById('container'), 'headless')
        .then(() => {
          const loader = graphics.getController().scene.getScene('main').load
          loader.imageLoadType = 'HTMLImageElement'
          loader.once('complete', () => {
            done()
          })
          graphics.addResource('image', 'anImage', `file://${__dirname}/dk.png`)
          graphics.addResource('map', 'aMap', `file://${__dirname}/map.json`)
          graphics.addResource(
            'atlas',
            'anAtlas',
            `file://${__dirname}/spritesheet.png`,
            `file://${__dirname}/atlas.json`,
          )
          graphics.start()
        })
    }))

  it('should load an image resource after start', () =>
    new Promise(done => {
      graphics
        .initialize(document.getElementById('container'), 'headless')
        .then(() => {
          const loader = graphics.getController().scene.getScene('main').load
          loader.imageLoadType = 'HTMLImageElement'
          loader.once('complete', () => {
            done()
          })
          return graphics.start()
        })
        .then(() => {
          graphics.addResource('image', 'anImage', `file://${__dirname}/dk.png`)
          graphics.addResource('map', 'aMap', `file://${__dirname}/map.json`)
          graphics.addResource(
            'atlas',
            'anAtlas',
            `file://${__dirname}/spritesheet.png`,
            `file://${__dirname}/atlas.json`,
          )
        })
    }))

  it('should support loading a resource twice', () =>
    new Promise(done => {
      graphics
        .initialize(document.getElementById('container'), 'headless')
        .then(() => {
          const loader = graphics.getController().scene.getScene('main').load
          loader.imageLoadType = 'HTMLImageElement'
          loader.once('complete', () => {
            done()
          })
          graphics.addResource('image', 'anImage', `file://${__dirname}/dk.png`)
          graphics.addResource('image', 'anImage', `file://${__dirname}/dk.png`)
          graphics.addResource(
            'atlas',
            'anAtlas',
            `file://${__dirname}/spritesheet.png`,
            `file://${__dirname}/atlas.json`,
          )
          graphics.addResource(
            'atlas',
            'anAtlas',
            `file://${__dirname}/spritesheet.png`,
            `file://${__dirname}/atlas.json`,
          )
          graphics.addResource('map', 'aMap', `file://${__dirname}/map.json`)
          graphics.addResource('map', 'aMap', `file://${__dirname}/map.json`)
          graphics.addLocalResource('image', 'localImage', localImage)
          graphics.addLocalResource('image', 'localImage', localImage)
          graphics.addLocalResource('map', 'localMap', localMap)
          graphics.addLocalResource('map', 'localMap', localMap)
          graphics.addLocalResource(
            'atlas',
            'localAtlas',
            localImage,
            localAtlas,
          )
          graphics.addLocalResource(
            'atlas',
            'localAtlas',
            localImage,
            localAtlas,
          )
          graphics.start()
        })
    }))

  it('should not load two different resources with same key', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        const loader = graphics.getController().scene.getScene('main').load
        loader.imageLoadType = 'HTMLImageElement'
        graphics.addResource('image', 'anImage', `file://${__dirname}/dk.png`)
        try {
          graphics.addResource(
            'image',
            'anImage',
            `file://${__dirname}/dk2.png`,
          )
          assert.fail('loaded two different resources with same key')
        } catch (e) {
          assert.equal(e.message, 'existing resource: anImage')
        }
      })
  })

  it('should be able to load a local image', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        graphics.addLocalResource('image', 'anImage', localImage)
        return graphics.start()
      })
      .then(() => {
        try {
          graphics
            .getController()
            .scene.getScene('main')
            .add.image(10, 10, 'anImage')
          assert.isOk(true)
        } catch (e) {
          assert.fail(e)
        }
      })
  })

  it('should be able to load a local atlas', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        graphics.addLocalResource('atlas', 'anAtlas', localImage, localAtlas)
        return graphics.start()
      })
      .then(() => {
        try {
          graphics
            .getController()
            .scene.getScene('main')
            .add.sprite(10, 10, 'anAtlas')
          assert.isOk(true)
        } catch (e) {
          assert.fail(e)
        }
      })
  })

  it('should be able to load a local map', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        graphics.addLocalResource('map', 'aMap', localMap)
        return graphics.start()
      })
      .then(() => {
        try {
          graphics
            .getController()
            .scene.getScene('main')
            .add.tilemap('aMap')
          assert.isOk(true)
        } catch (e) {
          assert.fail(e)
        }
      })
  })

  it('should not clear graphical resources when cleared', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        const loader = graphics.getController().scene.getScene('main').load
        loader.imageLoadType = 'HTMLImageElement'
        graphics.addResource('image', 'anImage', `file://${__dirname}/dk.png`)
        return graphics.start()
      })
      .then(() => {
        graphics.clear()
        const test = graphics
          .getController()
          .scene.getScene('main')
          .textures.exists('anImage')
        assert.isTrue(test)
      })
  })

  it('should clear image resources when reset', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        const loader = graphics.getController().scene.getScene('main').load
        loader.imageLoadType = 'HTMLImageElement'
        graphics.addResource('image', 'anImage', `file://${__dirname}/dk.png`)
        graphics.addLocalResource('image', 'aLocalImage', localImage)
        return graphics.start()
      })
      .then(() => {
        // TODO: remove following line when Phaser has fixed removal of texture with headless config
        graphics.getController().renderer = { type: 'void' }
        graphics.reset()
        const test = graphics
          .getController()
          .scene.getScene('main')
          .textures.exists('anImage')
        assert.isFalse(test)
      })
  })

  it('should clear atlas when reset', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        const loader = graphics.getController().scene.getScene('main').load
        loader.imageLoadType = 'HTMLImageElement'
        graphics.addResource(
          'atlas',
          'anAtlas',
          `file://${__dirname}/spritesheet.png`,
          `file://${__dirname}/atlas.json`,
        )
        return graphics.start()
      })
      .then(() => {
        // TODO: remove following line when Phaser has fixed removal of texture with headless config
        graphics.getController().renderer = { type: 'void' }
        return graphics.reset()
      })
      .then(() => {
        const test1 = graphics
          .getController()
          .scene.getScene('main')
          .textures.exists('anAtlas')
        const test2 = graphics
          .getController()
          .scene.getScene('main')
          .cache.json.exists('anAtlas')
        assert.isFalse(test1)
        assert.isFalse(test2)
      })
  })

  it('should clear map when reset', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        const loader = graphics.getController().scene.getScene('main').load
        loader.imageLoadType = 'HTMLImageElement'
        graphics.addResource('map', 'aMap', `file://${__dirname}/map.json`)
        return graphics.start()
      })
      .then(() => {
        // TODO: remove following line when Phaser has fixed removal of texture with headless config
        graphics.getController().renderer = { type: 'void' }
        return graphics.reset()
      })
      .then(() => {
        const test = graphics
          .getController()
          .scene.getScene('main')
          .cache.tilemap.exists('aMap')
        assert.isFalse(test)
      })
  })

  it('should not clear local graphical resources when cleared', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        graphics.addLocalResource('image', 'anImage', localImage)
        return graphics.start()
      })
      .then(() => {
        graphics.clear()
        const test = graphics
          .getController()
          .scene.getScene('main')
          .textures.exists('anImage')
        assert.isTrue(test)
      })
  })

  it('should not clear local graphical resources when reset', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        graphics.addLocalResource('image', 'anImage', localImage)
        return graphics.start()
      })
      .then(() => {
        graphics.reset()
        const test = graphics
          .getController()
          .scene.getScene('main')
          .textures.exists('anImage')
        assert.isTrue(test)
      })
  })

  it('should clear local transient graphical resources when reset', () => {
    return graphics
      .initialize(document.getElementById('container'), 'headless')
      .then(() => {
        graphics.addLocalTransientResource('image', 'anImage', localImage)
        return graphics.start()
      })
      .then(() => {
        graphics.reset()
        const test = graphics
          .getController()
          .scene.getScene('main')
          .textures.exists('anImage')
        assert.isFalse(test)
      })
  })

  it('should call delete on an object when cleared', () =>
    new Promise(done => {
      const myObject = {
        delete() {
          done()
        },
      }
      graphics
        .initialize(document.getElementById('container'), 'headless')
        .then(() => {
          graphics.addObject(myObject)
          return graphics.start()
        })
        .then(() => {
          graphics.clear()
        })
    }))
})
