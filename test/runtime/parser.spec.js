import { describe, it, beforeAll, afterAll } from 'vitest'
import { assert } from 'chai'
import Parser from '../../src/runtime/parser.js'
import { i18n } from '@lingui/core'
import frenchTranslations from '../../translations/translation.fr.json'
import englishTranslations from '../../translations/translation.en.json'

describe('when encountering a repeat statement', () => {
  let code = `répéter(3) {
    bob.avancer(5)
  }`

  beforeAll(() => {
    i18n.load('fr', frenchTranslations)
    i18n.activate('fr')
    Parser.setRepeatKeyword(i18n._('repeat'))
  })

  afterAll(function() {
    i18n.load('en', englishTranslations)
    i18n.activate('en')
  })

  it('should parse correctly a repeat statement', () => {
    let result = Parser.parse(code)
    assert.equal(result.body[0].type, 'RepeatStatement')
  })

  it('should set the count correctly', () => {
    let result = Parser.parse(code)
    assert.equal(result.body[0].count.value, 3)
  })

  it('should set the body correctly', () => {
    let result = Parser.parse(code)
    assert.equal(result.body[0].body.body[0].raw, 'bob.avancer(5)')
  })

  it('should send any error to a given error handler', () =>
    new Promise(done => {
      let falsyCode = 'a = b + '
      Parser.setErrorHandler(error => {
        assert.equal(error.getMessage(), 'Erreur de syntaxe')
        done()
      })
      Parser.parse(falsyCode)
    }))
})
