import Datauri from 'datauri'
window.focus = () => {}
const datauri = new Datauri()

window.URL.createObjectURL = function(blob) {
  if (blob) {
    return datauri.format(
      blob.type,
      blob[Object.getOwnPropertySymbols(blob)[0]]._buffer,
    ).content
  }
  return null
}

window.URL.revokeObjectURL = function(objectURL) {
  // Do nothing at the moment
}
