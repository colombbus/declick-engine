import babel from 'vite-plugin-babel'
import { lingui } from '@lingui/vite-plugin'
import { defineConfig } from 'vitest/config'
import { readFileSync } from 'fs'
//import dataUri from '@rollup/plugin-data-uri'

const mediaTypes = [
  'png',
  'jpg',
  'jpeg',
  'gif',
  'svg',
  'webp',
  'ico',
  'bmp',
  'avif',
  'webp',
]

export default defineConfig({
  test: {
    environment: 'jsdom',
    environmentOptions: {
      jsdom: {
        resources: 'usable',
      },
    },
    setupFiles: ['./test/setup.jsdom.js'],
  },
  plugins: [
    /*{ ...dataUri(), apply: 'serve', enforce: 'pre' },*/
    babel(),
    lingui(),
    [
      {
        name: 'media-to-data-url',
        enforce: 'pre',
        load(id) {
          for (const mediaType of mediaTypes) {
            if (id.endsWith(`.${mediaType}`)) {
              const src = readFileSync(id).toString('base64')
              return `export default "data:image/${mediaType};base64,${src}"`
            }
          }
        },
      },
    ],
  ],
})
