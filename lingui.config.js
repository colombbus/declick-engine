import { formatter } from '@lingui/format-json'

export default {
  format: formatter({ style: 'minimal' }),
  catalogs: [
    {
      path: '<rootDir>/translations/translation.{locale}',
      include: ['<rootDir>/src'],
      exclude: ['**/node_modules/**'],
    },
  ],
  locales: ['en', 'fr', 'nl'],
  compileNamespace: 'es',
}
