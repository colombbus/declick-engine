document.addEventListener('DOMContentLoaded', event => {
  for (const link of document.querySelectorAll("li[data-type='method']>a")) {
    link.href = link.href.replaceAll('%2525', '%25')
  }
})
