import 'reflect-metadata/no-conflict'
import { glob } from 'glob'
import fs from 'node:fs'
import path from 'node:path'
import { i18n } from '@lingui/core'
import jsdomGlobal from 'jsdom-global'
import Datauri from 'datauri'
import { parse } from 'yaml'

const BUILD_FOLDER = 'doc/build'

const localeList = await glob('translations/translation.*.json')

jsdomGlobal(undefined, {
  pretendToBeVisual: true,
  resources: 'usable',
  beforeParse(window) {
    window.focus = function() {}
  },
})

const datauri = new Datauri()

window.URL.createObjectURL = function(blob) {
  if (blob) {
    return datauri.format(
      blob.type,
      blob[Object.getOwnPropertySymbols(blob)[0]]._buffer,
    ).content
  }
  return null
}

window.URL.revokeObjectURL = function(objectURL) {
  // Do nothing at the moment
}

function getAllTranslatedMethodNames(aPrototype, moduleName) {
  let currentModule = moduleName
  const translatedMethods = new Map()
  do {
    const keys = Reflect.ownKeys(aPrototype)
    keys.forEach(key => {
      const value = Reflect.getMetadata('translated', aPrototype, key)
      if (value) {
        const translatedValue = i18n._(value)
        if (!translatedMethods.has(translatedValue)) {
          translatedMethods.set(translatedValue, null)
        }
        const documentation = translatedMethods.get(translatedValue)
        if (!documentation) {
          if (currentModule in translations) {
            const methodDocumentation = translations[currentModule][value]
            if (methodDocumentation !== value) {
              translatedMethods.set(translatedValue, methodDocumentation)
            }
          }
        }
      }
    })
    aPrototype = Reflect.getPrototypeOf(aPrototype)
    if (aPrototype != null) {
      currentModule = aPrototype.constructor.name.toLowerCase()
    }
  } while (aPrototype != null)

  return translatedMethods
}

function addComment(string) {
  let result = '/**\n'
  const lines = string.split('\n')
  lines.forEach(line => {
    result += '* ' + line + '\n'
  })
  result += '*/\n'
  return result
}

let translations = {}

try {
  const classList = await glob(`${BUILD_FOLDER}/objects/classes/*.cjs`)
  const instanceList = await glob(`${BUILD_FOLDER}/objects/instances/*.cjs`)
  let files = classList.map(aClass => ({ name: aClass, instance: false }))
  files = files.concat(
    instanceList.map(anInstance => ({ name: anInstance, instance: true })),
  )

  for (const localeFile of localeList) {
    translations = {}
    const matches = localeFile.match(/.*translation\.([^\.]*)\.json$/)
    if (matches.length > 0) {
      const locale = matches[1]
      console.log('Translating into: ' + locale)
      console.log('Using translation file: ' + localeFile)
      if (fs.existsSync(`${BUILD_FOLDER}/${locale}`)) {
        fs.rmSync(`${BUILD_FOLDER}/${locale}`, { recursive: true })
      }
      fs.mkdirSync(`${BUILD_FOLDER}/${locale}`)

      i18n.load(locale, JSON.parse(fs.readFileSync(localeFile, 'utf8')))
      i18n.activate(locale)

      const documentationList = await glob(
        `translations/documentation/${locale}/*.yaml`,
      )
      for (const fileName of documentationList) {
        console.log('importing documentation file: ' + fileName)
        const documentationContent = fs.readFileSync(fileName, 'utf8')
        const documentation = parse(documentationContent)
        const moduleName = path.basename(fileName, '.yaml').toLowerCase()
        if (!(moduleName in translations)) {
          translations[moduleName] = documentation
        }
      }
      for (const file of files) {
        console.log(`importing ../${file.name}`)
        let classContent = await import(`../${file.name}`)
        classContent = classContent.default.default
        let className = i18n._(Reflect.getMetadata('translated', classContent))

        if (file.instance) {
          className = className.charAt(0).toUpperCase() + className.slice(1)
        }
        let translatedContent = ''

        const moduleName = file.instance
          ? classContent.name
              .substring(0, classContent.name.length - 'Class'.length)
              .toLowerCase()
          : classContent.name.toLowerCase()

        if (moduleName in translations && 'class' in translations[moduleName]) {
          const classDocumentation = translations[moduleName]['class']
          translatedContent += addComment(classDocumentation)
        }

        translatedContent += `class ${className} {\n`
        const methods = getAllTranslatedMethodNames(
          classContent.prototype,
          moduleName,
        )

        for (const methodName of methods.keys()) {
          if (methods.get(methodName)) {
            translatedContent += addComment(
              methods.get(methodName) + '\n@memberof ' + className,
            )
          }
          translatedContent += `${methodName}() {}\n`
        }
        if (!file.instance) {
          if (
            moduleName in translations &&
            'constructor' in translations[moduleName]
          ) {
            const translatedConstructorDocumentation =
              translations[moduleName]['constructor']
            translatedContent += addComment(translatedConstructorDocumentation)
            translatedContent += 'constructor() {}\n'
          }
        }
        translatedContent += '}\n'
        if (file.instance) {
          translatedContent += addComment(`@type {${className}}`)
          translatedContent += `${className.toLowerCase()} = new ${className}()`
        }
        fs.writeFileSync(
          `${BUILD_FOLDER}/${locale}/${moduleName}.js`,
          translatedContent,
        )
      }
    }
  }
} catch (err) {
  console.error(err)
}
