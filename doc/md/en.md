## Classes

<dl>
<dt><a href="#Sprite">Sprite</a></dt>
<dd><p>Object displaying a sprite</p>
</dd>
<dt><a href="#Walker">Walker</a></dt>
<dd><p>A <a href="#Sprite">Sprite</a> object that may fall and jump</p>
</dd>
</dl>

## Members

<dl>
<dt><a href="#camera">camera</a> : <code>Camera</code></dt>
<dd></dd>
<dt><a href="#declick">declick</a> : <code>Declick</code></dt>
<dd></dd>
<dt><a href="#keyboard">keyboard</a> : <code>Keyboard</code></dt>
<dd></dd>
</dl>

<a name="Sprite"></a>

## Sprite
Object displaying a sprite


* [Sprite](#Sprite)
    * [new Sprite(texture)](#new_Sprite_new)
    * [.moveForward(distance)](#Sprite+moveForward)
    * [.moveBackward(distance)](#Sprite+moveBackward)
    * [.moveUpward(distance)](#Sprite+moveUpward)
    * [.moveDownward(distance)](#Sprite+moveDownward)
    * [.stop()](#Sprite+stop)
    * [.moveAlwaysForward()](#Sprite+moveAlwaysForward)
    * [.moveAlwaysBackward()](#Sprite+moveAlwaysBackward)
    * [.moveAlwaysUpward()](#Sprite+moveAlwaysUpward)
    * [.moveAlwaysDownward()](#Sprite+moveAlwaysDownward)
    * [.ifCollisionWith(object, command, optionalParameter)](#Sprite+ifCollisionWith)
    * [.addBlock(block)](#Sprite+addBlock)
    * [.ifOverlapWith(object, command, optionalParameter)](#Sprite+ifOverlapWith)
    * [.setLocation(x, y)](#Sprite+setLocation)
    * [.mayCatch(object)](#Sprite+mayCatch)
    * [.ifCatch(object, command)](#Sprite+ifCatch)
    * [.isOnItem(object)](#Sprite+isOnItem) ⇒ <code>boolean</code>
    * [.drop(object)](#Sprite+drop)
    * [.getX()](#Sprite+getX) ⇒ <code>number</code>
    * [.getY()](#Sprite+getY) ⇒ <code>number</code>
    * [.delete()](#Sprite+delete)

<a name="new_Sprite_new"></a>

### new Sprite(texture)
Create a Sprite

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>texture</td><td><code>string</code></td><td><p>the name of an optional image or spritesheet</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+moveForward"></a>

### sprite.moveForward(distance)
"Move a distance forward

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>number</code></td><td><p>La distance en pixels&quot;</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+moveBackward"></a>

### sprite.moveBackward(distance)
Move a distance backward

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>number</code></td><td><p>The distance in pixels</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+moveUpward"></a>

### sprite.moveUpward(distance)
Move a distance upward

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>number</code></td>
    </tr>  </tbody>
</table>

<a name="Sprite+moveDownward"></a>

### sprite.moveDownward(distance)
Move a distance downward

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>number</code></td>
    </tr>  </tbody>
</table>

<a name="Sprite+stop"></a>

### sprite.stop()
Stop the object

<a name="Sprite+moveAlwaysForward"></a>

### sprite.moveAlwaysForward()
Move forward

<a name="Sprite+moveAlwaysBackward"></a>

### sprite.moveAlwaysBackward()
Move backward

<a name="Sprite+moveAlwaysUpward"></a>

### sprite.moveAlwaysUpward()
Move upward

<a name="Sprite+moveAlwaysDownward"></a>

### sprite.moveAlwaysDownward()
Move downward

<a name="Sprite+ifCollisionWith"></a>

### sprite.ifCollisionWith(object, command, optionalParameter)
Execute command if collision is detected with a given object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Object</code></td><td><p>The object that may generate collision</p>
</td>
    </tr><tr>
    <td>command</td><td><code>string</code></td><td><p>The command</p>
</td>
    </tr><tr>
    <td>optionalParameter</td><td><code>*</code></td><td><p>Any optional parameter that will be sent to the command</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+addBlock"></a>

### sprite.addBlock(block)
Define an object that can block the object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>block</td><td><code>Object</code></td>
    </tr>  </tbody>
</table>

<a name="Sprite+ifOverlapWith"></a>

### sprite.ifOverlapWith(object, command, optionalParameter)
Execute command if overlap a given object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Object</code></td><td><p>The object that may be overlapped</p>
</td>
    </tr><tr>
    <td>command</td><td><code>string</code></td><td><p>The command</p>
</td>
    </tr><tr>
    <td>optionalParameter</td><td><code>*</code></td><td><p>Any optional parameter that will be sent to the command</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+setLocation"></a>

### sprite.setLocation(x, y)
Set the location of the object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>number</code></td><td><p>the x coordinate</p>
</td>
    </tr><tr>
    <td>y</td><td><code>number</code></td><td><p>the y coordinate</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+mayCatch"></a>

### sprite.mayCatch(object)
Set an item as catchable by the object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Item</code></td><td><p>the item that may be caught</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+ifCatch"></a>

### sprite.ifCatch(object, command)
Defines a command to execute when the sprite catches an item

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Item</code></td><td><p>the item that may be caught</p>
</td>
    </tr><tr>
    <td>command</td><td><code>function</code> | <code>string</code></td><td><p>the command to execute</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+isOnItem"></a>

### sprite.isOnItem(object) ⇒ <code>boolean</code>
Check if the sprite is on an item

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Item</code></td><td><p>the item</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+drop"></a>

### sprite.drop(object)
Drop an item

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Item</code></td><td><p>the item to drop</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+getX"></a>

### sprite.getX() ⇒ <code>number</code>
Retrieve the x coordinate of the object

**Returns**: <code>number</code> - The x coordinate  
<a name="Sprite+getY"></a>

### sprite.getY() ⇒ <code>number</code>
Retrieve the y coordinate of the object

**Returns**: <code>number</code> - The y coordinate  
<a name="Sprite+delete"></a>

### sprite.delete()
Delete the object.

<a name="Walker"></a>

## Walker
A [Sprite](#Sprite) object that may fall and jump


* [Walker](#Walker)
    * [new Walker(texture)](#new_Walker_new)
    * [.moveForward(distance)](#Walker+moveForward)
    * [.moveBackward(distance)](#Walker+moveBackward)
    * [.moveUpward(distance)](#Walker+moveUpward)
    * [.moveDownward(distance)](#Walker+moveDownward)
    * [.stop()](#Walker+stop)
    * [.moveAlwaysForward()](#Walker+moveAlwaysForward)
    * [.moveAlwaysBackward()](#Walker+moveAlwaysBackward)
    * [.moveAlwaysUpward()](#Walker+moveAlwaysUpward)
    * [.moveAlwaysDownward()](#Walker+moveAlwaysDownward)
    * [.ifCollisionWith(object, command, optionalParameter)](#Walker+ifCollisionWith)
    * [.addBlock(block)](#Walker+addBlock)
    * [.ifOverlapWith(object, command, optionalParameter)](#Walker+ifOverlapWith)
    * [.setLocation(x, y)](#Walker+setLocation)
    * [.mayCatch(object)](#Walker+mayCatch)
    * [.ifCatch(object, command)](#Walker+ifCatch)
    * [.isOnItem(object)](#Walker+isOnItem) ⇒ <code>boolean</code>
    * [.drop(object)](#Walker+drop)
    * [.getX()](#Walker+getX) ⇒ <code>number</code>
    * [.getY()](#Walker+getY) ⇒ <code>number</code>
    * [.delete()](#Walker+delete)

<a name="new_Walker_new"></a>

### new Walker(texture)
Create a Walker object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>texture</td><td><code>string</code></td><td><p>the name of an optional image or spritesheet</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+moveForward"></a>

### walker.moveForward(distance)
"Move a distance forward

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>number</code></td><td><p>La distance en pixels&quot;</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+moveBackward"></a>

### walker.moveBackward(distance)
Move a distance backward

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>number</code></td><td><p>The distance in pixels</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+moveUpward"></a>

### walker.moveUpward(distance)
Move a distance upward

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>number</code></td>
    </tr>  </tbody>
</table>

<a name="Walker+moveDownward"></a>

### walker.moveDownward(distance)
Move a distance downward

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>number</code></td>
    </tr>  </tbody>
</table>

<a name="Walker+stop"></a>

### walker.stop()
Stop the object

<a name="Walker+moveAlwaysForward"></a>

### walker.moveAlwaysForward()
Move forward

<a name="Walker+moveAlwaysBackward"></a>

### walker.moveAlwaysBackward()
Move backward

<a name="Walker+moveAlwaysUpward"></a>

### walker.moveAlwaysUpward()
Move upward

<a name="Walker+moveAlwaysDownward"></a>

### walker.moveAlwaysDownward()
Move downward

<a name="Walker+ifCollisionWith"></a>

### walker.ifCollisionWith(object, command, optionalParameter)
Execute command if collision is detected with a given object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Object</code></td><td><p>The object that may generate collision</p>
</td>
    </tr><tr>
    <td>command</td><td><code>string</code></td><td><p>The command</p>
</td>
    </tr><tr>
    <td>optionalParameter</td><td><code>*</code></td><td><p>Any optional parameter that will be sent to the command</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+addBlock"></a>

### walker.addBlock(block)
Define an object that can block the object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>block</td><td><code>Object</code></td>
    </tr>  </tbody>
</table>

<a name="Walker+ifOverlapWith"></a>

### walker.ifOverlapWith(object, command, optionalParameter)
Execute command if overlap a given object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Object</code></td><td><p>The object that may be overlapped</p>
</td>
    </tr><tr>
    <td>command</td><td><code>string</code></td><td><p>The command</p>
</td>
    </tr><tr>
    <td>optionalParameter</td><td><code>*</code></td><td><p>Any optional parameter that will be sent to the command</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+setLocation"></a>

### walker.setLocation(x, y)
Set the location of the object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>number</code></td><td><p>the x coordinate</p>
</td>
    </tr><tr>
    <td>y</td><td><code>number</code></td><td><p>the y coordinate</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+mayCatch"></a>

### walker.mayCatch(object)
Set an item as catchable by the object

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Item</code></td><td><p>the item that may be caught</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+ifCatch"></a>

### walker.ifCatch(object, command)
Defines a command to execute when the sprite catches an item

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Item</code></td><td><p>the item that may be caught</p>
</td>
    </tr><tr>
    <td>command</td><td><code>function</code> | <code>string</code></td><td><p>the command to execute</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+isOnItem"></a>

### walker.isOnItem(object) ⇒ <code>boolean</code>
Check if the sprite is on an item

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Item</code></td><td><p>the item</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+drop"></a>

### walker.drop(object)
Drop an item

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>object</td><td><code>Item</code></td><td><p>the item to drop</p>
</td>
    </tr>  </tbody>
</table>

<a name="Walker+getX"></a>

### walker.getX() ⇒ <code>number</code>
Retrieve the x coordinate of the object

**Returns**: <code>number</code> - The x coordinate  
<a name="Walker+getY"></a>

### walker.getY() ⇒ <code>number</code>
Retrieve the y coordinate of the object

**Returns**: <code>number</code> - The y coordinate  
<a name="Walker+delete"></a>

### walker.delete()
Delete the object.

<a name="camera"></a>

## camera : <code>Camera</code>
<a name="declick"></a>

## declick : <code>Declick</code>
<a name="keyboard"></a>

## keyboard : <code>Keyboard</code>
