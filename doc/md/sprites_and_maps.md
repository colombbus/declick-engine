# Sprites and maps

## Sprites

### Single sprite

A single sprite is stored in an image file (png, jpg, gif).

To be able to use it in a Declick script, file has to be loaded first:

```js
Declick.addImageResource('image.png', 'my sprite')
Declick.startGraphics().then(() => {
  Declick.execute(myScript)
})
```

Then in the script, any object may access it, e.g. when creating an Image or a Sprite:

```js
myImage = new Image('my sprite')
mySprite = new Sprite('my sprite')
```

### Spritesheet

Several sprites may be stored in a single file. Tools like [TexturePacker](https://www.codeandweb.com/texturepacker) or [Atlas Packer](https://gammafp.com/tool/atlas-packer/) may be used to pack sprites and generate both image and atlas files. Atlas has to be saved in JSON format.

To setup **animations**, atlas file must be edited in order to add an "anims" section. This section specifies the sprites to be used for every move. A move may be:

- up
- down
- left
- right
- face

For every move, the following properties may be specified:

- **frames**: the list of sprites to be used
- **duration**: the duration of one cycle

Example:

```json
"anims": {
    "face": {
      "frames": ["female_stand.png"],
      "duration": 500
    },
    "left": {
      "frames": ["female_walk1_g.png", "female_walk2_g.png"],
      "duration": 500
    },
    "right": {
      "frames": ["female_walk1.png", "female_walk2.png"],
      "duration": 500
    }
  }
```

Adding a spritesheet to Declick has to be done before starting graphics:

```js
Declick.addSpriteSheetResource('sprites.png', 'atlas.json', 'my spritesheet')
Declick.startGraphics().then(() => {
  Declick.execute(myScript)
})
```

Then the spritesheet resource may be used in a script, e.g. when creating a Sprite object:

```js
platform = new Sprite('my spritesheet')
```

## Maps

Maps may be generated using [Tiled](https://www.mapeditor.org/):

- Tiles are saved in a single file (as for spritesheets, see above).
- Tiles are loaded chosing this file as Tilset ("New Tileset > Source"). Since they should all be of same size, there is no need for an atlas file.
- Maps have to be saved in JSON format.

To use a map with Declick, both sprites and map files have to be loaded before starting graphics:

```js
Declick.addImageResource('tiles.png', 'my tiles')
Declick.addMapResource('map.json', 'my map')
Declick.startGraphics().then(() => {
  Declick.execute(myScript)
})
```

Then, in a Declick script, a Plaform object may be created using these resources:

```js
platform = new Platform('my map', 'my tiles')
```

### Collisions and overlaps

Every tile from the tileset may generate collisions or overlaps. Collisions are used for instance for walls or floor : sprite objects won't be able to go through them. On the contrary, tile that generate overlaps may be overlapped, but this will trigger an event.

To set a tile as generating collision or overlaps, the tileset has to be edited in Tiled.

- To set a tile as generating collisions, a custom boolean property **collides** has to be added to the tile with its value set to true.
- To set a tile as generating overlaps, a custom boolean property **overlaps** has to be added to the tile with its value set to true.

### Tile category

When editing the tileset in Tiled, a custom string property **is** may be added to a tile. Its value may be used in declick programs to detect the category of a given Tile (see PlatformTile objects) or to restrict collisions and overlaps to a given tile category (see Platform objects).
