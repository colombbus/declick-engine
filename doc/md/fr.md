## Classes

<dl>
<dt><a href="#Maçon">Maçon</a></dt>
<dd><p>Un objet <a href="#Robot">Robot</a> qui est capable de construire une <a href="#Plateforme">Plateforme</a> en posant des briques, ainsi que de poser des objets <a href="#Item">Item</a>.</p>
</dd>
<dt><a href="#Bouton">Bouton</a></dt>
<dd><p>Un objet qui affiche un bouton et peut exécuter des commandes lorsqu&#39;on clique dessus.</p>
</dd>
<dt><a href="#Declick">Declick</a></dt>
<dd><p>Méthodes de l&#39;objet <strong>declick</strong> utilisé pour accéder à l&#39;environnement Declick.</p>
</dd>
<dt><a href="#Item">Item</a></dt>
<dd><p>Un objet qui peut être ramassé par un objet <a href="#Sprite">Sprite</a>.
Un item peut soit représenter un média disponible, soit afficher un type parmi les items prédéfinis :</p>
<table>
<thead>
<tr>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody><tr>
<td align="left">clé</td>
<td align="left">une clé</td>
</tr>
<tr>
<td align="left">vie</td>
<td align="left">une potion de vie</td>
</tr>
<tr>
<td align="left">poison</td>
<td align="left">une potion de poison</td>
</tr>
<tr>
<td align="left">coffre</td>
<td align="left">un coffre</td>
</tr>
</tbody></table>
</dd>
<dt><a href="#Clavier">Clavier</a></dt>
<dd><p>Méthodes de l&#39;objet <strong>clavier</strong> utilisé pour surveiller les touches du clavier.</p>
</dd>
<dt><a href="#Liste">Liste</a></dt>
<dd><p>Une liste pouvant être composée d&#39;objets ou de valeurs (textes, nombres, etc.)</p>
</dd>
<dt><a href="#Plateforme">Plateforme</a></dt>
<dd><p>Un objet qui affiche une plateforme, dans laquelle peuvent évoluer des objets <a href="#Sprite">Sprite</a>.</p>
</dd>
<dt><a href="#Brique">Brique</a></dt>
<dd><p>Un objet représentant une brique dans un objet <a href="#Plateforme">Plateforme</a>.
Ces objets ne sont pas créés directement. Ils sont créés via la méthode <a href="#Plateforme+r%C3%A9cup%C3%A9rerBrique">récupérerBrique</a>.</p>
</dd>
<dt><a href="#Robot">Robot</a></dt>
<dd><p>Un objet <a href="#Promeneur">Promeneur</a> qui bloque l&#39;exécution de Declick pendant chacun de ses mouvements. Contrairement aux objets <a href="#Promeneur">Promeneur</a>, les distances s&#39;expriment en nombre de <strong>pas</strong>.</p>
</dd>
<dt><a href="#Sprite">Sprite</a></dt>
<dd><p>Un objet qui peut se déplacer, afficher des animations et provoquer des collisions ou des chevauchements avec d&#39;autres objets <a href="#Sprite">Sprite</a>.</p>
</dd>
<dt><a href="#Texte">Texte</a></dt>
<dd><p>Un objet qui peut afficher du texte.</p>
</dd>
<dt><a href="#Tortue">Tortue</a></dt>
<dd><p>Un objet <a href="#Robot">Robot</a> qui peut laisser une trace sur son chemin.</p>
</dd>
<dt><a href="#Promeneur">Promeneur</a></dt>
<dd><p>Un objet <a href="#Sprite">Sprite</a> qui peut tomber et sauter</p>
</dd>
</dl>

## Members

<dl>
<dt><a href="#caméra">caméra</a> : <code>Caméra</code></dt>
<dd></dd>
<dt><a href="#declick">declick</a> : <code><a href="#Declick">Declick</a></code></dt>
<dd></dd>
<dt><a href="#clavier">clavier</a> : <code><a href="#Clavier">Clavier</a></code></dt>
<dd></dd>
</dl>

<a name="Maçon"></a>

## Maçon
Un objet [Robot](#Robot) qui est capable de construire une [Plateforme](#Plateforme) en posant des briques, ainsi que de poser des objets [Item](#Item).


* [Maçon](#Maçon)
    * [new Maçon(média)](#new_Maçon_new)
    * [.poserEntrée()](#Maçon+poserEntrée)
    * [.poserSortie()](#Maçon+poserSortie)
    * [.poserMur()](#Maçon+poserMur)
    * [.poserSol()](#Maçon+poserSol)
    * [.poserLigne(briques)](#Maçon+poserLigne)
    * [.sePresser(valeur)](#Maçon+sePresser)
    * [.poserItem(type)](#Maçon+poserItem)
    * [.récupérerPlateforme()](#Maçon+récupérerPlateforme) ⇒ [<code>Plateforme</code>](#Plateforme)
    * [.avancer(pas)](#Maçon+avancer)
    * [.reculer(pas)](#Maçon+reculer)
    * [.monter(pas)](#Maçon+monter)
    * [.descendre(pas)](#Maçon+descendre)
    * [.définirPosition(x, y)](#Maçon+définirPosition)
    * [.définirLongueurPas(longueur)](#Maçon+définirLongueurPas)
    * [.récupérerLongueurPas()](#Maçon+récupérerLongueurPas) ⇒ <code>nombre</code>
    * [.récupérerX()](#Maçon+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Maçon+récupérerY) ⇒ <code>nombre</code>
    * [.peutTomber(value)](#Maçon+peutTomber)
    * [.définirGravité(gravité)](#Maçon+définirGravité)
    * [.sauter()](#Maçon+sauter)
    * [.définirAmplitudeSaut(amplitude)](#Maçon+définirAmplitudeSaut)
    * [.arrêter()](#Maçon+arrêter)
    * [.avancerToujours()](#Maçon+avancerToujours)
    * [.reculerToujours()](#Maçon+reculerToujours)
    * [.monterToujours()](#Maçon+monterToujours)
    * [.descendreToujours()](#Maçon+descendreToujours)
    * [.siCollisionAvec(objet, commande, paramètre)](#Maçon+siCollisionAvec)
    * [.ajouterBloc(objet)](#Maçon+ajouterBloc)
    * [.siRencontre(objet, commande, paramètre)](#Maçon+siRencontre)
    * [.estDéplaçable(valeur)](#Maçon+estDéplaçable)
    * [.peutAttraper(item)](#Maçon+peutAttraper)
    * [.siAttrape(item, commande)](#Maçon+siAttrape)
    * [.estSurItem(item)](#Maçon+estSurItem) ⇒ <code>booléen</code>
    * [.lâcher(item)](#Maçon+lâcher)
    * [.récupérerItemsAttrapés()](#Maçon+récupérerItemsAttrapés) ⇒ [<code>Liste</code>](#Liste)
    * [.porteItem(type)](#Maçon+porteItem) ⇒ <code>booléen</code>
    * [.attraper(objet)](#Maçon+attraper)
    * [.supprimer()](#Maçon+supprimer)

<a name="new_Maçon_new"></a>

### new Maçon(média)
Crée un objet Maçon

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>média</td><td><code>texte</code></td><td><p>(optionnel) - le nom d&#39;une image ou d&#39;un fichier sprite disponible</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Création de l'objet
max = new Maçon()
// l'objet avance de 2 pas
max.avancer(2)
// l'objet pose un mur à l'endroit où il se situe
max.poserMur()
// etc.
```
<a name="Maçon+poserEntrée"></a>

### maçon.poserEntrée()
Pose une brique de type "entrée" à l'endroit où il se situe.

<a name="Maçon+poserSortie"></a>

### maçon.poserSortie()
Pose une brique de type "sortie" à l'endroit où il se situe.

<a name="Maçon+poserMur"></a>

### maçon.poserMur()
Pose une brique de type "mur" à l'endroit où il se situe.

<a name="Maçon+poserSol"></a>

### maçon.poserSol()
Pose une brique de type "sol" à l'endroit où il se situe.

<a name="Maçon+poserLigne"></a>

### maçon.poserLigne(briques)
Pose une rangée de briques à partir de l'endroit où il se situe.
Les types de briques sont définis pas des nombres :
| Nombre  | Type      |
| :------ | :-----    |
| 1       | mur       |
| 2       | sol       |
| 3       | entrée    |
| 4       | sortie    |

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>briques</td><td><code>liste</code></td><td><p>une liste de nombres définissant les briques à poser</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// pose un mur, puis 3 sols et encore un mur
max.poserLigne([1, 2, 2, 2, 1])
```
<a name="Maçon+sePresser"></a>

### maçon.sePresser(valeur)
Active ou non le mode rapide du Maçon. En mode rapide, le Maçon se déplace et pose les briques de manière accélérée.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>valeur</td><td><code>booléen</code></td><td><p><strong>true</strong> pour activer le mode, <strong>false</strong> pour le désactiver</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// active le mode rapide
max.sePresser(true)
```
<a name="Maçon+poserItem"></a>

### maçon.poserItem(type)
Pose un objet [Item](#Item) à l'endroit où il se situe.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>texte</code></td><td><p>le nom d&#39;un média disponible ou un type d&#39;item par défaut (voir <a href="#Item">Item</a>)</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// pose un item de type "clé"
max.poserItem("clé")
```
<a name="Maçon+récupérerPlateforme"></a>

### maçon.récupérerPlateforme() ⇒ [<code>Plateforme</code>](#Plateforme)
Récupère l'objet [Plateforme](#Plateforme) créé par le Maçon.

**Returns**: [<code>Plateforme</code>](#Plateforme) - l'objet Plateforme contenant les briques posées par le Maçon.  
<a name="Maçon+avancer"></a>

### maçon.avancer(pas)
Avance d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Avancer de 2 pas
bob.avancer(2)
```
<a name="Maçon+reculer"></a>

### maçon.reculer(pas)
Recule d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Reculer de 2 pas
bob.reculer(2)
```
<a name="Maçon+monter"></a>

### maçon.monter(pas)
Monte d'une certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Monter de 2 pas
bob.monter(2)
```
<a name="Maçon+descendre"></a>

### maçon.descendre(pas)
Descend d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Descendre de 2 pas
bob.descendre(2)
```
<a name="Maçon+définirPosition"></a>

### maçon.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en nombre de pas)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en nombre de pas)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+définirLongueurPas"></a>

### maçon.définirLongueurPas(longueur)
Définit la longueur d'un pas, en pixel.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>longueur</td><td><code>nombre</code></td><td><p>la longueur d&#39;un pas en pixels (valeur par défaut : 50)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+récupérerLongueurPas"></a>

### maçon.récupérerLongueurPas() ⇒ <code>nombre</code>
Récupère la longueur d'un pas, exprimée en pixel.

**Returns**: <code>nombre</code> - longueur - la longueur des pas de l'objet  
<a name="Maçon+récupérerX"></a>

### maçon.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en nombre de pas)  
<a name="Maçon+récupérerY"></a>

### maçon.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en nombre de pas)  
<a name="Maçon+peutTomber"></a>

### maçon.peutTomber(value)
Définit si l'objet Promeneur peut tomber ou non.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>value</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut tomber, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+définirGravité"></a>

### maçon.définirGravité(gravité)
Définit la valeur de la gravité. Plus la valeur est élevée, plus l'objet tombera vite. La valeur par défaut est de 500. Une valeur négative peut aussi être définie, dans ce cas l'objet s'envolera.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>gravité</td><td><code>nombre</code></td><td><p>un nombre entier, positif ou négatif (valeur par défaut : 500)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+sauter"></a>

### maçon.sauter()
Fait sauter l'objet Promeneur. Pour pouvoir sauter, l'objet doit être situé sur un objet qui le bloque : objet [Sprite](#Sprite) ou [Plateforme](#Plateforme).

<a name="Maçon+définirAmplitudeSaut"></a>

### maçon.définirAmplitudeSaut(amplitude)
Définit l'amplitude du saut. Par défaut, cette amplitude est de 400. Plus la valeur est élevée, plus le Promeneur sautera haut.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>amplitude</td><td><code>nombre</code></td><td><p>un nombre entier positif (valeur par défaut : 400)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+arrêter"></a>

### maçon.arrêter()
Arrête le déplacement de l'objet.

<a name="Maçon+avancerToujours"></a>

### maçon.avancerToujours()
Avance indéfiniment.

<a name="Maçon+reculerToujours"></a>

### maçon.reculerToujours()
Recule indéfiniment.

<a name="Maçon+monterToujours"></a>

### maçon.monterToujours()
Monte indéfiniment.

<a name="Maçon+descendreToujours"></a>

### maçon.descendreToujours()
Descend indefiniment.

<a name="Maçon+siCollisionAvec"></a>

### maçon.siCollisionAvec(objet, commande, paramètre)
Exécute une commande si l'objet réalise une collision avec l'objet Sprite donné en paramètre.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut générer une collision</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de collision avec une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siCollisionAvec(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siCollisionAvec(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siCollisionAvec(bill, descendre, 50)
```
<a name="Maçon+ajouterBloc"></a>

### maçon.ajouterBloc(objet)
Définit un autre objet Sprite en tant que bloc pour cet objet. L'objet désigné ne laissera pas passer cet objet. Cette méthode est équivalente à la méthode [siCollisionAvec](#Sprite+siCollisionAvec), sauf qu'elle ne définit pas de commande à exécuter.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite défini comme bloc</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
bob.ajouterBloc(rocher)
```
<a name="Maçon+siRencontre"></a>

### maçon.siRencontre(objet, commande, paramètre)
Exécute une commande si l'objet chevauche l'objet Sprite donné en paramètre. À la différence d'une collision, un chevauchement ne perturbe pas le déplacement des objets : ils se superposent quand ils se rencontrent.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut être chevauché</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de chevauchement d&#39;une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siRencontre(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siRencontre(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siRencontre(bill, descendre, 50)
```
<a name="Maçon+estDéplaçable"></a>

### maçon.estDéplaçable(valeur)
Par défaut, un objet est **déplaçable**, c'est à dire qu'il bouge lorsqu'un autre objet entre en collision avec lui. Ce comportement peut être modifié avec cette méthode.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>valeur</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut bouger, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+peutAttraper"></a>

### maçon.peutAttraper(item)
Définit un [Item](#Item) comme pouvant être attrapé par cet objet. Dans ce cas, si l'objet Sprite passe sur l'item, il se déplacera avec lui.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
clé = new Item()
clé.définirPosition(100,0)
bob.peutAttraper(clé)
bob.avancer(200)
```
<a name="Maçon+siAttrape"></a>

### maçon.siAttrape(item, commande)
Exécute une commande si l'objet attrape cet [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>la commande à exécuter</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siAttraper(clé, "declick.écrire('je l\'ai')")
// Ajout d'une commande par fonction
function effrayer() {
  méchant.avancerToujours()
}
bob.siAttraper(arme, effrayer)
```
<a name="Maçon+estSurItem"></a>

### maçon.estSurItem(item) ⇒ <code>booléen</code>
Vérifie si l'objet est sur un [Item](#Item).

**Returns**: <code>booléen</code> - **true** si l'objet est sur l'Item, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à rechercher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+lâcher"></a>

### maçon.lâcher(item)
Relâche un [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à relâcher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+récupérerItemsAttrapés"></a>

### maçon.récupérerItemsAttrapés() ⇒ [<code>Liste</code>](#Liste)
Récupère un objet [Liste](#Liste) avec l'ensemble des objets [Item](#Item) attrapés

**Returns**: [<code>Liste</code>](#Liste) - la liste des objets [Item](#Item)  
<a name="Maçon+porteItem"></a>

### maçon.porteItem(type) ⇒ <code>booléen</code>
Vérifie si l'objet a attrapé un objet [Item](#Item) du type spécifié.

**Returns**: <code>booléen</code> - **true** si l'objet a attrapé un objet de ce type, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>texte</code></td><td><p>le type d&#39;objet <a href="#Item">Item</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+attraper"></a>

### maçon.attraper(objet)
Attrape l'objet [Item](#Item) passé en paramètre, s'il est à la portée de l'objet Sprite (les deux objets doivent se chevaucher).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à attraper</p>
</td>
    </tr>  </tbody>
</table>

<a name="Maçon+supprimer"></a>

### maçon.supprimer()
Supprime l'objet.

<a name="Bouton"></a>

## Bouton
Un objet qui affiche un bouton et peut exécuter des commandes lorsqu'on clique dessus.


* [Bouton](#Bouton)
    * [new Bouton(texte, x, y)](#new_Bouton_new)
    * [.définirTexte(texte)](#Bouton+définirTexte)
    * [.définirTailleTexte(taille)](#Bouton+définirTailleTexte)
    * [.définirCouleur(rouge, vert, bleur)](#Bouton+définirCouleur)
    * [.définirCouleurTexte(rouge, vert, bleur)](#Bouton+définirCouleurTexte)
    * [.ajouterCommande(commande)](#Bouton+ajouterCommande)
    * [.enleverCommandes()](#Bouton+enleverCommandes)
    * [.définirPosition(x, y)](#Bouton+définirPosition)
    * [.récupérerX()](#Bouton+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Bouton+récupérerY) ⇒ <code>nombre</code>
    * [.supprimer()](#Bouton+supprimer)

<a name="new_Bouton_new"></a>

### new Bouton(texte, x, y)
Crée un objet Bouton.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>texte</td><td><code>texte</code></td><td><p>(optionnel) - le texte à afficher (&quot;Bouton&quot; par défaut)</p>
</td>
    </tr><tr>
    <td>x</td><td><code>nombre</code></td><td><p>(optionnel) - la coordonnée horizontale du bouton (0 par défaut)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>(optionnel) - la coordonnée verticale du bouton (0 par défaut)</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Création d'un objet Bouton dans le coin en haut à gauche de l'écran
b = new Bouton()
// Création d'un objet Bouton avec le texte "avancer" et situé aux coordonnées (50,100)
b = new Bouton("avancer", 50, 100)
```
<a name="Bouton+définirTexte"></a>

### bouton.définirTexte(texte)
Définit le texte affiché sur le bouton.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>texte</td><td><code>texte</code></td><td><p>le texte à afficher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Bouton+définirTailleTexte"></a>

### bouton.définirTailleTexte(taille)
Définit la taille du texte affiché sur le bouton.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>taille</td><td><code>nombre</code></td><td><p>la taille du texte (16 pixels par défaut)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Bouton+définirCouleur"></a>

### bouton.définirCouleur(rouge, vert, bleur)
Définit la couleur du bouton. La couleur est exprimée comme un mélange de rouge, vert et bleu.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>rouge</td><td><code>nombre</code></td><td><p>la quantité de rouge (entre 0 et 255)</p>
</td>
    </tr><tr>
    <td>vert</td><td><code>nombre</code></td><td><p>la quantité de vert (entre 0 et 255)</p>
</td>
    </tr><tr>
    <td>bleur</td><td><code>nombre</code></td><td><p>la quantité de bleu (entre 0 et 255)</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Bouton rouge
b.définirCouleur(255,0,0)
// Bouton violet
b.définirCouleur(255,0,255)
```
<a name="Bouton+définirCouleurTexte"></a>

### bouton.définirCouleurTexte(rouge, vert, bleur)
Définit la couleur du texte. La couleur est exprimée comme un mélange de rouge, vert et bleu.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>rouge</td><td><code>nombre</code></td><td><p>la quantité de rouge (entre 0 et 255)</p>
</td>
    </tr><tr>
    <td>vert</td><td><code>nombre</code></td><td><p>la quantité de vert (entre 0 et 255)</p>
</td>
    </tr><tr>
    <td>bleur</td><td><code>nombre</code></td><td><p>la quantité de bleu (entre 0 et 255)</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Bouton avec un texte rouge
b.définirCouleurTexte(255,0,0)
// Bouton avec un texte blanc
b.définirCouleurTexte(255,255,255)
```
<a name="Bouton+ajouterCommande"></a>

### bouton.ajouterCommande(commande)
Ajoute une commande à exécuter lorsque le bouton est cliqué.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>la commande à exécuter</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// ajout d'une commande sous forme de texte
b.ajouterCommande("bob.avancer(10)")
// ajout d'une commande sous forme de fonction
function avancer() {
  bob.avancer(10)
}
b.ajouterCommande(avancer)
```
<a name="Bouton+enleverCommandes"></a>

### bouton.enleverCommandes()
Supprime toutes les commandes de la liste des commandes à exécuter.

<a name="Bouton+définirPosition"></a>

### bouton.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en pixels)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en pixels)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Bouton+récupérerX"></a>

### bouton.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en pixels)  
<a name="Bouton+récupérerY"></a>

### bouton.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en pixels)  
<a name="Bouton+supprimer"></a>

### bouton.supprimer()
Supprime l'objet.

<a name="Declick"></a>

## Declick
Méthodes de l'objet **declick** utilisé pour accéder à l'environnement Declick.


* [Declick](#Declick)
    * [.écrire(message)](#Declick+écrire)
    * [.initialiser()](#Declick+initialiser)
    * [.suspendre()](#Declick+suspendre)
    * [.reprendre()](#Declick+reprendre)
    * [.arrêter()](#Declick+arrêter)
    * [.attendre(délai)](#Declick+attendre)
    * [.exécuter(nom)](#Declick+exécuter)

<a name="Declick+écrire"></a>

### declick.écrire(message)
Ecrit un message dans la console du navigateur.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>message</td><td><code>texte</code></td><td><p>le message à écrire</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
declick.écrire("ce message apparaît dans la console")
```
<a name="Declick+initialiser"></a>

### declick.initialiser()
Réinitialise l'environnement (supprime les objets créés).

<a name="Declick+suspendre"></a>

### declick.suspendre()
Suspend l'exécution du programme en cours.

<a name="Declick+reprendre"></a>

### declick.reprendre()
Reprend l'exécution du programme en cours.

<a name="Declick+arrêter"></a>

### declick.arrêter()
Arrête l'exécution du programme en cours.

<a name="Declick+attendre"></a>

### declick.attendre(délai)
Suspend l'exécution du programme en cours pendant un certain temps.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>délai</td><td><code>nombre</code></td><td><p>le délai avant de reprendre l&#39;exécution (en milisecondes)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Declick+exécuter"></a>

### declick.exécuter(nom)
Charge et exécute un programme.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>nom</td><td><code>texte</code></td><td><p>le nom du programme</p>
</td>
    </tr>  </tbody>
</table>

<a name="Item"></a>

## Item
Un objet qui peut être ramassé par un objet [Sprite](#Sprite).
Un item peut soit représenter un média disponible, soit afficher un type parmi les items prédéfinis :
| Type    | Description           |
| :------ | :-----                |
| clé     | une clé               |
| vie     | une potion de vie     |
| poison  | une potion de poison  |
| coffre  | un coffre             |


* [Item](#Item)
    * [new Item(image)](#new_Item_new)
    * [.définirNom(nom)](#Item+définirNom)
    * [.récupérerNom()](#Item+récupérerNom) ⇒ <code>texte</code>
    * [.définirPosition(x, y)](#Item+définirPosition)
    * [.récupérerX()](#Item+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Item+récupérerY) ⇒ <code>nombre</code>
    * [.supprimer()](#Item+supprimer)

<a name="new_Item_new"></a>

### new Item(image)
Crée un objet Item.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>image</td><td><code>texte</code></td><td><p>(optionnel) - le nom d&#39;un fichier image disponible</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Création d'un objet Item avec la représentation par défaut
clé = new Item()
// Création d'un objet Item à partir d'un média disponible
clé = new Item("image")
```
<a name="Item+définirNom"></a>

### item.définirNom(nom)
Définit le nom de l'objet. Ce nom peut ensuite servir à vérifier sur quel item un objet [Sprite](#Sprite) arrive.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>nom</td><td><code>texte</code></td><td><p>Un nom</p>
</td>
    </tr>  </tbody>
</table>

<a name="Item+récupérerNom"></a>

### item.récupérerNom() ⇒ <code>texte</code>
Récupère le nom de l'objet.

**Returns**: <code>texte</code> - nom - Le nom de l'objet, tel que défini par la méthode [définirNom](#Item+définirNom)  
<a name="Item+définirPosition"></a>

### item.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en pixels)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en pixels)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Item+récupérerX"></a>

### item.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en pixels)  
<a name="Item+récupérerY"></a>

### item.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en pixels)  
<a name="Item+supprimer"></a>

### item.supprimer()
Supprime l'objet.

<a name="Clavier"></a>

## Clavier
Méthodes de l'objet **clavier** utilisé pour surveiller les touches du clavier.

<a name="Clavier+nouvelAppui"></a>

### clavier.nouvelAppui(touche) ⇒ <code>booléen</code>
Teste si une touche vient d'être actionnée ou pas.

**Returns**: <code>booléen</code> - résultat du test : **true** si la touche vient d'être actionnée, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>touche</td><td><code>texte</code></td><td><p>La touche testée</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
répéter() {
  if (clavier.nouvelAppui("a")) {
    declick.écrire("Appui sur la touche a")
  }
}
```
<a name="Liste"></a>

## Liste
Une liste pouvant être composée d'objets ou de valeurs (textes, nombres, etc.)


* [Liste](#Liste)
    * [new Liste(valeurs)](#new_Liste_new)
    * [.ajouter(élément)](#Liste+ajouter)
    * [.retirer(élément)](#Liste+retirer)
    * [.revenirAuDébut()](#Liste+revenirAuDébut)
    * [.récupérerSuivant()](#Liste+récupérerSuivant) ⇒
    * [.estNonVide()](#Liste+estNonVide) ⇒ <code>booléen</code>
    * [.récupérer(position)](#Liste+récupérer) ⇒
    * [.modifier(position, élément)](#Liste+modifier)
    * [.contient(élément)](#Liste+contient) ⇒ <code>booléen</code>
    * [.récupérerTaille()](#Liste+récupérerTaille) ⇒ <code>nombre</code>
    * [.vider()](#Liste+vider)
    * [.supprimer()](#Liste+supprimer)

<a name="new_Liste_new"></a>

### new Liste(valeurs)
Crée une Liste.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>valeurs</td><td><code>liste</code></td><td><p>(optionnel) - les valeurs stockées dans la liste</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Crée une liste vide
vide = new Liste()
// Crée une liste de textes
textes = new Liste(["bonjour", "au revoir"])
// Crée une liste d'objets [Item](#Item)
vie = new Item("vie")
poison = new Item("poison")
items = new Liste([vie, poison])
```
<a name="Liste+ajouter"></a>

### liste.ajouter(élément)
Ajoute un élément à la liste

<table>
  <thead>
    <tr>
      <th>Param</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>élément</td><td><p>l&#39;élément à ajouter à la liste</p>
</td>
    </tr>  </tbody>
</table>

<a name="Liste+retirer"></a>

### liste.retirer(élément)
Retire un élément de la liste. Si la liste contient plusieurs fois l'élément, seul le premier sera supprimé.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>élément</td><td><p>l&#39;élément devant être retiré de la liste</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Retire un texte
textes = new Liste(["bonjour", "au revoir"])
textes.retirer("bonjour")
// Retire un objet
vie = new Item("vie")
poison = new Item("poison")
items = new Liste([vie, poison])
items.retirer(vie)
```
<a name="Liste+revenirAuDébut"></a>

### liste.revenirAuDébut()
Revient au premier élément de la liste. Permet d'agir sur la méthode récupérerSuivant : le prochain appel de cette méthode retournera le premier élément.

<a name="Liste+récupérerSuivant"></a>

### liste.récupérerSuivant() ⇒
Récupère le prochain élément de la liste. Au premier appel, le premier élément est retourné. À chaque appel, on passe à l'élément d'après.

**Returns**: élément - le prochain élément de la liste.  
**Example**  
```js
// Récupérer 2 éléments
textes = new Liste(["bonjour", "au revoir"])
premier = textes.récupérerSuivant()
deuxième = textes.récupérerSuivant()
```
<a name="Liste+estNonVide"></a>

### liste.estNonVide() ⇒ <code>booléen</code>
Vérifie si la liste contient des éléments ou non.

**Returns**: <code>booléen</code> - **true** si la liste contient des éléments, **false** sinon  
<a name="Liste+récupérer"></a>

### liste.récupérer(position) ⇒
Récupère l'élément depuis sa position dans la liste. Le premier élément est à la position 0.

**Returns**: élément - l'élément demandé  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>position</td><td><code>nombre</code></td><td><p>la position de l&#39;élément dans la liste</p>
</td>
    </tr>  </tbody>
</table>

<a name="Liste+modifier"></a>

### liste.modifier(position, élément)
Définit un élément à la position donnée.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>position</td><td><code>nombre</code></td><td><p>la position de l&#39;élément dans la liste</p>
</td>
    </tr><tr>
    <td>élément</td><td></td><td><p>l&#39;élément à stocker dans la liste</p>
</td>
    </tr>  </tbody>
</table>

<a name="Liste+contient"></a>

### liste.contient(élément) ⇒ <code>booléen</code>
Vérifie si la liste possède un élément ou non.

**Returns**: <code>booléen</code> - **true** si la liste contient l'élément, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>élément</td><td><p>l&#39;élément recherché</p>
</td>
    </tr>  </tbody>
</table>

<a name="Liste+récupérerTaille"></a>

### liste.récupérerTaille() ⇒ <code>nombre</code>
Récupère le nombre d'éléments contenus dans la liste.

**Returns**: <code>nombre</code> - taille - le nombre d'éléments de la liste  
<a name="Liste+vider"></a>

### liste.vider()
Vide la liste.

<a name="Liste+supprimer"></a>

### liste.supprimer()
Supprime l'objet.

<a name="Plateforme"></a>

## Plateforme
Un objet qui affiche une plateforme, dans laquelle peuvent évoluer des objets [Sprite](#Sprite).


* [Plateforme](#Plateforme)
    * [new Plateforme(carte, briques)](#new_Plateforme_new)
    * [.retirerBrique(brique)](#Plateforme+retirerBrique)
    * [.sélectionnerCouche(calque)](#Plateforme+sélectionnerCouche)
    * [.poserBrique(catégorie, x, y)](#Plateforme+poserBrique)
    * [.récupérerBrique(x, y)](#Plateforme+récupérerBrique) ⇒ [<code>Brique</code>](#Brique)
    * [.définirPosition(x, y)](#Plateforme+définirPosition)
    * [.récupérerX()](#Plateforme+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Plateforme+récupérerY) ⇒ <code>nombre</code>
    * [.supprimer()](#Plateforme+supprimer)

<a name="new_Plateforme_new"></a>

### new Plateforme(carte, briques)
Crée un objet Plateforme.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>carte</td><td><code>texte</code></td><td><p>(optionnel) - le nom d&#39;un fichier carte disponible</p>
</td>
    </tr><tr>
    <td>briques</td><td><code>texte</code></td><td><p>(optionnel) - le nom d&#39;un fichier images contenant les différentes briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Création d'un objet Plateforme avec la représentation par défaut
décor = new Plateforme()
// Création d'un objet Plateforme avec une carte et des briques personnalisées
décor = new Plateforme("carte", "briques")
```
<a name="Plateforme+retirerBrique"></a>

### plateforme.retirerBrique(brique)
Supprime un objet [Brique](#Brique).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>brique</td><td><code><a href="#Brique">Brique</a></code></td><td><p>l&#39;objet {Brique} à supprimer</p>
</td>
    </tr>  </tbody>
</table>

<a name="Plateforme+sélectionnerCouche"></a>

### plateforme.sélectionnerCouche(calque)
Sélectionne un calque dans la carte. Ce calque doit être présent dans le fichier carte utilisé.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>calque</td><td><code>texte</code></td><td><p>le nom du calque à utiliser.</p>
</td>
    </tr>  </tbody>
</table>

<a name="Plateforme+poserBrique"></a>

### plateforme.poserBrique(catégorie, x, y)
Crée une brique à la position donnée.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>catégorie</td><td><code>texte</code></td><td><p>la catégorie de brique. Cette catégorie doit être définie dans le fichier carte utilisé.</p>
</td>
    </tr><tr>
    <td>x</td><td><code>nombre</code></td><td><p>la position X de la brique, exprimée en nombre de briques (0 est la brique la plus à gauche)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la position Y de la brique, exprimée en nombre de briques (0 est la brique la plus en haut)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Plateforme+récupérerBrique"></a>

### plateforme.récupérerBrique(x, y) ⇒ [<code>Brique</code>](#Brique)
Récupère un objet [Brique](#Brique) correspondant à une position dans la plateforme.

**Returns**: [<code>Brique</code>](#Brique) - un objet [Brique](#Brique)  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la position X de la brique, exprimée en nombre de briques (0 est la brique la plus à gauche)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la position Y de la brique, exprimée en nombre de briques (0 est la brique la plus en haut)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Plateforme+définirPosition"></a>

### plateforme.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en pixels)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en pixels)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Plateforme+récupérerX"></a>

### plateforme.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en pixels)  
<a name="Plateforme+récupérerY"></a>

### plateforme.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en pixels)  
<a name="Plateforme+supprimer"></a>

### plateforme.supprimer()
Supprime l'objet.

<a name="Brique"></a>

## Brique
Un objet représentant une brique dans un objet [Plateforme](#Plateforme).
Ces objets ne sont pas créés directement. Ils sont créés via la méthode [récupérerBrique](#Plateforme+récupérerBrique).


* [Brique](#Brique)
    * [.récupérerX()](#Brique+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Brique+récupérerY) ⇒ <code>nombre</code>
    * [.est(catégorie)](#Brique+est) ⇒ <code>booléen</code>
    * [.supprimer()](#Brique+supprimer)

<a name="Brique+récupérerX"></a>

### brique.récupérerX() ⇒ <code>nombre</code>
Récupère la position horizontale de cette brique dans l'objet [Plateforme](#Plateforme) (0 correspond à la position à la plus à gauche).

**Returns**: <code>nombre</code> - x - la position horizontale de la brique  
<a name="Brique+récupérerY"></a>

### brique.récupérerY() ⇒ <code>nombre</code>
Récupère la position verticals de cette brique dans l'objet [Plateforme](#Plateforme) (0 correspond à la position à la plus en haut).

**Returns**: <code>nombre</code> - y - la position verticale de la brique  
<a name="Brique+est"></a>

### brique.est(catégorie) ⇒ <code>booléen</code>
Vérifie si la brique de la catégorie spécifiée.

**Returns**: <code>booléen</code> - **true** si la brique est de la catégorie, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>catégorie</td><td><code>texte</code></td><td><p>la catégorie à tester</p>
</td>
    </tr>  </tbody>
</table>

<a name="Brique+supprimer"></a>

### brique.supprimer()
Supprime l'objet.

<a name="Robot"></a>

## Robot
Un objet [Promeneur](#Promeneur) qui bloque l'exécution de Declick pendant chacun de ses mouvements. Contrairement aux objets [Promeneur](#Promeneur), les distances s'expriment en nombre de **pas**.


* [Robot](#Robot)
    * [new Robot(média)](#new_Robot_new)
    * [.avancer(pas)](#Robot+avancer)
    * [.reculer(pas)](#Robot+reculer)
    * [.monter(pas)](#Robot+monter)
    * [.descendre(pas)](#Robot+descendre)
    * [.définirPosition(x, y)](#Robot+définirPosition)
    * [.définirLongueurPas(longueur)](#Robot+définirLongueurPas)
    * [.récupérerLongueurPas()](#Robot+récupérerLongueurPas) ⇒ <code>nombre</code>
    * [.récupérerX()](#Robot+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Robot+récupérerY) ⇒ <code>nombre</code>
    * [.peutTomber(value)](#Robot+peutTomber)
    * [.définirGravité(gravité)](#Robot+définirGravité)
    * [.sauter()](#Robot+sauter)
    * [.définirAmplitudeSaut(amplitude)](#Robot+définirAmplitudeSaut)
    * [.arrêter()](#Robot+arrêter)
    * [.avancerToujours()](#Robot+avancerToujours)
    * [.reculerToujours()](#Robot+reculerToujours)
    * [.monterToujours()](#Robot+monterToujours)
    * [.descendreToujours()](#Robot+descendreToujours)
    * [.siCollisionAvec(objet, commande, paramètre)](#Robot+siCollisionAvec)
    * [.ajouterBloc(objet)](#Robot+ajouterBloc)
    * [.siRencontre(objet, commande, paramètre)](#Robot+siRencontre)
    * [.estDéplaçable(valeur)](#Robot+estDéplaçable)
    * [.peutAttraper(item)](#Robot+peutAttraper)
    * [.siAttrape(item, commande)](#Robot+siAttrape)
    * [.estSurItem(item)](#Robot+estSurItem) ⇒ <code>booléen</code>
    * [.lâcher(item)](#Robot+lâcher)
    * [.récupérerItemsAttrapés()](#Robot+récupérerItemsAttrapés) ⇒ [<code>Liste</code>](#Liste)
    * [.porteItem(type)](#Robot+porteItem) ⇒ <code>booléen</code>
    * [.attraper(objet)](#Robot+attraper)
    * [.supprimer()](#Robot+supprimer)

<a name="new_Robot_new"></a>

### new Robot(média)
Crée un objet Robot

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>média</td><td><code>texte</code></td><td><p>(optionnel) - le nom d&#39;une image ou d&#39;un fichier sprite disponible</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Création de l'objet
bob = new Robot()
// l'objet avance de 4 pas, bloquant l'exécution du programme pendant le mouvement
bob.avancer(4)
// l'objet descend ensuite de 2 pas, bloquant à nouveau l'exécution du programme
bob.descendre(2)
// etc.
```
<a name="Robot+avancer"></a>

### robot.avancer(pas)
Avance d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Avancer de 2 pas
bob.avancer(2)
```
<a name="Robot+reculer"></a>

### robot.reculer(pas)
Recule d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Reculer de 2 pas
bob.reculer(2)
```
<a name="Robot+monter"></a>

### robot.monter(pas)
Monte d'une certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Monter de 2 pas
bob.monter(2)
```
<a name="Robot+descendre"></a>

### robot.descendre(pas)
Descend d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Descendre de 2 pas
bob.descendre(2)
```
<a name="Robot+définirPosition"></a>

### robot.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en nombre de pas)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en nombre de pas)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+définirLongueurPas"></a>

### robot.définirLongueurPas(longueur)
Définit la longueur d'un pas, en pixel.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>longueur</td><td><code>nombre</code></td><td><p>la longueur d&#39;un pas en pixels (valeur par défaut : 50)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+récupérerLongueurPas"></a>

### robot.récupérerLongueurPas() ⇒ <code>nombre</code>
Récupère la longueur d'un pas, exprimée en pixel.

**Returns**: <code>nombre</code> - longueur - la longueur des pas de l'objet  
<a name="Robot+récupérerX"></a>

### robot.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en nombre de pas)  
<a name="Robot+récupérerY"></a>

### robot.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en nombre de pas)  
<a name="Robot+peutTomber"></a>

### robot.peutTomber(value)
Définit si l'objet Promeneur peut tomber ou non.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>value</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut tomber, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+définirGravité"></a>

### robot.définirGravité(gravité)
Définit la valeur de la gravité. Plus la valeur est élevée, plus l'objet tombera vite. La valeur par défaut est de 500. Une valeur négative peut aussi être définie, dans ce cas l'objet s'envolera.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>gravité</td><td><code>nombre</code></td><td><p>un nombre entier, positif ou négatif (valeur par défaut : 500)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+sauter"></a>

### robot.sauter()
Fait sauter l'objet Promeneur. Pour pouvoir sauter, l'objet doit être situé sur un objet qui le bloque : objet [Sprite](#Sprite) ou [Plateforme](#Plateforme).

<a name="Robot+définirAmplitudeSaut"></a>

### robot.définirAmplitudeSaut(amplitude)
Définit l'amplitude du saut. Par défaut, cette amplitude est de 400. Plus la valeur est élevée, plus le Promeneur sautera haut.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>amplitude</td><td><code>nombre</code></td><td><p>un nombre entier positif (valeur par défaut : 400)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+arrêter"></a>

### robot.arrêter()
Arrête le déplacement de l'objet.

<a name="Robot+avancerToujours"></a>

### robot.avancerToujours()
Avance indéfiniment.

<a name="Robot+reculerToujours"></a>

### robot.reculerToujours()
Recule indéfiniment.

<a name="Robot+monterToujours"></a>

### robot.monterToujours()
Monte indéfiniment.

<a name="Robot+descendreToujours"></a>

### robot.descendreToujours()
Descend indefiniment.

<a name="Robot+siCollisionAvec"></a>

### robot.siCollisionAvec(objet, commande, paramètre)
Exécute une commande si l'objet réalise une collision avec l'objet Sprite donné en paramètre.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut générer une collision</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de collision avec une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siCollisionAvec(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siCollisionAvec(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siCollisionAvec(bill, descendre, 50)
```
<a name="Robot+ajouterBloc"></a>

### robot.ajouterBloc(objet)
Définit un autre objet Sprite en tant que bloc pour cet objet. L'objet désigné ne laissera pas passer cet objet. Cette méthode est équivalente à la méthode [siCollisionAvec](#Sprite+siCollisionAvec), sauf qu'elle ne définit pas de commande à exécuter.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite défini comme bloc</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
bob.ajouterBloc(rocher)
```
<a name="Robot+siRencontre"></a>

### robot.siRencontre(objet, commande, paramètre)
Exécute une commande si l'objet chevauche l'objet Sprite donné en paramètre. À la différence d'une collision, un chevauchement ne perturbe pas le déplacement des objets : ils se superposent quand ils se rencontrent.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut être chevauché</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de chevauchement d&#39;une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siRencontre(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siRencontre(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siRencontre(bill, descendre, 50)
```
<a name="Robot+estDéplaçable"></a>

### robot.estDéplaçable(valeur)
Par défaut, un objet est **déplaçable**, c'est à dire qu'il bouge lorsqu'un autre objet entre en collision avec lui. Ce comportement peut être modifié avec cette méthode.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>valeur</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut bouger, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+peutAttraper"></a>

### robot.peutAttraper(item)
Définit un [Item](#Item) comme pouvant être attrapé par cet objet. Dans ce cas, si l'objet Sprite passe sur l'item, il se déplacera avec lui.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
clé = new Item()
clé.définirPosition(100,0)
bob.peutAttraper(clé)
bob.avancer(200)
```
<a name="Robot+siAttrape"></a>

### robot.siAttrape(item, commande)
Exécute une commande si l'objet attrape cet [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>la commande à exécuter</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siAttraper(clé, "declick.écrire('je l\'ai')")
// Ajout d'une commande par fonction
function effrayer() {
  méchant.avancerToujours()
}
bob.siAttraper(arme, effrayer)
```
<a name="Robot+estSurItem"></a>

### robot.estSurItem(item) ⇒ <code>booléen</code>
Vérifie si l'objet est sur un [Item](#Item).

**Returns**: <code>booléen</code> - **true** si l'objet est sur l'Item, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à rechercher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+lâcher"></a>

### robot.lâcher(item)
Relâche un [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à relâcher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+récupérerItemsAttrapés"></a>

### robot.récupérerItemsAttrapés() ⇒ [<code>Liste</code>](#Liste)
Récupère un objet [Liste](#Liste) avec l'ensemble des objets [Item](#Item) attrapés

**Returns**: [<code>Liste</code>](#Liste) - la liste des objets [Item](#Item)  
<a name="Robot+porteItem"></a>

### robot.porteItem(type) ⇒ <code>booléen</code>
Vérifie si l'objet a attrapé un objet [Item](#Item) du type spécifié.

**Returns**: <code>booléen</code> - **true** si l'objet a attrapé un objet de ce type, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>texte</code></td><td><p>le type d&#39;objet <a href="#Item">Item</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+attraper"></a>

### robot.attraper(objet)
Attrape l'objet [Item](#Item) passé en paramètre, s'il est à la portée de l'objet Sprite (les deux objets doivent se chevaucher).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à attraper</p>
</td>
    </tr>  </tbody>
</table>

<a name="Robot+supprimer"></a>

### robot.supprimer()
Supprime l'objet.

<a name="Sprite"></a>

## Sprite
Un objet qui peut se déplacer, afficher des animations et provoquer des collisions ou des chevauchements avec d'autres objets [Sprite](#Sprite).


* [Sprite](#Sprite)
    * [new Sprite(média)](#new_Sprite_new)
    * [.avancer(distance)](#Sprite+avancer)
    * [.reculer(distance)](#Sprite+reculer)
    * [.monter(distance)](#Sprite+monter)
    * [.descendre(distance)](#Sprite+descendre)
    * [.arrêter()](#Sprite+arrêter)
    * [.avancerToujours()](#Sprite+avancerToujours)
    * [.reculerToujours()](#Sprite+reculerToujours)
    * [.monterToujours()](#Sprite+monterToujours)
    * [.descendreToujours()](#Sprite+descendreToujours)
    * [.siCollisionAvec(objet, commande, paramètre)](#Sprite+siCollisionAvec)
    * [.ajouterBloc(objet)](#Sprite+ajouterBloc)
    * [.siRencontre(objet, commande, paramètre)](#Sprite+siRencontre)
    * [.estDéplaçable(valeur)](#Sprite+estDéplaçable)
    * [.définirPosition(x, y)](#Sprite+définirPosition)
    * [.peutAttraper(item)](#Sprite+peutAttraper)
    * [.siAttrape(item, commande)](#Sprite+siAttrape)
    * [.estSurItem(item)](#Sprite+estSurItem) ⇒ <code>booléen</code>
    * [.lâcher(item)](#Sprite+lâcher)
    * [.récupérerItemsAttrapés()](#Sprite+récupérerItemsAttrapés) ⇒ [<code>Liste</code>](#Liste)
    * [.porteItem(type)](#Sprite+porteItem) ⇒ <code>booléen</code>
    * [.attraper(objet)](#Sprite+attraper)
    * [.récupérerX()](#Sprite+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Sprite+récupérerY) ⇒ <code>nombre</code>
    * [.supprimer()](#Sprite+supprimer)

<a name="new_Sprite_new"></a>

### new Sprite(média)
Crée un objet Sprite.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>média</td><td><code>texte</code></td><td><p>(optionnel) - le nom d&#39;une image ou d&#39;un fichier sprite disponible</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Création d'un objet Sprite avec la représentation par défaut
bob = new Sprite()
// Création d'un objet Sprite à partir d'un média disponible
bob = new Sprite("image")
```
<a name="Sprite+avancer"></a>

### sprite.avancer(distance)
Avance d'une certaine distance.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>nombre</code></td><td><p>La distance en pixels</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Avancer de 10 pixels
bob.avancer(10)
```
<a name="Sprite+reculer"></a>

### sprite.reculer(distance)
Recule d'une certaine distance.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>nombre</code></td><td><p>La distance en pixels</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Reculer de 10 pixels
bob.reculer(10)
```
<a name="Sprite+monter"></a>

### sprite.monter(distance)
Monte d'une certaine distance.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>nombre</code></td><td><p>La distance en pixels</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Monter de 10 pixels
bob.monter(10)
```
<a name="Sprite+descendre"></a>

### sprite.descendre(distance)
Descend d'une certaine distance.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>nombre</code></td><td><p>La distance en pixels</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Descendre de 10 pixels
bob.descendre(10)
```
<a name="Sprite+arrêter"></a>

### sprite.arrêter()
Arrête le déplacement de l'objet.

<a name="Sprite+avancerToujours"></a>

### sprite.avancerToujours()
Avance indéfiniment.

<a name="Sprite+reculerToujours"></a>

### sprite.reculerToujours()
Recule indéfiniment.

<a name="Sprite+monterToujours"></a>

### sprite.monterToujours()
Monte indéfiniment.

<a name="Sprite+descendreToujours"></a>

### sprite.descendreToujours()
Descend indefiniment.

<a name="Sprite+siCollisionAvec"></a>

### sprite.siCollisionAvec(objet, commande, paramètre)
Exécute une commande si l'objet réalise une collision avec l'objet Sprite donné en paramètre.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut générer une collision</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de collision avec une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siCollisionAvec(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siCollisionAvec(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siCollisionAvec(bill, descendre, 50)
```
<a name="Sprite+ajouterBloc"></a>

### sprite.ajouterBloc(objet)
Définit un autre objet Sprite en tant que bloc pour cet objet. L'objet désigné ne laissera pas passer cet objet. Cette méthode est équivalente à la méthode [siCollisionAvec](#Sprite+siCollisionAvec), sauf qu'elle ne définit pas de commande à exécuter.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite défini comme bloc</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
bob.ajouterBloc(rocher)
```
<a name="Sprite+siRencontre"></a>

### sprite.siRencontre(objet, commande, paramètre)
Exécute une commande si l'objet chevauche l'objet Sprite donné en paramètre. À la différence d'une collision, un chevauchement ne perturbe pas le déplacement des objets : ils se superposent quand ils se rencontrent.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut être chevauché</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de chevauchement d&#39;une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siRencontre(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siRencontre(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siRencontre(bill, descendre, 50)
```
<a name="Sprite+estDéplaçable"></a>

### sprite.estDéplaçable(valeur)
Par défaut, un objet est **déplaçable**, c'est à dire qu'il bouge lorsqu'un autre objet entre en collision avec lui. Ce comportement peut être modifié avec cette méthode.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>valeur</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut bouger, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+définirPosition"></a>

### sprite.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en pixels)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en pixels)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+peutAttraper"></a>

### sprite.peutAttraper(item)
Définit un [Item](#Item) comme pouvant être attrapé par cet objet. Dans ce cas, si l'objet Sprite passe sur l'item, il se déplacera avec lui.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
clé = new Item()
clé.définirPosition(100,0)
bob.peutAttraper(clé)
bob.avancer(200)
```
<a name="Sprite+siAttrape"></a>

### sprite.siAttrape(item, commande)
Exécute une commande si l'objet attrape cet [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>la commande à exécuter</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siAttraper(clé, "declick.écrire('je l\'ai')")
// Ajout d'une commande par fonction
function effrayer() {
  méchant.avancerToujours()
}
bob.siAttraper(arme, effrayer)
```
<a name="Sprite+estSurItem"></a>

### sprite.estSurItem(item) ⇒ <code>booléen</code>
Vérifie si l'objet est sur un [Item](#Item).

**Returns**: <code>booléen</code> - **true** si l'objet est sur l'Item, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à rechercher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+lâcher"></a>

### sprite.lâcher(item)
Relâche un [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à relâcher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+récupérerItemsAttrapés"></a>

### sprite.récupérerItemsAttrapés() ⇒ [<code>Liste</code>](#Liste)
Récupère un objet [Liste](#Liste) avec l'ensemble des objets [Item](#Item) attrapés

**Returns**: [<code>Liste</code>](#Liste) - la liste des objets [Item](#Item)  
<a name="Sprite+porteItem"></a>

### sprite.porteItem(type) ⇒ <code>booléen</code>
Vérifie si l'objet a attrapé un objet [Item](#Item) du type spécifié.

**Returns**: <code>booléen</code> - **true** si l'objet a attrapé un objet de ce type, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>texte</code></td><td><p>le type d&#39;objet <a href="#Item">Item</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+attraper"></a>

### sprite.attraper(objet)
Attrape l'objet [Item](#Item) passé en paramètre, s'il est à la portée de l'objet Sprite (les deux objets doivent se chevaucher).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à attraper</p>
</td>
    </tr>  </tbody>
</table>

<a name="Sprite+récupérerX"></a>

### sprite.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en pixels)  
<a name="Sprite+récupérerY"></a>

### sprite.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en pixels)  
<a name="Sprite+supprimer"></a>

### sprite.supprimer()
Supprime l'objet.

<a name="Texte"></a>

## Texte
Un objet qui peut afficher du texte.


* [Texte](#Texte)
    * [new Texte(texte)](#new_Texte_new)
    * [.définirTexte(texte)](#Texte+définirTexte)
    * [.récupérerTexte()](#Texte+récupérerTexte) ⇒ <code>texte</code>
    * [.définirCouleur(rouge, vert, bleu)](#Texte+définirCouleur)
    * [.définirPolice(police)](#Texte+définirPolice)
    * [.définirPosition(x, y)](#Texte+définirPosition)
    * [.récupérerX()](#Texte+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Texte+récupérerY) ⇒ <code>nombre</code>
    * [.supprimer()](#Texte+supprimer)

<a name="new_Texte_new"></a>

### new Texte(texte)
Crée un objet Item.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>texte</td><td><code>texte</code></td><td><p>le texte à afficher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Texte+définirTexte"></a>

### texte.définirTexte(texte)
Définit le texte à afficher.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>texte</td><td><code>texte</code></td><td><p>le texte à afficher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Texte+récupérerTexte"></a>

### texte.récupérerTexte() ⇒ <code>texte</code>
Renvoie le texte affiché.

**Returns**: <code>texte</code> - texte - le texte affiché par l'objet  
<a name="Texte+définirCouleur"></a>

### texte.définirCouleur(rouge, vert, bleu)
Définit la couleur du texte. Par défaut, le texte s'affiche en noir (0,0,0).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>rouge</td><td><code>nombre</code></td><td><p>la quantité de rouge (entre 0 et 255)</p>
</td>
    </tr><tr>
    <td>vert</td><td><code>nombre</code></td><td><p>la quantité de vert (entre 0 et 255)</p>
</td>
    </tr><tr>
    <td>bleu</td><td><code>nombre</code></td><td><p>la quantité de bleu (entre 0 et 255)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Texte+définirPolice"></a>

### texte.définirPolice(police)
Définit la police de caractères. Par défaut, la police est "Courier".

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>police</td><td><code>texte</code></td><td><p>le nom d&#39;une police de caractères</p>
</td>
    </tr>  </tbody>
</table>

<a name="Texte+définirPosition"></a>

### texte.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en pixels)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en pixels)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Texte+récupérerX"></a>

### texte.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en pixels)  
<a name="Texte+récupérerY"></a>

### texte.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en pixels)  
<a name="Texte+supprimer"></a>

### texte.supprimer()
Supprime l'objet.

<a name="Tortue"></a>

## Tortue
Un objet [Robot](#Robot) qui peut laisser une trace sur son chemin.


* [Tortue](#Tortue)
    * [new Tortue(média)](#new_Tortue_new)
    * [.tracer()](#Tortue+tracer)
    * [.nePasTracer()](#Tortue+nePasTracer)
    * [.définirPositionTraceur(x, y)](#Tortue+définirPositionTraceur)
    * [.créerTexture(nom)](#Tortue+créerTexture)
    * [.définirPosition(x, y)](#Tortue+définirPosition)
    * [.avancer(pas)](#Tortue+avancer)
    * [.reculer(pas)](#Tortue+reculer)
    * [.monter(pas)](#Tortue+monter)
    * [.descendre(pas)](#Tortue+descendre)
    * [.définirLongueurPas(longueur)](#Tortue+définirLongueurPas)
    * [.récupérerLongueurPas()](#Tortue+récupérerLongueurPas) ⇒ <code>nombre</code>
    * [.récupérerX()](#Tortue+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Tortue+récupérerY) ⇒ <code>nombre</code>
    * [.peutTomber(value)](#Tortue+peutTomber)
    * [.définirGravité(gravité)](#Tortue+définirGravité)
    * [.sauter()](#Tortue+sauter)
    * [.définirAmplitudeSaut(amplitude)](#Tortue+définirAmplitudeSaut)
    * [.arrêter()](#Tortue+arrêter)
    * [.avancerToujours()](#Tortue+avancerToujours)
    * [.reculerToujours()](#Tortue+reculerToujours)
    * [.monterToujours()](#Tortue+monterToujours)
    * [.descendreToujours()](#Tortue+descendreToujours)
    * [.siCollisionAvec(objet, commande, paramètre)](#Tortue+siCollisionAvec)
    * [.ajouterBloc(objet)](#Tortue+ajouterBloc)
    * [.siRencontre(objet, commande, paramètre)](#Tortue+siRencontre)
    * [.estDéplaçable(valeur)](#Tortue+estDéplaçable)
    * [.peutAttraper(item)](#Tortue+peutAttraper)
    * [.siAttrape(item, commande)](#Tortue+siAttrape)
    * [.estSurItem(item)](#Tortue+estSurItem) ⇒ <code>booléen</code>
    * [.lâcher(item)](#Tortue+lâcher)
    * [.récupérerItemsAttrapés()](#Tortue+récupérerItemsAttrapés) ⇒ [<code>Liste</code>](#Liste)
    * [.porteItem(type)](#Tortue+porteItem) ⇒ <code>booléen</code>
    * [.attraper(objet)](#Tortue+attraper)
    * [.supprimer()](#Tortue+supprimer)

<a name="new_Tortue_new"></a>

### new Tortue(média)
Crée un objet Tortue.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>média</td><td><code>texte</code></td><td><p>(optionnel) - le nom d&#39;une image ou d&#39;un fichier sprite disponible</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Création d'un objet Tortue avec la représentation par défaut
bob = new Tortue()
// Création d'un objet Tortue à partir d'un média disponible
bob = new Tortue("image")
```
<a name="Tortue+tracer"></a>

### tortue.tracer()
Dit à l'objet de laisser une trace lors de ses déplacements.

**Example**  
```js
bob.tracer()
```
<a name="Tortue+nePasTracer"></a>

### tortue.nePasTracer()
Dit à l'objet de ne pas laisser de trace lors de ses déplacements.

**Example**  
```js
bob.nePasTracer()
```
<a name="Tortue+définirPositionTraceur"></a>

### tortue.définirPositionTraceur(x, y)
Définit la position du traceur par rapport à l'objet. Le coin en haut et à gauche de l'objet représente les coordonnées (0,0).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la position horizontale du traceur par rapport au coin en haut à gauche de l&#39;objet (en pixels)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la position verticale du traceur par rapport au coin en haut à gauche de l&#39;objet (en pixels)</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// locaclise le traceur à 50 pixels du haut et à 20 pixels de la gauche de l'objet
bob.définirPositionTraceur(20,50)
```
<a name="Tortue+créerTexture"></a>

### tortue.créerTexture(nom)
Créer une image à partir de la trace laissée par la tortue. Cette trace fait ensuite partie des médias disponibles : elle peut être utilisée pour changer l'apparence d'autres objets.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>nom</td><td><code>texte</code></td><td><p>le nom de l&#39;image créée</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// création de l'image
bob.créerTexture("ma trace")
// utilisation de l'image pour créer un nouvel objet [Sprite](#Sprite)
bill = new Sprite("ma trace")
```
<a name="Tortue+définirPosition"></a>

### tortue.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en nombre de pas)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en nombre de pas)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+avancer"></a>

### tortue.avancer(pas)
Avance d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Avancer de 2 pas
bob.avancer(2)
```
<a name="Tortue+reculer"></a>

### tortue.reculer(pas)
Recule d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Reculer de 2 pas
bob.reculer(2)
```
<a name="Tortue+monter"></a>

### tortue.monter(pas)
Monte d'une certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Monter de 2 pas
bob.monter(2)
```
<a name="Tortue+descendre"></a>

### tortue.descendre(pas)
Descend d'un certain nombre de pas.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>pas</td><td><code>nombre</code></td><td><p>Le nombre de pas</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Descendre de 2 pas
bob.descendre(2)
```
<a name="Tortue+définirLongueurPas"></a>

### tortue.définirLongueurPas(longueur)
Définit la longueur d'un pas, en pixel.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>longueur</td><td><code>nombre</code></td><td><p>la longueur d&#39;un pas en pixels (valeur par défaut : 50)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+récupérerLongueurPas"></a>

### tortue.récupérerLongueurPas() ⇒ <code>nombre</code>
Récupère la longueur d'un pas, exprimée en pixel.

**Returns**: <code>nombre</code> - longueur - la longueur des pas de l'objet  
<a name="Tortue+récupérerX"></a>

### tortue.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en nombre de pas)  
<a name="Tortue+récupérerY"></a>

### tortue.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en nombre de pas)  
<a name="Tortue+peutTomber"></a>

### tortue.peutTomber(value)
Définit si l'objet Promeneur peut tomber ou non.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>value</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut tomber, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+définirGravité"></a>

### tortue.définirGravité(gravité)
Définit la valeur de la gravité. Plus la valeur est élevée, plus l'objet tombera vite. La valeur par défaut est de 500. Une valeur négative peut aussi être définie, dans ce cas l'objet s'envolera.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>gravité</td><td><code>nombre</code></td><td><p>un nombre entier, positif ou négatif (valeur par défaut : 500)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+sauter"></a>

### tortue.sauter()
Fait sauter l'objet Promeneur. Pour pouvoir sauter, l'objet doit être situé sur un objet qui le bloque : objet [Sprite](#Sprite) ou [Plateforme](#Plateforme).

<a name="Tortue+définirAmplitudeSaut"></a>

### tortue.définirAmplitudeSaut(amplitude)
Définit l'amplitude du saut. Par défaut, cette amplitude est de 400. Plus la valeur est élevée, plus le Promeneur sautera haut.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>amplitude</td><td><code>nombre</code></td><td><p>un nombre entier positif (valeur par défaut : 400)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+arrêter"></a>

### tortue.arrêter()
Arrête le déplacement de l'objet.

<a name="Tortue+avancerToujours"></a>

### tortue.avancerToujours()
Avance indéfiniment.

<a name="Tortue+reculerToujours"></a>

### tortue.reculerToujours()
Recule indéfiniment.

<a name="Tortue+monterToujours"></a>

### tortue.monterToujours()
Monte indéfiniment.

<a name="Tortue+descendreToujours"></a>

### tortue.descendreToujours()
Descend indefiniment.

<a name="Tortue+siCollisionAvec"></a>

### tortue.siCollisionAvec(objet, commande, paramètre)
Exécute une commande si l'objet réalise une collision avec l'objet Sprite donné en paramètre.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut générer une collision</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de collision avec une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siCollisionAvec(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siCollisionAvec(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siCollisionAvec(bill, descendre, 50)
```
<a name="Tortue+ajouterBloc"></a>

### tortue.ajouterBloc(objet)
Définit un autre objet Sprite en tant que bloc pour cet objet. L'objet désigné ne laissera pas passer cet objet. Cette méthode est équivalente à la méthode [siCollisionAvec](#Sprite+siCollisionAvec), sauf qu'elle ne définit pas de commande à exécuter.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite défini comme bloc</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
bob.ajouterBloc(rocher)
```
<a name="Tortue+siRencontre"></a>

### tortue.siRencontre(objet, commande, paramètre)
Exécute une commande si l'objet chevauche l'objet Sprite donné en paramètre. À la différence d'une collision, un chevauchement ne perturbe pas le déplacement des objets : ils se superposent quand ils se rencontrent.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut être chevauché</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de chevauchement d&#39;une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siRencontre(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siRencontre(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siRencontre(bill, descendre, 50)
```
<a name="Tortue+estDéplaçable"></a>

### tortue.estDéplaçable(valeur)
Par défaut, un objet est **déplaçable**, c'est à dire qu'il bouge lorsqu'un autre objet entre en collision avec lui. Ce comportement peut être modifié avec cette méthode.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>valeur</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut bouger, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+peutAttraper"></a>

### tortue.peutAttraper(item)
Définit un [Item](#Item) comme pouvant être attrapé par cet objet. Dans ce cas, si l'objet Sprite passe sur l'item, il se déplacera avec lui.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
clé = new Item()
clé.définirPosition(100,0)
bob.peutAttraper(clé)
bob.avancer(200)
```
<a name="Tortue+siAttrape"></a>

### tortue.siAttrape(item, commande)
Exécute une commande si l'objet attrape cet [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>la commande à exécuter</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siAttraper(clé, "declick.écrire('je l\'ai')")
// Ajout d'une commande par fonction
function effrayer() {
  méchant.avancerToujours()
}
bob.siAttraper(arme, effrayer)
```
<a name="Tortue+estSurItem"></a>

### tortue.estSurItem(item) ⇒ <code>booléen</code>
Vérifie si l'objet est sur un [Item](#Item).

**Returns**: <code>booléen</code> - **true** si l'objet est sur l'Item, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à rechercher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+lâcher"></a>

### tortue.lâcher(item)
Relâche un [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à relâcher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+récupérerItemsAttrapés"></a>

### tortue.récupérerItemsAttrapés() ⇒ [<code>Liste</code>](#Liste)
Récupère un objet [Liste](#Liste) avec l'ensemble des objets [Item](#Item) attrapés

**Returns**: [<code>Liste</code>](#Liste) - la liste des objets [Item](#Item)  
<a name="Tortue+porteItem"></a>

### tortue.porteItem(type) ⇒ <code>booléen</code>
Vérifie si l'objet a attrapé un objet [Item](#Item) du type spécifié.

**Returns**: <code>booléen</code> - **true** si l'objet a attrapé un objet de ce type, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>texte</code></td><td><p>le type d&#39;objet <a href="#Item">Item</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+attraper"></a>

### tortue.attraper(objet)
Attrape l'objet [Item](#Item) passé en paramètre, s'il est à la portée de l'objet Sprite (les deux objets doivent se chevaucher).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à attraper</p>
</td>
    </tr>  </tbody>
</table>

<a name="Tortue+supprimer"></a>

### tortue.supprimer()
Supprime l'objet.

<a name="Promeneur"></a>

## Promeneur
Un objet [Sprite](#Sprite) qui peut tomber et sauter


* [Promeneur](#Promeneur)
    * [new Promeneur(média)](#new_Promeneur_new)
    * [.peutTomber(value)](#Promeneur+peutTomber)
    * [.définirGravité(gravité)](#Promeneur+définirGravité)
    * [.sauter()](#Promeneur+sauter)
    * [.définirAmplitudeSaut(amplitude)](#Promeneur+définirAmplitudeSaut)
    * [.avancer(distance)](#Promeneur+avancer)
    * [.reculer(distance)](#Promeneur+reculer)
    * [.monter(distance)](#Promeneur+monter)
    * [.descendre(distance)](#Promeneur+descendre)
    * [.arrêter()](#Promeneur+arrêter)
    * [.avancerToujours()](#Promeneur+avancerToujours)
    * [.reculerToujours()](#Promeneur+reculerToujours)
    * [.monterToujours()](#Promeneur+monterToujours)
    * [.descendreToujours()](#Promeneur+descendreToujours)
    * [.siCollisionAvec(objet, commande, paramètre)](#Promeneur+siCollisionAvec)
    * [.ajouterBloc(objet)](#Promeneur+ajouterBloc)
    * [.siRencontre(objet, commande, paramètre)](#Promeneur+siRencontre)
    * [.estDéplaçable(valeur)](#Promeneur+estDéplaçable)
    * [.définirPosition(x, y)](#Promeneur+définirPosition)
    * [.peutAttraper(item)](#Promeneur+peutAttraper)
    * [.siAttrape(item, commande)](#Promeneur+siAttrape)
    * [.estSurItem(item)](#Promeneur+estSurItem) ⇒ <code>booléen</code>
    * [.lâcher(item)](#Promeneur+lâcher)
    * [.récupérerItemsAttrapés()](#Promeneur+récupérerItemsAttrapés) ⇒ [<code>Liste</code>](#Liste)
    * [.porteItem(type)](#Promeneur+porteItem) ⇒ <code>booléen</code>
    * [.attraper(objet)](#Promeneur+attraper)
    * [.récupérerX()](#Promeneur+récupérerX) ⇒ <code>nombre</code>
    * [.récupérerY()](#Promeneur+récupérerY) ⇒ <code>nombre</code>
    * [.supprimer()](#Promeneur+supprimer)

<a name="new_Promeneur_new"></a>

### new Promeneur(média)
Crée un objet Promeneur

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>média</td><td><code>texte</code></td><td><p>(optionnel) - le nom d&#39;une image ou d&#39;un fichier sprite disponible</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Création d'un objet Promeneur avec la représentation par défaut
bob = new Promeneur()
// Création d'un objet Promeneur à partir d'un média disponible
bob = new Promeneur("image")
```
<a name="Promeneur+peutTomber"></a>

### promeneur.peutTomber(value)
Définit si l'objet Promeneur peut tomber ou non.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>value</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut tomber, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+définirGravité"></a>

### promeneur.définirGravité(gravité)
Définit la valeur de la gravité. Plus la valeur est élevée, plus l'objet tombera vite. La valeur par défaut est de 500. Une valeur négative peut aussi être définie, dans ce cas l'objet s'envolera.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>gravité</td><td><code>nombre</code></td><td><p>un nombre entier, positif ou négatif (valeur par défaut : 500)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+sauter"></a>

### promeneur.sauter()
Fait sauter l'objet Promeneur. Pour pouvoir sauter, l'objet doit être situé sur un objet qui le bloque : objet [Sprite](#Sprite) ou [Plateforme](#Plateforme).

<a name="Promeneur+définirAmplitudeSaut"></a>

### promeneur.définirAmplitudeSaut(amplitude)
Définit l'amplitude du saut. Par défaut, cette amplitude est de 400. Plus la valeur est élevée, plus le Promeneur sautera haut.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>amplitude</td><td><code>nombre</code></td><td><p>un nombre entier positif (valeur par défaut : 400)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+avancer"></a>

### promeneur.avancer(distance)
Avance d'une certaine distance.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>nombre</code></td><td><p>La distance en pixels</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Avancer de 10 pixels
bob.avancer(10)
```
<a name="Promeneur+reculer"></a>

### promeneur.reculer(distance)
Recule d'une certaine distance.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>nombre</code></td><td><p>La distance en pixels</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Reculer de 10 pixels
bob.reculer(10)
```
<a name="Promeneur+monter"></a>

### promeneur.monter(distance)
Monte d'une certaine distance.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>nombre</code></td><td><p>La distance en pixels</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Monter de 10 pixels
bob.monter(10)
```
<a name="Promeneur+descendre"></a>

### promeneur.descendre(distance)
Descend d'une certaine distance.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>distance</td><td><code>nombre</code></td><td><p>La distance en pixels</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Descendre de 10 pixels
bob.descendre(10)
```
<a name="Promeneur+arrêter"></a>

### promeneur.arrêter()
Arrête le déplacement de l'objet.

<a name="Promeneur+avancerToujours"></a>

### promeneur.avancerToujours()
Avance indéfiniment.

<a name="Promeneur+reculerToujours"></a>

### promeneur.reculerToujours()
Recule indéfiniment.

<a name="Promeneur+monterToujours"></a>

### promeneur.monterToujours()
Monte indéfiniment.

<a name="Promeneur+descendreToujours"></a>

### promeneur.descendreToujours()
Descend indefiniment.

<a name="Promeneur+siCollisionAvec"></a>

### promeneur.siCollisionAvec(objet, commande, paramètre)
Exécute une commande si l'objet réalise une collision avec l'objet Sprite donné en paramètre.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut générer une collision</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de collision avec une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siCollisionAvec(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siCollisionAvec(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siCollisionAvec(bill, descendre, 50)
```
<a name="Promeneur+ajouterBloc"></a>

### promeneur.ajouterBloc(objet)
Définit un autre objet Sprite en tant que bloc pour cet objet. L'objet désigné ne laissera pas passer cet objet. Cette méthode est équivalente à la méthode [siCollisionAvec](#Sprite+siCollisionAvec), sauf qu'elle ne définit pas de commande à exécuter.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite défini comme bloc</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
bob.ajouterBloc(rocher)
```
<a name="Promeneur+siRencontre"></a>

### promeneur.siRencontre(objet, commande, paramètre)
Exécute une commande si l'objet chevauche l'objet Sprite donné en paramètre. À la différence d'une collision, un chevauchement ne perturbe pas le déplacement des objets : ils se superposent quand ils se rencontrent.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Sprite">Sprite</a></code></td><td><p>L&#39;objet Sprite qui peut être chevauché</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>La commande à exécuter</p>
</td>
    </tr><tr>
    <td>paramètre</td><td><code>*</code></td><td><p>(optionnel) - Un paramètre optionnel. En cas de chevauchement d&#39;une plateforme, sert à préciser la catégorie des briques</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siRencontre(bill, "declick.écrire('aïe')")
// Ajout d'une commande par fonction
function disparaître() {
  bob.supprimer()
}
bob.siRencontre(bill, disparaître)
// Ajout d'une commande par fonction avec un paramètre
function descendre(distance) {
  bob.descendre(50)
}
bob.siRencontre(bill, descendre, 50)
```
<a name="Promeneur+estDéplaçable"></a>

### promeneur.estDéplaçable(valeur)
Par défaut, un objet est **déplaçable**, c'est à dire qu'il bouge lorsqu'un autre objet entre en collision avec lui. Ce comportement peut être modifié avec cette méthode.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>valeur</td><td><code>booléen</code></td><td><p><strong>true</strong> si l&#39;objet peut bouger, <strong>false</strong> sinon</p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+définirPosition"></a>

### promeneur.définirPosition(x, y)
Définit la position de l'objet

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>x</td><td><code>nombre</code></td><td><p>la cooordonnée x (en pixels)</p>
</td>
    </tr><tr>
    <td>y</td><td><code>nombre</code></td><td><p>la coordonnée y (en pixels)</p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+peutAttraper"></a>

### promeneur.peutAttraper(item)
Définit un [Item](#Item) comme pouvant être attrapé par cet objet. Dans ce cas, si l'objet Sprite passe sur l'item, il se déplacera avec lui.

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
clé = new Item()
clé.définirPosition(100,0)
bob.peutAttraper(clé)
bob.avancer(200)
```
<a name="Promeneur+siAttrape"></a>

### promeneur.siAttrape(item, commande)
Exécute une commande si l'objet attrape cet [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> qui peut être attrapé</p>
</td>
    </tr><tr>
    <td>commande</td><td><code>fonction</code> | <code>texte</code></td><td><p>la commande à exécuter</p>
</td>
    </tr>  </tbody>
</table>

**Example**  
```js
// Ajout d'une commande directe
bob.siAttraper(clé, "declick.écrire('je l\'ai')")
// Ajout d'une commande par fonction
function effrayer() {
  méchant.avancerToujours()
}
bob.siAttraper(arme, effrayer)
```
<a name="Promeneur+estSurItem"></a>

### promeneur.estSurItem(item) ⇒ <code>booléen</code>
Vérifie si l'objet est sur un [Item](#Item).

**Returns**: <code>booléen</code> - **true** si l'objet est sur l'Item, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à rechercher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+lâcher"></a>

### promeneur.lâcher(item)
Relâche un [Item](#Item).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>item</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à relâcher</p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+récupérerItemsAttrapés"></a>

### promeneur.récupérerItemsAttrapés() ⇒ [<code>Liste</code>](#Liste)
Récupère un objet [Liste](#Liste) avec l'ensemble des objets [Item](#Item) attrapés

**Returns**: [<code>Liste</code>](#Liste) - la liste des objets [Item](#Item)  
<a name="Promeneur+porteItem"></a>

### promeneur.porteItem(type) ⇒ <code>booléen</code>
Vérifie si l'objet a attrapé un objet [Item](#Item) du type spécifié.

**Returns**: <code>booléen</code> - **true** si l'objet a attrapé un objet de ce type, **false** sinon  
<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>type</td><td><code>texte</code></td><td><p>le type d&#39;objet <a href="#Item">Item</a></p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+attraper"></a>

### promeneur.attraper(objet)
Attrape l'objet [Item](#Item) passé en paramètre, s'il est à la portée de l'objet Sprite (les deux objets doivent se chevaucher).

<table>
  <thead>
    <tr>
      <th>Param</th><th>Type</th><th>Description</th>
    </tr>
  </thead>
  <tbody>
<tr>
    <td>objet</td><td><code><a href="#Item">Item</a></code></td><td><p>l&#39;objet <a href="#Item">Item</a> à attraper</p>
</td>
    </tr>  </tbody>
</table>

<a name="Promeneur+récupérerX"></a>

### promeneur.récupérerX() ⇒ <code>nombre</code>
Retrouve la coordonnée x de l'objet

**Returns**: <code>nombre</code> - la coordonnée x (en pixels)  
<a name="Promeneur+récupérerY"></a>

### promeneur.récupérerY() ⇒ <code>nombre</code>
Retrouve la coordonnée y de l'objet

**Returns**: <code>nombre</code> - la coordonnée y (en pixels)  
<a name="Promeneur+supprimer"></a>

### promeneur.supprimer()
Supprime l'objet.

<a name="caméra"></a>

## caméra : <code>Caméra</code>
<a name="declick"></a>

## declick : [<code>Declick</code>](#Declick)
<a name="clavier"></a>

## clavier : [<code>Clavier</code>](#Clavier)
